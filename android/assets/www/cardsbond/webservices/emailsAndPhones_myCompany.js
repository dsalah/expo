function getMyCompanyContactsEmailsFromWebService(bondId){
console.log("getMyCompanyContactsEmailsFromWebService");
var url = "http://www.cardsbond.com/BondSys/web/api/v1/contacts/emails?id="+bondId;
var ajaxRequest;
		try{
			ajaxRequest = new XMLHttpRequest();
		} catch (e){
			try{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
					//alert("Microsoft");
				} catch (e){
					alert("Your browser broke!");
				return false;
				}
			}
		}

		ajaxRequest.onreadystatechange = function(){
		//alert(ajaxRequest.readyState);
			if(ajaxRequest.readyState == 4 && ajaxRequest.status==200){
				var response=ajaxRequest.responseText;
				//alert("res email"+response);
			getMyCompanyContactsMobilesFromWebService(bondId);
				if(response.search('error') == -1){
					var results = JSON.parse(response);
					var email, emailid, bId, typeId;
						for(var i=0; i<results.result.length;i++){
						emailid = results.result[i].bond_contact_email_id;						
						email =  results.result[i].email;
						bId= results.result[i].bond_contact_id;
						typeId = results.result[i].contact_levels;
							insertEmails(emailid, email, bId, typeId);
						}
	        
				}

			}
		}
		ajaxRequest.open("GET", url, true);		
		ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		//ajaxRequest.setRequestHeader("Content-length", param.length);
		ajaxRequest.setRequestHeader("Connection", "close");
		ajaxRequest.send();	

}
/**************************************************************/	
function insertEmails(emailid, email, contactid, typeId)
{
		var insertEmail= 'INSERT INTO EMAILS(emailid, contactid, emailvalue, typeid) VALUES (?, ?, ?, ?)';
		var selectEmail= 'SELECT emailid FROM EMAILS WHERE emailid=(?)';

		var insertEmailType= 'INSERT INTO EMAILTYPE(emailid, typeid) VALUES (?, ?)';
		
			db.transaction(function(tx){
							tx.executeSql(selectEmail,[emailid],function(tx,res){
				//				alert("emails len b4 insert : "+res.rows.length);
								if(res.rows.length==0){
									tx.executeSql(insertEmail,[emailid, contactid, email, typeId]);
			//						tx.executeSql(insertEmailType,[emailid, typeId]);

								}
									},errorCB);		
			}, errorCB, successDB);   											
		
}
/**************************************************************/
function getMyCompanyContactsMobilesFromWebService(bondId){
var url = "http://www.cardsbond.com/BondSys/web/api/v1/contacts/mobiles?id="+bondId;
//alert("mobile"+url);
var ajaxRequest;
		try{
			ajaxRequest = new XMLHttpRequest();
		} catch (e){
			try{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
					//alert("Microsoft");
				} catch (e){
					alert("Your browser broke!");
				return false;
				}
			}
		}
		ajaxRequest.onreadystatechange = function(){
		//alert(ajaxRequest.readyState);
			if(ajaxRequest.readyState == 4 && ajaxRequest.status==200){
				var response=ajaxRequest.responseText;
				getMyCompanyContactsPhonesFromWebService(bondId);
				//alert("res mobile"+response);						
				if(response.search('error') == -1){
					var results = JSON.parse(response);
						var phoneid, phone, bId, typeId;
						for(var i=0; i<results.result.length;i++){
							phoneid= results.result[i].bond_contact_mobile_id;
							phone= results.result[i].mobile;
							bId= results.result[i].bond_contact_id;
							typeId = results.result[i].contact_levels;
							insertMobiles(phoneid, phone, bId, typeId);
						}
		
				}
			}
			}
		ajaxRequest.open("GET", url, true);		
		ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		//ajaxRequest.setRequestHeader("Content-length", param.length);
		ajaxRequest.setRequestHeader("Connection", "close");
		ajaxRequest.send();	

}	
/******************************************************************/
function insertMobiles(mobileid, mobile, contactid, typeId)
{
	var insertMobile= 'INSERT INTO MOBILES(mobileid, contactid, mobilevalue, typeid) VALUES (?, ?, ?, ?)';

	var selectMobile= 'SELECT mobileid FROM MOBILES WHERE mobileid=(?)';
	db.transaction(function(tx){
		tx.executeSql(selectMobile,[mobileid],function(tx,res){
		if(res.rows.length==0){
			tx.executeSql(insertMobile,[mobileid, contactid, mobile, typeId]);

	//		alert("mobile: "+mobile);		
		}
		
		},errorCB);		
		
	}, errorCB, successDB);   												
}
/***************************************/
function getMyCompanyContactsPhonesFromWebService(bondId){
var url = "http://www.cardsbond.com/BondSys/web/api/v1/contacts/phones?id="+bondId;
//alert("phone"+url);
var ajaxRequest;
		try{
			ajaxRequest = new XMLHttpRequest();
		} catch (e){
			try{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
					//alert("Microsoft");
				} catch (e){
					alert("Your browser broke!");
				return false;
				}
			}
		}
		ajaxRequest.onreadystatechange = function(){
		//alert(ajaxRequest.readyState);
			if(ajaxRequest.readyState == 4 && ajaxRequest.status==200){
				var response=ajaxRequest.responseText;
				getMyCompanyContactsFaxesFromWebService(bondId);
				//alert("res phone"+response);						
				if(response.search('error') == -1){
					var results = JSON.parse(response);
						var phoneid, phone, bId, typeId;
						for(var i=0; i<results.result.length;i++){
							phoneid= results.result[i].bond_contact_phone_id;
							phone= results.result[i].phone;
							bId= results.result[i].bond_contact_id;
							typeId = results.result[i].contact_levels;
							insertPhones(phoneid, phone, bId, typeId);
						}
		
				}
			}
			}
		ajaxRequest.open("GET", url, true);		
		ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		//ajaxRequest.setRequestHeader("Content-length", param.length);
		ajaxRequest.setRequestHeader("Connection", "close");
		ajaxRequest.send();	

}	
/******************************************************************/
function insertPhones(phoneid, phone, contactid, typeId)
{
	var insertPhones= 'INSERT INTO PHONES(phoneid, contactid, phonevalue, typeid) VALUES (?, ?, ?, ?)';
	var selectPhones= 'SELECT phoneid FROM PHONES WHERE phoneid=(?)';
	
	db.transaction(function(tx){
		tx.executeSql(selectPhones,[phoneid],function(tx,res){
		if(res.rows.length==0){
			tx.executeSql(insertPhones,[phoneid, contactid, phone, typeId]);
		}		
		},errorCB);		
		
	}, errorCB, successDB);   											
	
}
/***************************************/
function getMyCompanyContactsFaxesFromWebService(bondId, contactType){
var url = "http://www.cardsbond.com/BondSys/web/api/v1/contacts/faxes?id="+bondId;
//alert("fax"+url);
var ajaxRequest;
		try{
			ajaxRequest = new XMLHttpRequest();
		} catch (e){
			try{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
					//alert("Microsoft");
				} catch (e){
					alert("Your browser broke!");
				return false;
				}
			}
		}
		ajaxRequest.onreadystatechange = function(){
		//alert(ajaxRequest.readyState);
			if(ajaxRequest.readyState == 4 && ajaxRequest.status==200){
				var response=ajaxRequest.responseText;
				//alert("fax res"+response);				
				if(response.search('error') == -1){
					var results = JSON.parse(response);
						var phoneid, phone, bId, typeId;
						for(var i=0; i<results.result.length;i++){
							phoneid= results.result[i].bond_contact_fax_id;
							phone= results.result[i].fax;
							bId= results.result[i].bond_contact_id;
							typeId = results.result[i].contact_levels;
							insertFaxes(phoneid, phone, bId, typeId);
						}
		
				}
			}
			}
		
		ajaxRequest.open("GET", url, true);		
		ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		//ajaxRequest.setRequestHeader("Content-length", param.length);
		ajaxRequest.setRequestHeader("Connection", "close");
		ajaxRequest.send();	

}	
/******************************************************************/
function insertFaxes(faxid, fax, contactid, typeId)
{
	var insertFaxes= 'INSERT INTO FAXES(faxid, contactid, faxvalue, typeid) VALUES (?, ?, ?, ?)';

	var selectFaxes= 'SELECT faxid FROM FAXES WHERE faxid=(?)';
	db.transaction(function(tx){
		tx.executeSql(selectFaxes,[faxid],function(tx,res){
		if(res.rows.length==0){
			tx.executeSql(insertFaxes,[faxid, contactid, fax, typeId]);
	//		tx.executeSql(insertFaxesType,[faxid, typeId]);

			//alert("phones"+phone);		
		}
		
		},errorCB);		
	}, errorCB, successDB);   											
}