function fileSystemSuccess(fileSystem) {
    directoryEntry = fileSystem.root; // to get root path of directory
    directoryEntry.getDirectory(Folder_Name, { create: true, exclusive: false }, onDirectorySuccess, onDirectoryFail); // creating folder in sdcard
    rootdir = fileSystem.root;
    fp = rootdir.fullPath; // Returns Fulpath of local directory  
}

function onDirectorySuccess(parent) {
    // Directory created successfuly
}

function onDirectoryFail(error) {
    //Error while creating directory
  //  alert("Unable to create new directory: " + error.code);
}

  function fileSystemFail(evt) {
//    alert(evt.target.error.code);
 }


/*****************************************/
function filetransfer(download_link, targetDesination) {
	//alert(download_link);
	var fileTransfer = new FileTransfer();
	
	fileTransfer.download(download_link, targetDesination,
    	function (entry) {
    		
    		var updates= 'DELETE FROM UPDATESFUNCTIONS WHERE name =(?) AND parameters=(?)';
			db.transaction(function(tx){
				var arr1 = download_link+"|>"+targetDesination;
			    tx.executeSql(updates,["filetransfer", arr1]);
			}, errorCB, successDB);		
    		
        	var systemPath = entry.fullPath;

        },
        function (error) {
        	//Download abort errors or download failed errors
            console.log("download error source " + error.source);
            //alert("download error target " + error.target);
            console.log("upload error code" + error.code);
            var select_updates= 'SELECT * FROM UPDATESFUNCTIONS WHERE name = (?) AND parameters=(?)';					 		
            var updates= 'INSERT INTO UPDATESFUNCTIONS(updateid, name, parameters, datetime) VALUES (?,?,?,?)';
			    var sql_max_updated = "SELECT MAX(updateid) as updateid FROM UPDATESFUNCTIONS";	    
				db.transaction(function(tx){
					var updatedId = 0, arr1=""; 
					arr1 = download_link+"|>"+targetDesination;
					 
					tx.executeSql(select_updates,["filetransfer", arr1],function(tx, rs){
						if(rs.rows.length==0){
					
					    	tx.executeSql(sql_max_updated,[],function(tx, results){
					    		var len = results.rows.length;
					    		if(len>0)
					    		updatedId = results.rows.item(0).updateid;
					    		updatedId= updatedId+1;
					    		arr1 = download_link+"|>"+targetDesination;
					    		console.log("arr1"+arr1);
					    		console.log("updated function id "+updatedId);
							    tx.executeSql(updates,[updatedId, "filetransfer", arr1, setLastUpdatesTime()]);
		    				},errorCB);		
						}
					},errorCB);		
				}, errorCB, successDB);	
			fileTransfer.abort();		
            }
   );
}
/*************************************************************************/
function setLastUpdatesTime(){
return new Date();
}