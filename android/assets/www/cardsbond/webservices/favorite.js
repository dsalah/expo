function fillInFavoriteContacts()
{
	
	var ajaxRequest;
	//alert("myId: "+id);
	var url ="http://www.cardsbond.com/BondSys/web/api/v1/contacts/favcontacts?bond_contact_id="+id+"&is_favorite=&level_filter=0";
	//alert(url);
		try{
			ajaxRequest = new XMLHttpRequest();
		} catch (e){
			try{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
					//alert("Microsoft");
				} catch (e){
					alert("Your browser broke!");
				return false;
				}
			}
		}
		ajaxRequest.onreadystatechange = function(){
//		alert("req: "+ajaxRequest.readyState);		
			if(ajaxRequest.readyState == 4){
				var response=ajaxRequest.responseText;
				
				getEmailsFromWebService(id, "session");
		//		alert("favorite: "+response);
				var bondId, bond_socialnetwork_id, typeVal, contactconnectionid;
				if(response.search('error')==-1){
					results = JSON.parse(response);
		//			alert("no error"+results.result.length);
						for(var i=0; i<results.result.length;i++){		
			//			alert("in for"+i);		
							bondId = results.result[i].bond_contact_id;
							typeVal = results.result[i].is_favorite;
							contactconnectionid= results.result[i].bond_contactconnection_id;
							if(typeVal=="1")
								typeVal="true";
							else if(typeVal=="0")
								typeVal="false";
				//            alert("fav id: "+bondId);
				            insertFavoriteContacts(bondId,typeVal, contactconnectionid);
						}			
				}
				}
				}
		ajaxRequest.open("GET", url, true);		
		ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		//ajaxRequest.setRequestHeader("Content-length", param.length);
		ajaxRequest.setRequestHeader("Connection", "close");
		ajaxRequest.send();	
	}
/**************************************************************************/
function insertFavoriteContacts(bondId,typeVal, contactconnectionid)
{
//	alert("insertFavoriteContacts: "+bondId);        
	
	var insertFavorite = 'INSERT INTO CONTACTSCONNECTION (contactconnectionid, contactid, othercontactid, type, typevalue) VALUES (?, ?, ?,?,?)';
    var selectFavorite = 'SELECT * FROM CONTACTSCONNECTION WHERE contactid=(?) AND othercontactid=(?) AND type=(?) AND typevalue=(?)';
		db.transaction(function(tx){
	   		tx.executeSql(selectFavorite,[id, bondId, "favorite", "true"],function(tx,results){
	//   		alert("select favorite len: "+results.rows.length);
	   		if(results.rows.length==0){
	//   		alert("b4 insert fav : "+bondId);
		   		tx.executeSql(insertFavorite,[contactconnectionid, id, bondId, "favorite", typeVal]);
	   		}
	   		},errorCB);
	   				
	   		
    	}, errorCB, successDB);
	
}
/*******************************************/
