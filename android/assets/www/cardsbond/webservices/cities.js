function getCitiesFromWebService(){
	console.log("getCitiesFromWebService");
	checkConnection();
	if(networkState!="none" && networkState!="unknown")
	{			
	var url = "http://www.cardsbond.com/BondSys/web/api/v1/cities";
	var ajaxRequest;
		try{
			ajaxRequest = new XMLHttpRequest();
		} catch (e){
			try{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
					//alert("Microsoft");
				} catch (e){
					//alert("Your browser broke!");
					return false;
				}
			}
		}
		ajaxRequest.onreadystatechange = function(){
			if(ajaxRequest.readyState == 4){
				var response=ajaxRequest.responseText;
				//alert(response);
				if(response.search('error') == -1){
					var results = JSON.parse(response);
					var cityId,name, erpCountry;
						for(var i=0; i<results.result.length;i++){				
							cityId = results.result[i].bond_city_id;
		            		name = results.result[i].name;
		            		erpCountry = results.result[i].erp_country_id;            	
		            		insertCities(cityId, name, erpCountry);
		           	}
	           	}
	       		getPositionsFromWebService();	        		    		
			}
		}
		ajaxRequest.open("GET", url, true);		
		ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		//ajaxRequest.setRequestHeader("Content-length", param.length);
		ajaxRequest.setRequestHeader("Connection", "close");
		ajaxRequest.send();	
	}
}	
/************************************************/
function insertCities(cityId, name, erpCountry)
{
	var insertcitysql = 'INSERT INTO CITIES (cityid, name, erpcountry) VALUES (?,?,?)';
    var selectcitysql = 'SELECT cityid FROM CITIES WHERE cityid=(?)';    
	db.transaction(function(tx){
		tx.executeSql(selectcitysql,[cityId],function(tx,results){
			if(results.rows.length==0){
		   		tx.executeSql(insertcitysql,[cityId, name, erpCountry]);
		   	}
		},errorCB);   	
    }, errorCB, successDB);            	
}
/************************************************/
function getPositionsFromWebService(){
	console.log("getPositionsFromWebService");
	checkConnection();
	if(networkState!="none" && networkState!="unknown")
	{			
	var url = "http://www.cardsbond.com/BondSys/web/api/v1/positions";
	var ajaxRequest;
	try{
			ajaxRequest = new XMLHttpRequest();
	} catch (e){
			try{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
				} catch (e) {
				try{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
					//alert("Microsoft");
				} catch (e){
					alert("Your browser broke!");
				return false;
				}
			}
		}
		ajaxRequest.onreadystatechange = function(){
		//alert(ajaxRequest.readyState);
			if(ajaxRequest.readyState == 4){
				response=ajaxRequest.responseText;
				getCountriesFromWebService();
				var results = JSON.parse(response);
				var posId,name, order;
				for(var i=0; i<results.result.length;i++){				
					posId = results.result[i].bond_position_id;
            		name = results.result[i].name;
            		order = results.result[i]._order;            	
            		insertPositions(posId, name, order);
            			}
				}
				}
		ajaxRequest.open("GET", url, true);		
		ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		//ajaxRequest.setRequestHeader("Content-length", param.length);
		ajaxRequest.setRequestHeader("Connection", "close");
		ajaxRequest.send();	
	}
}	
/************************************************/
function insertPositions(posId, name, order){
	var insertpossql = 'INSERT INTO POSITIONS (positionid, name) VALUES (?,?)';
    var selectpossql = 'SELECT positionid FROM POSITIONS WHERE positionid=(?)';
	db.transaction(function(tx){
		tx.executeSql(selectpossql,[posId],function(tx,results){
			if(results.rows.length==0){
		   		tx.executeSql(insertpossql,[posId, name]);
		   	}
		},errorCB);    	
    }, errorCB, successDB);            	
}
/************************************************/
function getCountriesFromWebService(){
	checkConnection();
	if(networkState!="none" && networkState!="unknown")
	{			

		var url = "http://www.cardsbond.com/BondSys/web/api/v1/countries";
		var ajaxRequest;
		try{
			ajaxRequest = new XMLHttpRequest();
		} catch (e){
			try{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
					//alert("Microsoft");
				} catch (e){
					alert("Your browser broke!");
				return false;
				}
			}
		}
		ajaxRequest.onreadystatechange = function(){
		//alert(ajaxRequest.readyState);
			if(ajaxRequest.readyState == 4){
				var response=ajaxRequest.responseText;
				getLevelTypesFromWebService();

		//		alert(response);
				var results = JSON.parse(response);
					var countryId, name, phoneCode;
//					alert(results.result.length);
				for(var i=0; i<results.result.length;i++){				
					countryId = results.result[i].erp_country_id;
            		name = results.result[i].name;
            		phoneCode = results.result[i].phone_code;            	
            		insertCountries(countryId, name, phoneCode);
            			}
				}
				}
		ajaxRequest.open("GET", url, true);		
		ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		//ajaxRequest.setRequestHeader("Content-length", param.length);
		ajaxRequest.setRequestHeader("Connection", "close");
		ajaxRequest.send();	
	}
}	
/*****************************************************/
function insertCountries(countryId, name, phoneCode){
	var insertCountries = 'INSERT INTO COUNTRIES (countryid,name) VALUES (?,?)';
	var selectcountrysql = 'SELECT countryid FROM COUNTRIES WHERE countryid=(?)';
	db.transaction(function(tx){
		tx.executeSql(selectcountrysql,[countryId],function(tx,results){
			if(results.rows.length==0){
		   		tx.executeSql(insertCountries,[countryId, name]);
		   	}
	   	},errorCB);	
   	}, errorCB, successDB);            	
}

/************************************************/
function getLevelTypesFromWebService(){
var url = "http://cardsbond.com/BondSys/web/api/v1/events/level_types";
var ajaxRequest;
		try{
			ajaxRequest = new XMLHttpRequest();
		} catch (e){
			try{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
					//alert("Microsoft");
				} catch (e){
					alert("Your browser broke!");
				return false;
				}
			}
		}
		ajaxRequest.onreadystatechange = function(){
		//alert(ajaxRequest.readyState);
			if(ajaxRequest.readyState == 4){
				var response=ajaxRequest.responseText;
				getCompaniesFromWebService();
				console.log(response);
				var results = JSON.parse(response);
				var levelId, name;
				for(var i=0; i<results.result.length;i++){				
					levelId = results.result[i].bond_connectiontype_id;
            		name = results.result[i].name;
            		
            		/*if(name=="supplier")
            			familyId = bond_connectiontype_id;
            		else if(name=="customer")
            			friendsId = bond_connectiontype_id;

            		else if(name=="partner")
            			businessId = bond_connectiontype_id;

           			else if(name=="favorite")
            			favoriteId = bond_connectiontype_id;
					*/
           			insertLevels(levelId, name);
            	}
			}
		}
		ajaxRequest.open("GET", url, true);		
		ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		//ajaxRequest.setRequestHeader("Content-length", param.length);
		ajaxRequest.setRequestHeader("Connection", "close");
		ajaxRequest.send();	

}	
/*****************************************************/
function insertLevels(levelId, name){
	var insertCountries = 'INSERT INTO LEVELTYPES (levelid ,levelname) VALUES (?,?)';
	var selectcountrysql = 'SELECT levelid FROM LEVELTYPES WHERE levelid=(?)';
		
	db.transaction(function(tx){
		tx.executeSql(selectcountrysql,[levelId],function(tx,results){
	//		alert(results.rows.length);
			if(results.rows.length==0){
		   		tx.executeSql(insertCountries,[levelId, name]);
		//	alert(countryId+"country id b4 insert");
		   	}
	   	},errorCB);	
   	}, errorCB, successDB);            	
}

function getCompaniesFromWebService(){
	checkConnection();
	if(networkState!="none" && networkState!="unknown")
	{			

		var url = "http://www.cardsbond.com/BondSys/web/api/v1/companies";
		var ajaxRequest;
		try{
			ajaxRequest = new XMLHttpRequest();
		} catch (e){
			try{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
					//alert("Microsoft");
				} catch (e){
					alert("Your browser broke!");
				return false;
				}
			}
		}
		ajaxRequest.onreadystatechange = function(){
			if(ajaxRequest.readyState == 4){
				var response=ajaxRequest.responseText;
				//alert(response);
				getEmailsFromWebService(id, "session");
				if(response.search('error') == -1){
					var results = JSON.parse(response);
					var comId, erp, companyName, logo, field;
					for(var i=0; i<results.result.length;i++){				
						var comId = results.result[i].bond_company_id;
	            		var erp = results.result[i].erp_country_id;
	            		var companyName = results.result[i].name;
	            		var logo = results.result[i].logo;
	            		var field = results.result[i].business_field;
	            		var brief = results.result[i].brief;
	            		var address = results.result[i].address;
	         
	            		insertCompanies(comId, erp, companyName, logo, field, brief, address);
	           		}
           		}
			}
		}
		ajaxRequest.open("GET", url, true);		
		ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		//ajaxRequest.setRequestHeader("Content-length", param.length);
		ajaxRequest.setRequestHeader("Connection", "close");
		ajaxRequest.send();
	}	
}	
/************************************************/
function insertCompanies(companyId, erp, companyName, logo, field, brief, address){	
	var insertcompanysql = 'INSERT INTO SYSTEMCOMPANIES (companyid, name, imgsource, timezone, field, info, address) VALUES (?,?,?,?,?,?,?)';
    var selectcompanysql = 'SELECT companyid FROM SYSTEMCOMPANIES WHERE companyid=(?)';
	db.transaction(function(tx){
		tx.executeSql(selectcompanysql,[companyId],function(tx,results){
			if(results.rows.length==0){
		   		tx.executeSql(insertcompanysql,[companyId, companyName, logo, 0 ,  field, brief, address]);
		   	}
		},errorCB);    	
    }, errorCB, successDB);
}
/************************************************/    