function insertContactsFromWebService()
{
	var url ="http://www.cardsbond.com/BondSys/web/api/v1/events/contacts_connected?bond_contact_id="+id+"&event_id="+event_id;//+"&limit="+;
	console.log("insertContactsFromWebService"+url);
	var ajaxRequest;
	try{
			ajaxRequest = new XMLHttpRequest();
		} catch (e){
			try{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
					//alert("Microsoft");
				} catch (e){
					alert("Your browser broke!");
				return false;
				}
			}
		}
		ajaxRequest.onreadystatechange = function(){
			if(ajaxRequest.readyState == 4){
				var response=ajaxRequest.responseText;
				console.log("mycontactsResponse: "+response+"end response");
				if(response!=""){				
			 		var updates= 'DELETE FROM UPDATESFUNCTIONS WHERE name =(?)';
					db.transaction(function(tx){
					    tx.executeSql(updates,["insertContactsFromWebService"]);
					}, errorCB, successDB);		
	
					if(response.search('error')==-1){	
						contactsResultJson = JSON.parse(response);			
						for(var i=0;i<contactsResultJson.result.length;i++){		
							checkExist(contactsResultJson.result[i].bond_contact_id, i);
				 		}
			 		}
		 			else
		 			{
			 			var errors = JSON.parse(response);
			 			if(errors.error.type=="data not found"){
							window.localStorage.removeItem('id');
							window.localStorage.clear();
							window.localStorage.setItem("id", id);
							window.localStorage.removeItem('lastSync');					
							window.localStorage.setItem("lastSync", new Date());
							window.localStorage.removeItem('update_time_in_secondes');					
							window.localStorage.setItem("update_time_in_secondes", "300000");
				 			window.location="home.html?id="+id;
				 		}	 		
		 			}
	 			}
	 			else{	 			
					console.log("add to updatesfunctions");	
		
				 		var select_updates= 'SELECT * FROM UPDATESFUNCTIONS WHERE name = (?)';	
				 		var updates= 'INSERT INTO UPDATESFUNCTIONS(updateid, name, parameters, datetime) VALUES (?,?,?,?)';
					    var sql_max_updated = "SELECT MAX(updateid) as updateid FROM UPDATESFUNCTIONS";	    
						db.transaction(function(tx){
					    	tx.executeSql(select_updates,["insertContactsFromWebService"],function(tx, rs){
								if(rs.rows.length==0){
									var updatedId = 0, arr1="";  
							    	tx.executeSql(sql_max_updated,[],function(tx, results){
							    		var len = results.rows.length;
							    		if(len>0)
								    		updatedId = results.rows.item(0).updateid;
							    		updatedId= updatedId+1;
							    		arr1 = "";
							    		console.log("arr1"+arr1);
							    		console.log("updated function id "+updatedId);
									    tx.executeSql(updates,[updatedId, "insertContactsFromWebService", arr1, setLastUpdatesTime()]);
					    			},errorCB);		
								}
			    			},errorCB);		
						}, errorCB, successDB);		
					
		 				window.localStorage.removeItem('id');
						window.localStorage.clear();
						window.localStorage.setItem("id", id);
						window.localStorage.removeItem('lastSync');					
						window.localStorage.setItem("lastSync", new Date());
						window.localStorage.removeItem('update_time_in_secondes');					
						window.localStorage.setItem("update_time_in_secondes", "300000");
				 		window.location="home.html?id="+id;

					}									
			}
		}			
		ajaxRequest.open("GET", url, true);
		ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		//ajaxRequest.setRequestHeader("Content-length", param.length);
		ajaxRequest.setRequestHeader("Connection", "close");
		ajaxRequest.send();					
}
/*******************************************************************/
function checkExist(bondId, index)
{
	var checkIfUSerExist = 'SELECT * FROM CONTACTS WHERE contactid=(?)';
	db.transaction(function(ex){
		ex.executeSql(checkIfUSerExist,[bondId],function(ax,res){
	    var conLength = res.rows.length;
	    if(conLength == 0)
	    {
	    	//alert("will return : "+ bondId);
	        insertContacts(index);   			
		}
		else
		{
			if(index==(contactsResultJson.result.length)-1)
			{
				getAllBondedContactsFromDB();
			}
		}         
		}, errorCB);
	}, errorCB, successDB);	
}
/*******************************************************************/
function insertContacts(index)
{
	if(contactsResultJson.result[index].bond_contact_id > max_id )
		max_id = contactsResultJson.result[index].bond_contact_id;
	console.log(max_id+" max_id insertContacts ");

	var contact= 'INSERT INTO CONTACTS (contactid , fname, lname, username, password, companyid, company, positionid, position, imgsource, timezone, address, issocial, behidden, countryid, cityid, aboutme, ispublic, wants, favorite, city_name, country_name) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?)';
	var select_contact= 'SELECT contactid FROM CONTACTS WHERE contactid =(?)';

	var tag= 'INSERT INTO CONTACTSCONNECTIONTAGS (contactid, othercontactid, tags) VALUES (?, ?, ?)';
	var favorite= 'INSERT INTO CONTACTSCONNECTION (contactconnectionid, contactid, othercontactid, type, typevalue) VALUES (?,?,?,?,?)';
	var image="css/imgs/contact-v.png";
	db.transaction(function(tx){
		tx.executeSql(select_contact,[contactsResultJson.result[index].bond_contact_id], function(ax, res){if(res.rows.length==0){		
			if(contactsResultJson.result[index].image!="" && contactsResultJson.result[index].image!="null"){
				image= "http://www.cardsbond.com/"+contactsResultJson.result[index].image;
				var URL = image;
				File_Name = "img"+contactsResultJson.result[index].bond_contact_id;
			    var download_link = encodeURI(URL);
			    //alert(download_link);
			    var ext = download_link.substr(download_link.lastIndexOf('.') + 1); //Get extension of URL
			    var target = Folder_Name + "/" + File_Name + "." + ext; // fullpath and name of the file which we want to give			
			    filetransfer(download_link, target);
			    image=target;											
			}
			else
				image="css/imgs/contact-v.png"
				
	
			var bondId= contactsResultJson.result[index].bond_contact_id, tags= (contactsResultJson.result[index].tags).substring(1, contactsResultJson.result[index].tags.length-1);
		
			var tagsArray =(contactsResultJson.result[index].tags).split(",");
	
			var timezone = 	parseInt(contactsResultJson.result[index].time_zone); 
	
			tx.executeSql(contact,[contactsResultJson.result[index].bond_contact_id,
			contactsResultJson.result[index].fname,
			contactsResultJson.result[index].lname,
			contactsResultJson.result[index].username,
			"",
			contactsResultJson.result[index].bond_company_id,
			contactsResultJson.result[index].company, 
			contactsResultJson.result[index].bond_position_id, 
			contactsResultJson.result[index].position, 
			image, 
			timezone, 
			contactsResultJson.result[index].address,
			contactsResultJson.result[index].be_social,
		 	contactsResultJson.result[index].hide_profile,
			contactsResultJson.result[index].bond_country_id,
			contactsResultJson.result[index].bond_city_id,
			contactsResultJson.result[index].about,
			contactsResultJson.result[index].is_public,
			contactsResultJson.result[index].wants,
			contactsResultJson.result[index].is_favorite,
			contactsResultJson.result[index].city_name,
			contactsResultJson.result[index].country_name
			]);
		
			if((contactsResultJson.result[index].tags).length>0){
				for(var j=0;j<tagsArray.length;j++){
					tx.executeSql(tag,[id,bondId,tagsArray[j]]);
				}
			}
		}
		else
			console.log(contactsResultJson.result[index].bond_contact_id+" exist before");
	}, errorCB);
			
	}, errorCB, function(){
			if(index==(contactsResultJson.result.length)-1)
				{
					//alert("in insert fn"); alwayes call getAllBondedContactsFromDB() from here not from above fn
					console.log("getAllBondedContactsFromDB(contacts)");
					getAllBondedContactsFromDB("contacts");
				}	
	});
}
/****************************************************************************/
function getAllBondedContactsFromDB(flag)
{
    var allBonded= 'SELECT * FROM CONTACTS WHERE contactid!=(?)';
	db.transaction(function(tx){
    	tx.executeSql(allBonded,[id],function(tx,results){
    		var len = results.rows.length;
    		//alert("contacts len: "+len);
    		contactsConnectionLen = len;
    		for(var i=0;i<len;i++){
    			if(flag=="contacts")
	    			contactsLevelFromWebService(results.rows.item(i).contactid, i, results.rows.item(i).favorite);
	    		if(flag=="social")   
	    			attachSocialFromWebService(results.rows.item(i).contactid, "mybonded", "", i);
	    		if(flag=="comments")	
		    		getCommentsFromWebService(results.rows.item(i).contactid, "mybonded"); 		
    		}
	    },errorCB);
    }, errorCB, successDB);
}
/********************************************/
function contactsLevelFromWebService(othercontactid, index)
{
	//alert("othercontactid: "+othercontactid);
	var supplier = "false";
	var customer= "false";
	var producer = "false";
	var favorite="false";
	othercontactid = ''+othercontactid+'';
	var url = "http://www.cardsbond.com/BondSys/web/api/v1/events/contactLevels?id="+id+"&other_id="+othercontactid;

console.log(url);
var ajaxRequest;
		try{
			ajaxRequest = new XMLHttpRequest();
		} catch (e){
			try{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
					//alert("Microsoft");
				} catch (e){
					alert("Your browser broke!");
				return false;
				}
			}
		}
		ajaxRequest.onreadystatechange = function(){
			if(ajaxRequest.readyState == 4){
				var response=ajaxRequest.responseText;
				console.log("other_id="+othercontactid +"&contacts Level: "+response);
	    		if(response!=""){
					if(response.search('error')== -1){
						var results = JSON.parse(response);
						var typeName, contactconnectionid;
						for(var i=0; i<results.result.length;i++){
							typeName = results.result[i].name;
							contactconnectionid = results.result[i].bond_contactconnection_id;
							//alert("typeName"+typeName);
							//alert("contactconnectionid"+othercontactid);
							if(typeName=="supplier")
								supplier="true";
							if(typeName=="customer")
								customer="true";
							if(typeName=="partner")
								producer="true";
							if(typeName=="favorite")
								favorite="true";
							
							checkThenInsertConnection(contactconnectionid, othercontactid, typeName, "true", index);
						}
						
						if(supplier=="false")
							checkThenInsertConnection(contactconnectionid, othercontactid, "supplier", "false", index);
		
						if(customer=="false")
							checkThenInsertConnection(contactconnectionid, othercontactid, "customer", "false", index);
		
						if(producer=="false")
							checkThenInsertConnection(contactconnectionid, othercontactid, "partner", "false", index);
						
						if(favorite=="false")
							checkThenInsertConnection(contactconnectionid, othercontactid, "favorite", "false", index);
	
				}
			}
				else
				{
					/*$.msg({ content : 'error in getting data from server' })*/
				 
				 
				 		var updates= 'INSERT INTO UPDATESFUNCTIONS(updateid, name, parameters, datetime) VALUES (?,?,?,?)';
					    var sql_max_updated = "SELECT MAX(updateid) as updateid FROM UPDATESFUNCTIONS";	    
						db.transaction(function(tx){
							var updatedId = 0, arr1="";  
					    	tx.executeSql(sql_max_updated,[],function(tx, results){
					    		var len = results.rows.length;
					    		if(len>0)
						    		updatedId = results.rows.item(0).updateid;
					    		updatedId= updatedId+1;
					    		arr1 = othercontactid+"|>"+index+"|>"+updatedId;
					    		console.log("arr1"+arr1);
					    		console.log("updated function id "+updatedId);
							    tx.executeSql(updates,[updatedId, "contactsLevelFromWebService", arr1, setLastUpdatesTime()]);
			    			},errorCB);		
						}, errorCB, successDB);		
						///
						if(index==(contactsResultJson.result.length)-1)
							{
								window.localStorage.removeItem('id');
								window.localStorage.clear();
								window.localStorage.setItem("id", id);
								window.localStorage.removeItem('lastSync');					
								window.localStorage.setItem("lastSync", new Date());
								window.localStorage.removeItem('update_time_in_secondes');					
								window.localStorage.setItem("update_time_in_secondes", "300000");
						 		window.location="home.html?id="+id;
							}	

						
						
				}

		}
		}			
		ajaxRequest.open("GET", url, true);		
		ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		//ajaxRequest.setRequestHeader("Content-length", param.length);
		ajaxRequest.setRequestHeader("Connection", "close");
		ajaxRequest.send();	
}
/****************************************************************************/
function checkThenInsertConnection(contactconnectionid, othercontactid, typeName, value, index)
{ 
console.log("in onsert connection "+contactconnectionid);
if(typeName=="Supplier")
	typeName="supplier";

if(typeName=="Customer")
	typeName="customer";

if(typeName=="Partner")
	typeName="partner";

if(typeName=="Favorite")
	typeName="favorite";
var checkIfUSerExist1 = 'SELECT * FROM CONTACTSCONNECTION WHERE contactid=(?) AND othercontactid=(?)';
var checkIfUSerExist2 = 'SELECT * FROM CONTACTSCONNECTIONLEVELS WHERE contactid=(?) AND othercontactid=(?) AND type=(?) AND typevalue=(?)';

var conn1= 'INSERT INTO CONTACTSCONNECTIONLEVELS (contactconnectionid, contactid, othercontactid, type, typevalue) VALUES (?,?,?,?,?)';
var conn2= 'INSERT INTO CONTACTSCONNECTION (contactconnectionid, contactid, othercontactid) VALUES (?,?,?)';
var conn3= 'UPDATE CONTACTSCONNECTION SET contactconnectionid=(?) WHERE contactid = (?) AND othercontactid=(?)';
var conn4= 'UPDATE CONTACTSCONNECTIONLEVELS SET contactconnectionid=(?) AND type=(?) AND typevalue=(?) WHERE contactid = (?) AND othercontactid=(?)';

db.transaction(function(tx){		
    tx.executeSql(checkIfUSerExist2,[id, othercontactid, typeName, value], function(tx, results){
	    if(results.rows.length==0){
	        tx.executeSql(checkIfUSerExist1,[id, othercontactid], function(tx, connresults){if(connresults.rows.length==0){
	       	tx.executeSql(conn1,[contactconnectionid, id, othercontactid, typeName, value]);
	        tx.executeSql(conn2,[contactconnectionid, id, othercontactid]);
	    //    tx.executeSql(conn3,["1", othercontactid]);

	       	} 
	       	else
	       	tx.executeSql(conn3,[contactconnectionid, id, othercontactid]);
	       	tx.executeSql(conn4,[contactconnectionid, typeName, value, id, othercontactid]);

	        },errorCB);                    
	        
	    			}
		
    },errorCB);                    
}, errorCB, function(){
			if((index==(contactsResultJson.result.length)-1) && callSocial=="false")
				{	
					callSocial="true";
					getAllBondedContactsFromDB("social");
				}	
		}
	);
}