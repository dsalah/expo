function requestWebService(results)
{
	console.log("requestWebService");    	
// 	alert("results length: "+results.result.length);
   	var bond_company_id, erp_country_id, name, logo, brief, is_verified, tags, address, bonds_count, field, has_account,
	   	emails, phones, emailsId, phonesId, timezone, socials, socialsId, social_names_id;
				
	for(var i=0; i<results.result.length;i++){
		console.log("fillInMyBondedCompanies i: "+i);				
		bond_company_id = results.result[i].bond_company_id;
	    erp_country_id = results.result[i].erp_country_id;
	    name = results.result[i].name;      
	    logo = results.result[i].logo;      
	    brief = results.result[i].brief;     
	    is_verified = results.result[i].is_verified;      
	    tags = results.result[i].tags;      
	    address = results.result[i].address;      
	    bonds_count = results.result[i].bonds_count; 
	    field = results.result[i].business_fields;
	    has_account = results.result[i].has_account;
				            
	    emailsId = results.result[i].emails_id;
		emails = results.result[i].emails;
		phonesId = results.result[i].phones_id;
		phones = results.result[i].phones;
		socialsId = results.result[i].socials_id;
		socials = results.result[i].socials;
		social_names_id = results.result[i].socials_name_id;
		console.log(bond_company_id+"emails"+emails);
		console.log(bond_company_id+"phones"+phones);
		if(emailsId!="" && emails!=""){
				var emailsIdArray =emailsId.split(","); 
				var emailsArray =emails.split(",");
				for(var j=0; j<emailsIdArray.length;j++){
					console.log("emails "+emailsIdArray[j]+"  "+ bond_company_id+"  "+ emailsArray[j]);
					addEmailsToCompany(emailsIdArray[j], bond_company_id, emailsArray[j], "Business");
				}
		}
		
		if(phonesId!="" && phones!=""){
				var phonesIdArray =phonesId.split(","); 
				var phonesArray =phones.split(",");
				for(var k=0; k<phonesIdArray.length;k++){
					console.log("phones"+phonesIdArray[k]+"  "+ bond_company_id+"   "+ phonesArray[k]);
					addPhonesToCompany(phonesIdArray[k], bond_company_id, phonesArray[k], "Business");
				}
		}
		
		timezone = parseInt(results.result[i].timezone);
		if(logo!="" && logo!="null")
        {
            	logo= "http://www.cardsbond.com/"+results.result[i].logo;
				var URL = logo;
				File_Name = "bondedCompany"+bond_company_id;
		    	var download_link = encodeURI(URL);
		    	//alert("download_link: "+download_link);
		    	var ext = download_link.substr(download_link.lastIndexOf('.') + 1); //Get extension of URL
		    	var target = Folder_Name + "/" + File_Name + "." + ext; // fullpath and name of the file which we want to give			
		    	filetransfer(download_link, target);			    	
					
		    	logo=target;								
            }
            else
            	logo="css/imgs/icons/default-company.png";
            
	            insertBondedCompanies(bond_company_id, name, logo, timezone,  field, brief, address, has_account);
	            attachCompanySocialFromWebService(bond_company_id);
	            getCompanyTags(bond_company_id);
	            getCompanyComments(bond_company_id);

	} 	
}
/***********************************************/    
function insertBondedCompanies(bond_company_id, name, logo, timezone, field, brief, address, has_account)
{
  	console.log("insertBondedCompanies : "+bond_company_id);
   	var insertCompanies= 'INSERT INTO COMPANIES (companyid ,name, imgsource, timezone, field, info, address, has_account) VALUES (?,?,?,?,?,?,?,?)';
   	var bondedCompanies= 'INSERT INTO CONTACTSCOMPANIESCONNECTION (contactid, othercompanyid, type) VALUES (?, ?,?)';
   	var selectCompany = 'SELECT companyid FROM COMPANIES WHERE companyid=(?)';
   	var selectBonded  = 'SELECT * FROM CONTACTSCOMPANIESCONNECTION WHERE contactid=(?) AND othercompanyid=(?) AND type=(?)';


		db.transaction(function(tx){
	   		tx.executeSql(selectCompany,[bond_company_id],function(tx,results){
	   		if(results.rows.length==0){
		   		tx.executeSql(insertCompanies,[bond_company_id, name, logo, timezone, field, brief, address, has_account]);
	   		}
	   		},errorCB);
	   		
	   		tx.executeSql(selectBonded,[id, bond_company_id, "Business"],
	   		function(tx,results){
	   		if(results.rows.length==0){
		   		tx.executeSql(bondedCompanies,[id, bond_company_id, "Business"]);
		   		}	   		
	   		},errorCB);		
	   		
    	}, errorCB, successDB);
}
/************************************************************************/
function fillInMyBondedCompanies() 
{
console.log("fillInMyBondedCompanies");
   	var url = "http://www.cardsbond.com/BondSys/web/api/v1/events/companiesview?id="+id+"&event_id="+event_id;
	var ajaxRequest;
		try{
			ajaxRequest = new XMLHttpRequest();
		} catch (e){
			try{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
					//alert("Microsoft");
				} catch (e){
					alert("Your browser broke!");
				return false;
				}
			}
		}
		ajaxRequest.onreadystatechange = function(){
			if(ajaxRequest.readyState == 4){
					var response=ajaxRequest.responseText;
				console.log("mybondedCompaniesResponse: "+response);
				insertContactsFromWebService();
				if(response!=""){
					if(response.search('error')==-1){
					    var results = JSON.parse(response);					
						requestWebService(results);
					}
				}
			//else
			//	fillInMyBondedCompanies();
			}
		}
		ajaxRequest.open("GET", url, true);		
		ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		//ajaxRequest.setRequestHeader("Content-length", param.length);
		ajaxRequest.setRequestHeader("Connection", "close");
		ajaxRequest.send();	
}	
/***************************************************/
function attachCompanySocialFromWebService(companyId)
{
console.log("attachCompanySocialFromWebService");
var ajaxRequest;
var url ="http://www.cardsbond.com/BondSys/web/api/v1/companies/socialnetworks?id="+companyId;
//alert(url);
		try{
			ajaxRequest = new XMLHttpRequest();
		} catch (e){
			try{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
					//alert("Microsoft");
				} catch (e){
					alert("Your browser broke!");
				return false;
				}
			}
		}
		ajaxRequest.onreadystatechange = function(){
//		alert("req: "+ajaxRequest.readyState);		
			if(ajaxRequest.readyState == 4){
				var response=ajaxRequest.responseText;
//				alert("response: "+response);
				var bond_company_socialnetwork_id, bond_socialnetwork_id, socialnetwork_type,socialnetwork,url, fullPath, companyId;
				if(response.search('error')==-1){
					var results = JSON.parse(response);

			//	alert("no error");
						for(var i=0; i<results.result.length;i++){				
							bond_company_socialnetwork_id = results.result[i].bond_company_socialnetwork_id;
				            socialnetwork_type            = results.result[i].socialnetwork_type;      // "Facebook"
				            socialnetwork                 = results.result[i].company_socialnetwork; //   face/dinsalah 
				            url = results.result[i].url;      //www.facebook.com
				            fullPath= url+"/"+socialnetwork;
				            companyId = results.result[i].bond_company_id;
				   //         alert("calling insertCompanySocialNetwork");
				            insertCompanySocialNetwork(bond_company_socialnetwork_id, companyId, socialnetwork_type, fullPath);
						}			
				}	
				}
				}
		ajaxRequest.open("GET", url, true);		
		ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		//ajaxRequest.setRequestHeader("Content-length", param.length);
		ajaxRequest.setRequestHeader("Connection", "close");
		ajaxRequest.send();
}
/********************************************/
function getCompanyTags(companyId)
{
console.log("getCompanyTags");
var ajaxRequest;
var url ="http://www.cardsbond.com/BondSys/web/api/v1/contacts/bondedcompany_tags?id="+id+"&company_id="+companyId+"&event_id="+event_id;
//alert(url);
		try{
			ajaxRequest = new XMLHttpRequest();
		} catch (e){
			try{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
					//alert("Microsoft");
				} catch (e){
					alert("Your browser broke!");
				return false;
				}
			}
		}
		ajaxRequest.onreadystatechange = function(){
			if(ajaxRequest.readyState == 4){
				var response=ajaxRequest.responseText;
				var bond_company_socialnetwork_id, bond_socialnetwork_id, socialnetwork_type,socialnetwork,url, fullPath, companyId;
				if(response.search('error')==-1){
					var results = JSON.parse(response);
						for(var i=0; i<results.result.length;i++){				
							tagId = results.result[i].bond_contacts_companies_tag_id; 
				            tag = results.result[i].tag;
				            companyId = results.result[i].bond_company_id;
				            addTagToCompany(tagId, tag, companyId);
						}			
				}	
				}
				}
		ajaxRequest.open("GET", url, true);		
		ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		//ajaxRequest.setRequestHeader("Content-length", param.length);
		ajaxRequest.setRequestHeader("Connection", "close");
		ajaxRequest.send();
}

/********************************************/
function getCompanyComments(companyId)
{
console.log("getCompanyComments");
var ajaxRequest;
var url ="http://www.cardsbond.com/BondSys/web/api/v1/contacts/bondedcompany_comments?id="+id+"&company_id="+companyId;
//alert(url);
		try{
			ajaxRequest = new XMLHttpRequest();
		} catch (e){
			try{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
					//alert("Microsoft");
				} catch (e){
					alert("Your browser broke!");
				return false;
				}
			}
		}
		ajaxRequest.onreadystatechange = function(){
			if(ajaxRequest.readyState == 4){
				var response=ajaxRequest.responseText;
				var commentId, comment, companyId,date;
				if(response.search('error')==-1){
					var results = JSON.parse(response);
					for(var i=0; i<results.result.length;i++){				
							commentId = results.result[i].bond_contacts_companies_comment_id; 
				            comment = results.result[i].comment;
				            companyId = results.result[i].bond_company_id;
				            date = results.result[i].date;
				            addCommentToCompany(commentId, comment, companyId, comment);
					}			
				}	
			}
		}
		ajaxRequest.open("GET", url, true);		
		ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		//ajaxRequest.setRequestHeader("Content-length", param.length);
		ajaxRequest.setRequestHeader("Connection", "close");
		ajaxRequest.send();
}

function insertCompanySocialNetwork(bond_company_socialnetwork_id, companyid, socialnetwork_type, fullPath)
{
console.log("insertCompanySocialNetwork");
 //     alert("in insert & select name: "+name);

    	var insertSocial = 'INSERT INTO COMPANYSOCIALNETWORKS (socialid, companyid, socialname, url) VALUES (?, ?, ?,?)';
    	var selectSocial = 'SELECT * FROM COMPANYSOCIALNETWORKS WHERE socialid=(?)';
		db.transaction(function(tx){
	   		tx.executeSql(selectSocial,[bond_company_socialnetwork_id],function(tx,results){
	//   		alert("select comp len: "+results.rows.length);
	   		if(results.rows.length==0){
//	   		alert("b4 insert company social: "+results.rows.length);
		   		tx.executeSql(insertSocial,[bond_company_socialnetwork_id, companyid, socialnetwork_type, fullPath]);
	   		}
	   		},errorCB);
	   				
	   		
    	}, errorCB, successDB);
    
}
/*********************************************************************/
function addTagToCompany(tagId, tagValue, companyid)
{
		var inserttag= 'INSERT INTO COMPANIESCONNECTIONTAGS (tagid, contactid, companyid, tags) VALUES (?, ?, ?, ?)';	
		db.transaction(function(tx){
	    	   	tx.executeSql(inserttag,[tagId, id, companyid, tagValue]);
		}, errorCB, successDB);   
}
/*********************************************************************/
function addCommentToCompany(commentId, commentValue, companyid, time)
{
		var inserttag= 'INSERT INTO COMPANIESCONNECTIONCOMMENTS (commentid, contactid,  companyid, comment, time) VALUES (?, ?, ?, ?, ?)';	
		db.transaction(function(tx){
	    	   	tx.executeSql(inserttag,[commentId, id, companyid, commentValue, time]);
		}, errorCB, successDB);   
}
/*********************************************************************/
function addEmailsToCompany(emailid, companyid, emailvalue, typeid)
{
	var inserttag= 'INSERT INTO COMPANIES_EMAILS (emailid, companyid, emailvalue, typeid) VALUES (?, ?, ?, ?)';	
	db.transaction(function(tx){
    	   	tx.executeSql(inserttag,[emailid, companyid, emailvalue, typeid]);
	}, errorCB, successDB);   
}
/*********************************************************************/
function addPhonesToCompany(emailid, companyid, emailvalue, typeid)
{
	var inserttag= 'INSERT INTO COMPANIES_PHONES (phoneid, companyid, phonevalue, typeid) VALUES (?, ?, ?, ?)';	
	db.transaction(function(tx){
    	   	tx.executeSql(inserttag,[emailid, companyid, emailvalue, typeid]);
	}, errorCB, successDB);   
}