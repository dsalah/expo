function createDB(flag, values)
{
    db.transaction(function(tx){
    	tx.executeSql('DROP TABLE IF EXISTS CONTACTS');
    	tx.executeSql('DROP TABLE IF EXISTS COMPANIES');
    	tx.executeSql('DROP TABLE IF EXISTS SYSTEMCOMPANIES');
    	tx.executeSql('DROP TABLE IF EXISTS CONTACTSOCIALNETWORKS');
    	tx.executeSql('DROP TABLE IF EXISTS COMPANYSOCIALNETWORKS');
    	tx.executeSql('DROP TABLE IF EXISTS CONTACTSCONNECTION');
    	tx.executeSql('DROP TABLE IF EXISTS CONTACTSCONNECTIONLEVELS');	   	
		tx.executeSql('DROP TABLE IF EXISTS CONTACTSCOMPANIESCONNECTION');
		tx.executeSql('DROP TABLE IF EXISTS CONTACTSCONNECTIONTAGS');
		tx.executeSql('DROP TABLE IF EXISTS COMPANIESCONNECTIONCOMMENTS');
		tx.executeSql('DROP TABLE IF EXISTS COMPANIESCONNECTIONTAGS');
		tx.executeSql('DROP TABLE IF EXISTS COMPANIES_EMAILS');		
		tx.executeSql('DROP TABLE IF EXISTS COMPANIES_PHONES');		
		tx.executeSql('DROP TABLE IF EXISTS POSITIONS');
		tx.executeSql('DROP TABLE IF EXISTS COUNTRIES');
		tx.executeSql('DROP TABLE IF EXISTS CITIES');		
		tx.executeSql('DROP TABLE IF EXISTS NOTIFICATIONS');
		tx.executeSql('DROP TABLE IF EXISTS LOGS');
		tx.executeSql('DROP TABLE IF EXISTS CONTACTSCONNECTIONCOMMENT');
		tx.executeSql('DROP TABLE IF EXISTS CONTACTSCONNECTIONCOMMENTFORCOMPANY');
    	tx.executeSql('DROP TABLE IF EXISTS CONTACTTASKS');
		tx.executeSql('DROP TABLE IF EXISTS INVITATION');
		tx.executeSql('DROP TABLE IF EXISTS SHARING');
		tx.executeSql('DROP TABLE IF EXISTS INVITATIONCONNECTIONSLEVEL');
		tx.executeSql('DROP TABLE IF EXISTS PHONES');
		tx.executeSql('DROP TABLE IF EXISTS EMAILS');
		tx.executeSql('DROP TABLE IF EXISTS MOBILES');
		tx.executeSql('DROP TABLE IF EXISTS FAXES');
		tx.executeSql('DROP TABLE IF EXISTS FAXETYPE');
		tx.executeSql('DROP TABLE IF EXISTS MOBILETYPE');
		tx.executeSql('DROP TABLE IF EXISTS PHONESTYPE');
		tx.executeSql('DROP TABLE IF EXISTS EMAILTYPE');
		tx.executeSql('DROP TABLE IF EXISTS MYCOMPANYCONTATCS');
		tx.executeSql('DROP TABLE IF EXISTS PUBLICBONDCONTACTS');
    	tx.executeSql('DROP TABLE IF EXISTS LEVELTYPES');
    	tx.executeSql('DROP TABLE IF EXISTS UPDATESFUNCTIONS');

  		tx.executeSql('CREATE TABLE IF NOT EXISTS UPDATESFUNCTIONS(updateid, name, parameters, datetime)');            
  		tx.executeSql('CREATE TABLE IF NOT EXISTS LEVELTYPES(levelid, levelname)');            
  		tx.executeSql('CREATE TABLE IF NOT EXISTS COMPANIESCONNECTIONCOMMENTS (commentid INTEGER, contactid,  companyid, comment, time)');
  		tx.executeSql('CREATE TABLE IF NOT EXISTS PUBLICBONDCONTACTS(contactid, othercontactid, fname, lname, company, position)');            
  		tx.executeSql('CREATE TABLE IF NOT EXISTS COMPANIESCONNECTIONTAGS (tagid INTEGER, contactid, companyid, tags)');            
 		tx.executeSql('CREATE TABLE IF NOT EXISTS COMPANIES_EMAILS(emailid, companyid, emailvalue, typeid)'); 
  		tx.executeSql('CREATE TABLE IF NOT EXISTS COMPANIES_PHONES(phoneid, companyid, phonevalue, typeid)'); 

  		tx.executeSql('CREATE TABLE IF NOT EXISTS PHONES(phoneid, contactid, phonevalue, typeid)');            

  		tx.executeSql('CREATE TABLE IF NOT EXISTS EMAILS(emailid, contactid, emailvalue, typeid)');            

  		tx.executeSql('CREATE TABLE IF NOT EXISTS MOBILES(mobileid, contactid, mobilevalue, typeid)');            

  		tx.executeSql('CREATE TABLE IF NOT EXISTS FAXES(faxid, contactid, faxvalue, typeid)');            

  		tx.executeSql('CREATE TABLE IF NOT EXISTS INVITATION (invitaionid, contactid, othercontactid, name, company_name, position_name, image, tags, comments)');            

  		tx.executeSql('CREATE TABLE IF NOT EXISTS SHARING (sharing_id, contactid, othercontactid, name, tags, comments, sender_name, sender_id)');            

  		tx.executeSql('CREATE TABLE IF NOT EXISTS INVITATIONCONNECTIONSLEVEL (invitaionid, type)');            

        tx.executeSql('CREATE TABLE IF NOT EXISTS CONTACTS (contactid ,fname, lname, username, password, companyid, company, positionid, position, imgsource, timezone, address, issocial, behidden, countryid, cityid, aboutme, ispublic, wants, favorite, qrimage, city_name, country_name)');

        tx.executeSql('CREATE TABLE IF NOT EXISTS COMPANIES (companyid, name, imgsource, timezone, field, info, address, has_account, last_updated_by_id, last_updated_by_name, city_id, country_id)');

        tx.executeSql('CREATE TABLE IF NOT EXISTS SYSTEMCOMPANIES (companyid, name, imgsource, timezone, field, info, address, has_account)');

        tx.executeSql('CREATE TABLE IF NOT EXISTS MYCOMPANYCONTATCS (contactid ,fname, lname, username, password, companyid, company, positionid, position, imgsource, timezone, address, issocial, behidden, countryid, cityid, aboutme, ispublic, wants, country_name, favorite, sort_counter INTEGER)');

        tx.executeSql('CREATE TABLE IF NOT EXISTS COMPANYSOCIALNETWORKS (socialid, companyid, socialname, url, typeid)');            

        tx.executeSql('CREATE TABLE IF NOT EXISTS CONTACTSOCIALNETWORKS (socialid, contactid, socialname, url, typeid)');            

        tx.executeSql('CREATE TABLE IF NOT EXISTS CONTACTSCONNECTION (contactconnectionid, contactid, othercontactid, type, typevalue)');            

        tx.executeSql('CREATE TABLE IF NOT EXISTS CONTACTSCONNECTIONLEVELS (contactconnectionid, contactid, othercontactid, type, typevalue)');            

  		tx.executeSql('CREATE TABLE IF NOT EXISTS CONTACTSCOMPANIESCONNECTION (contactid, othercompanyid, type)');            
        
  		tx.executeSql('CREATE TABLE IF NOT EXISTS CONTACTSCONNECTIONTAGS (tagid, contactid, othercontactid, tags)');            

        tx.executeSql('CREATE TABLE IF NOT EXISTS POSITIONS (positionid, name)');

        tx.executeSql('CREATE TABLE IF NOT EXISTS COUNTRIES (countryid, name)');

        tx.executeSql('CREATE TABLE IF NOT EXISTS CITIES (cityid, name, erpcountry)');

        tx.executeSql('CREATE TABLE IF NOT EXISTS NOTIFICATIONS (contactid, bond_updatenotification_id, name, description)');
      
        tx.executeSql('CREATE TABLE IF NOT EXISTS CONTACTSCONNECTIONCOMMENT (commentid INTEGER, contactid,  othercontactid, comment, time, event_id)');
      	tx.executeSql('CREATE TABLE IF NOT EXISTS CONTACTSCONNECTIONCOMMENTFORCOMPANY (commentid INTEGER, contactid,  othercontactid, comment, time, companyid)');
      
      
        tx.executeSql('CREATE TABLE IF NOT EXISTS LOGS (logid, contactid, othercontactid, logtype, calltime)');
      	
    	}, errorCB, successCB);
		
      if(flag=="login"){
      		callAPI(values);
      	}
      
      if(flag=="signup")
	  	window.location="login.html";
	}
/***************************************************************************************/
function errorCB(err) {
	alert("Error processing SQL: index "+err.code);
}
/***************************************************************************************/
function successCB() {
//  window.location="index.html";        
}     
/***********************************************************/
function callAPI(values)
{	
console.log("callAPI");
	var url = values[0];
	var username = values[1];
	var password = values[2];
	var ajaxRequest;
	try{
		ajaxRequest = new XMLHttpRequest();
	} catch (e){
		try{
			ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try{
				ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
				//alert("Microsoft");
			} catch (e){
				alert("Your browser broke!");
				return false;
			}
		}
	}	
		ajaxRequest.onreadystatechange = function(){
			if(ajaxRequest.readyState == 4 && ajaxRequest.status==200){
				var response=ajaxRequest.responseText;
				console.log("response: "+response);
					
					var results = "";
					if(response.search('error') ==-1 ){
						results = JSON.parse(response);
	
						id = results.result;					
						if(id!=0){
							if(id== -1){$.msg({ content : 'Please activate your account' }); 
								document.getElementById('username').removeAttribute('disabled','');
							document.getElementById('password').removeAttribute('disabled','');
							document.getElementById('login_Button').removeAttribute('disabled','');
							document.getElementById('login_Button').setAttribute('onClick','getLoginParam()');
							document.getElementById('register-btn').removeAttribute('disabled','');
							document.getElementById('register-btn').setAttribute('onClick',"window.location='signup.html';");
							document.getElementById('forget-password-link').setAttribute('href','forgetPassword.html');
						
							once="true";
							var my = document.getElementById('loginDiv');
							var child = document.getElementById('loadingImage');0
							if(my)
								my.removeChild(child);
		    	
							
							}
							else{
								$.msg({ content : 'Please wait while loading your data' });	 								
								getCitiesFromWebService();
							}
						}
						else
						{							
							$.msg({ content : 'Invalid username or password' });
							document.getElementById('username').removeAttribute('disabled','');
							document.getElementById('password').removeAttribute('disabled','');
							document.getElementById('login_Button').removeAttribute('disabled','');
							document.getElementById('login_Button').setAttribute('onClick','getLoginParam()');
							document.getElementById('register-btn').removeAttribute('disabled','');
							document.getElementById('register-btn').setAttribute('onClick',"window.location='signup.html';");
							document.getElementById('forget-password-link').setAttribute('href','forgetPassword.html');
						
							once="true";
							var my = document.getElementById('loginDiv');
							var child = document.getElementById('loadingImage');0
							if(my)
								my.removeChild(child);
		    			}
	    			}
	    		}
	    										
			}
		ajaxRequest.open("GET", url, true);		
		ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		//ajaxRequest.setRequestHeader("Content-length", param.length);
		ajaxRequest.setRequestHeader("Connection", "close");
		ajaxRequest.send();		
	}
/********************************************/
