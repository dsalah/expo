function insertUserData()
{
	var url = "http://www.cardsbond.com/BondSys/web/api/v1/contacts?id="+id;
	console.log("insertUserData"+url);

	var ajaxRequest;
	try{
		ajaxRequest = new XMLHttpRequest();
	} catch (e){
		try{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
					//alert("Microsoft");
				} catch (e){
					alert("Your browser broke!");
				return false;
				}
			}
		}
		ajaxRequest.onreadystatechange = function(){
			if(ajaxRequest.readyState == 4){
				var response=ajaxRequest.responseText;
				if(response.search('error') == -1){
					console.log("user data response"+response);
					var results = JSON.parse(response);							
					var insertcontact= 'INSERT INTO CONTACTS (contactid , fname, lname, username, password, companyid, company, positionid, position, imgsource, timezone, address, issocial, behidden, countryid, cityid, aboutme, ispublic, wants, qrimage, city_name, country_name) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?)';
					var selectContact= 'SELECT contactid FROM CONTACTS WHERE contactid=(?)';
					db.transaction(function(tx){
					tx.executeSql(selectContact,[id],function(tx,res){
						if(res.rows.length==0){
							var image="";
							if(results.result[0].image!="" && results.result[0].image!="null"){
								image= "http://www.cardsbond.com/"+results.result[0].image;
								var URL = image;
								File_Name = "profile_"+id;
							    var download_link = encodeURI(URL);
							    //alert("download_link: "+download_link);
							    var ext = download_link.substr(download_link.lastIndexOf('.') + 1); //Get extension of URL
							    var target = Folder_Name + "/" + File_Name + "." + ext; // fullpath and name of the file which we want to give			
							    filetransfer(download_link, target);
							    image=target;								
							}
							else
								image="css/imgs/my-default.png";
							if(results.result[0].qr_image!="" && results.result[0].qr_image!="null"){
								qr_image= "http://www.cardsbond.com/"+results.result[0].qr_image;
								var URL = qr_image;
								var qr_File_Name = "qr_image"+id;
							    var download_link = encodeURI(URL);
							    var ext = "png"; //Get extension of URL
							    var target =  Folder_Name + "/"+qr_File_Name + "." + ext; // fullpath and name of the file which we want to give			
							    
							    filetransfer(download_link, target);
							    qr_image=target;								
							}
							else
								qr_image="";
						
								var timezone = parseInt(results.result[0].time_zone);
							tx.executeSql(insertcontact,[
								results.result[0].bond_contact_id,
								results.result[0].fname,
								results.result[0].lname,
								results.result[0].username,
								results.result[0].password,
								results.result[0].bond_company_id,
								results.result[0].company_name, 
								results.result[0].bond_position_id, 
								results.result[0].position_name, 
								image, timezone, 
								results.result[0].address,
								results.result[0].be_social,
							 	results.result[0].hide_profile,
								results.result[0].bond_country_id,
								results.result[0].bond_city_id,
								results.result[0].about,
								results.result[0].is_public,
								results.result[0].wants,
								qr_image,
								results.result[0].city_name,
								results.result[0].country_name
								]);
								
						        attachSocialFromWebService(results.result[0].bond_contact_id, "session", results.result[0].bond_company_id, "");
								//showNotificationFromServer(results.result[0].bond_contact_id);// hidden in events version only
								showInvitationListFromServer();//showInvitationListFromServer(results.result[0].bond_contact_id);
								}
							},errorCB);		
					}, errorCB, successDB);   											
			}
		}
	}
	ajaxRequest.open("GET", url, true);		
	ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	//ajaxRequest.setRequestHeader("Content-length", param.length);
	ajaxRequest.setRequestHeader("Connection", "close");
	ajaxRequest.send();		
}
/************************************************/
function fillInMyCompany(mycompanyid)
{
  //  alert("mycompanyid: "+mycompanyid);
	var url = "http://www.cardsbond.com/BondSys/web/api/v1/companies?id="+mycompanyid;
	var ajaxRequest;
		try{
			ajaxRequest = new XMLHttpRequest();
		} catch (e){
			try{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
					//alert("Microsoft");
				} catch (e){
					alert("Your browser broke!");
				return false;
				}
			}
		}
		ajaxRequest.onreadystatechange = function(){
			if(ajaxRequest.readyState == 4){
				var response=ajaxRequest.responseText;
				attachMyCompanyContactsFromServer(mycompanyid);
				
		//		alert("my company"+response);
				if(response.search('error')==-1){
    	
				var results = JSON.parse(response);
				var comId = results.result[0].bond_company_id;
            	var erp = results.result[0].erp_country_id;
            	var companyName = results.result[0].name;
            	var logo = results.result[0].logo;
            	var field = results.result[0].business_fields;
            	var brief = results.result[0].brief;
            	var address = results.result[0].address;
            	var timezone = parseInt(results.result[0].timezone);

				var has_account = results.result[0].has_account;
				var last_updated_by_id = results.result[0].last_updated_by_id;
				var last_updated_by_name = results.result[0].last_updated_by_fname+" "+results.result[0].last_updated_by_lname;
            	if(logo!="")
            	{
					logo= "http://www.cardsbond.com/"+results.result[0].logo;
					var URL = logo;
					File_Name = "mycompany"+comId+id;
		    		var download_link = encodeURI(URL);
		    		//alert("download_link: "+download_link);
		    		var ext = download_link.substr(download_link.lastIndexOf('.') + 1); //Get extension of URL
		    		var target = Folder_Name + "/" + File_Name + "." + ext; // fullpath and name of the file which we want to give			
		    		filetransfer(download_link, target);
		    		logo=target;								
		    	//	alert("my company logo: "+logo);
		    										
				}
            	else            	
            		logo = "css/imgs/icons/default-company.png";
            		
	var insertcompanysql = 'INSERT INTO COMPANIES (companyid, name, imgsource, timezone, field, info, address, has_account, last_updated_by_id, last_updated_by_name) VALUES (?,?,?,?,?,?,?,?,?,?)';
    var selectCompany = 'SELECT * FROM COMPANIES WHERE companyid=(?)';
			db.transaction(function(tx){
	   		tx.executeSql(selectCompany,[comId],function(tx,res){
	   		if(res.rows.length==0){
	            tx.executeSql(insertcompanysql,[comId, companyName, logo, timezone , field, brief, address, has_account, last_updated_by_id,  last_updated_by_name]);
	   		}
	   		},errorCB);

    	}, errorCB, successDB);
            	}
				}
				}
		ajaxRequest.open("GET", url, true);		
		ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		//ajaxRequest.setRequestHeader("Content-length", param.length);
		ajaxRequest.setRequestHeader("Connection", "close");
		ajaxRequest.send();	
}
/********************************************************/
function attachMyCompanyContactsFromServer(myCompany_id)
{
console.log("attachMyCompanyContactsFromServer");
var url  ="http://www.cardsbond.com/BondSys/web/api/v1/contacts/my_company_contacts?company_id="+myCompany_id+"&bond_contact_id="+id+
	"&limit="+my_company_contacts_limit+"&max_id="+my_company_contacts_max_id;
	var ajaxRequest;
		try{
			ajaxRequest = new XMLHttpRequest();
		} catch (e){
			try{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
					//alert("Microsoft");
				} catch (e){
					alert("Your browser broke!");
				return false;
				}
			}
		}
		
		ajaxRequest.onreadystatechange = function(){
			if(ajaxRequest.readyState == 4 && ajaxRequest.status==200){
				var response=ajaxRequest.responseText;
				fillInMyBondedCompanies();
					var results = "", image="";
					if(response.search('error') == -1 ){
						var results = JSON.parse(response);
						var i=0, mycompany_contact_length = results.result.length;
						if(results.result.length>100)
							mycompany_contact_length = 100;
							console.log("contactsLength: "+mycompany_contact_length);
							while(i<mycompany_contact_length){
								console.log("i"+i);
								console.log("id:"+ results.result[i].bond_contact_id);
								if(results.result[i].bond_contact_id != id){
									image="css/imgs/contact-v.png";
									if(results.result[i].image!="" && results.result[i].image!="null"){
										image= "http://www.cardsbond.com/"+results.result[i].image;
										var URL = image;
										File_Name = "img"+results.result[i].bond_contact_id;
									    var download_link = encodeURI(URL);
									    //alert(download_link);
									    var ext = download_link.substr(download_link.lastIndexOf('.') + 1); //Get extension of URL
									    var target = Folder_Name + "/" + File_Name + "." + ext; // fullpath and name of the file which we want to give			
									    filetransfer(download_link, target);
									    image= target;											
									}
									else
										image="css/imgs/contact-v.png"
									if(parseInt(results.result[i].bond_contact_id) > my_company_contacts_max_id)
										my_company_contacts_max_id = results.result[i].bond_contact_id;
									console.log("my company max id"+my_company_contacts_max_id);
																
									insertIntoMyCompanyNewContacts(results.result[i].bond_contact_id , results.result[i].fname, results.result[i].lname,
									 results.result[i].username, "", results.result[i].bond_company_id, results.result[i].company, results.result[i].bond_position_id, 
									 results.result[i].position, image, parseInt(results.result[i].time_zone), results.result[i].address, 
									 results.result[i].be_social, results.result[i].hide_profile, results.result[i].bond_country_id, 
									 results.result[i].bond_city_id, results.result[i].about, results.result[i].is_public, results.result[i].wants,results.result[i].country_name, 
									 "", i, mycompany_contact_length);
									 
								}
							i++;

						}	
	    			}
	    		}	    										
			}
		ajaxRequest.open("GET", url, true);		
		ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		//ajaxRequest.setRequestHeader("Content-length", param.length);
		ajaxRequest.setRequestHeader("Connection", "close");
		ajaxRequest.send();		
}
/************************************************************************/
function insertIntoMyCompanyNewContacts(contactid , fname, lname, username, password, companyid, company, positionid, position, imgsource, timezone, address, issocial, behidden, countryid, cityid, aboutme, ispublic, wants, country_name, favorite, i, mycompany_contact_length)
{
	var insertcontact = 'INSERT INTO MYCOMPANYCONTATCS (contactid , fname, lname, username, password, companyid, company, positionid, position, imgsource, timezone, address, issocial, behidden, countryid, cityid, aboutme, ispublic, wants, favorite, sort_counter, country_name) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?)';
    var selectcontact = 'SELECT contactid FROM MYCOMPANYCONTATCS WHERE contactid=(?)';
	db.transaction(function(tx){
		tx.executeSql(selectcontact,[contactid],function(tx,results){
			if(results.rows.length==0){
		   		tx.executeSql(insertcontact,[contactid , fname, lname, username, password, companyid, company, positionid, position, imgsource, timezone, address, issocial, behidden, countryid, cityid, aboutme, ispublic, wants, country_name, favorite, 0]);
				 //getMyCompanyContactsEmailsFromWebService(contactid);

		   		console.log("inserted"+i);
		   	}
		},errorCB);
    }, errorCB, function(){
    /*if(i==(mycompany_contact_length-1))
    	selectFromMyCompanyNewContacts();
    */});
}
/********************************************************************************/
function selectFromMyCompanyNewContacts()
{
	var sqltxt= 'SELECT * FROM MYCOMPANYCONTATCS';
	db.transaction(function(tx){
    	tx.executeSql(sqltxt,[],function(tx, results){
  			if(results.rows.length==0){         
  			for(var i=0; i<results.rows.length ; i++)
           		getMyCompanyContactsEmailsFromWebService(results.rows.item(0).contactid);
           	}
        },errorCB);
    }, errorCB, successDB);   
}