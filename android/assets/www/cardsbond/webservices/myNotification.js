function showNotificationFromServer(bond_id){
console.log("showNotificationFromServer");	
 	var url = "http://www.cardsbond.com/BondSys/web/api/v1/contacts/get_update_notifications?bond_contact_id="+bond_id;
	var ajaxRequest;
		try{
			ajaxRequest = new XMLHttpRequest();
		} catch (e){
			try{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
					//alert("Microsoft");
				} catch (e){
					alert("Your browser broke!");
				return false;
				}
			}
		}

		ajaxRequest.onreadystatechange = function(){
			if(ajaxRequest.readyState == 4){
					var response=ajaxRequest.responseText;
					//alert(response);					
					if(response.search('error')== -1){
					var results = JSON.parse(response);
						for(var i=0; i<results.length;i++){
							var notify_id =  results[i].bond_updatenotification_id;
							var name = results[i].name;
							var description = results[i].description;
							//alert("from server"+notify_id);
							insertNotificationsFromerver(notify_id, name, description, bond_id);
		            	}
	            	}

				}
			}
		ajaxRequest.open("GET", url, true);
		ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		//ajaxRequest.setRequestHeader("Content-length", param.length);
		ajaxRequest.setRequestHeader("Connection", "close");
		ajaxRequest.send();
}
/******************************************************************************/
function insertNotificationsFromerver(notify_id, name, description, bond_id)
{
	var insertNotification = 'INSERT INTO NOTIFICATIONS (contactid, bond_updatenotification_id, name, description) VALUES (?, ?, ?, ?)';
    var selectNotification = 'SELECT bond_updatenotification_id FROM NOTIFICATIONS WHERE contactid=(?) AND bond_updatenotification_id=(?)';
	db.transaction(function(tx){
		tx.executeSql(selectNotification,[bond_id, notify_id],function(tx,results){
			if(results.rows.length==0){
		   		tx.executeSql(insertNotification,[bond_id, notify_id, name, description]);
		   	}
		},errorCB);
    }, errorCB, successDB);
}
/********************************************************************************/
function showInvitationListFromServer(){
 	var url = "http://www.cardsbond.com/BondSys/web/api/v1/events/get_invitation_list?id="+id;
	var ajaxRequest;
		try{
			ajaxRequest = new XMLHttpRequest();
		} catch (e){
			try{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
					//alert("Microsoft");
				} catch (e){
					alert("Your browser broke!");
				return false;
				}
			}
		}

		ajaxRequest.onreadystatechange = function(){
			if(ajaxRequest.readyState == 4){
					var response=ajaxRequest.responseText;
					//alert(response);

					if(response.search('error')== -1){
					var results = JSON.parse(response);
						for(var i=0; i<results.result.length;i++){
							var fname = results.result[i].fname;
							var lname = results.result[i].lname;
							var otherbondid = results.result[i].bond_contact_id;
							var invitationId = results.result[i].bond_connectioninvitation_id;
							var invitedName = fname+' '+lname;
							var image="";
							if(results.result[i].image!="" && results.result[i].image!="null"){
								var File_Name = "profile_"+id;
							    var download_link = encodeURI("http://www.cardsbond.com/"+results.result[0].image);
							    //alert("download_link: "+download_link);
							    var ext = download_link.substr(download_link.lastIndexOf('.') + 1); //Get extension of URL
							    var target = Folder_Name + "/" + File_Name + "." + ext; // fullpath and name of the file which we want to give			
							    filetransfer(download_link, target);
							    image=target;								
							}
							else
								image="css/imgs/contact-v.png";

							//alert("invitedName: "+invitedName);
							insertInvitationFromServer(invitedName, otherbondid, invitationId, id, results.result[i].company, results.result[i].position, image);
		            	}
	            	}
				}
			}
		ajaxRequest.open("GET", url, true);
		ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		//ajaxRequest.setRequestHeader("Content-length", param.length);
		ajaxRequest.setRequestHeader("Connection", "close");
		ajaxRequest.send();
 }
 /************************************************************************/
function insertInvitationFromServer(name, othercontactid, invitationid, bond_id, company_name, position_name, image)
{
	var insertNotification = 'INSERT INTO INVITATION (invitaionid, contactid, othercontactid, name, company_name, position_name, image, tags, comments) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)';
    var selectNotification = 'SELECT invitaionid FROM INVITATION WHERE contactid=(?) AND invitaionid=(?)';
	db.transaction(function(tx){
		tx.executeSql(selectNotification,[bond_id, invitationid],function(tx,results){
			if(results.rows.length==0){
		   		tx.executeSql(insertNotification,[invitationid, bond_id, othercontactid, name, company_name, position_name, image, "", ""]);     													 
		   	}
		},errorCB);
    }, errorCB, successDB);

}