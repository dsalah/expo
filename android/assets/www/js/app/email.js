function addNewEmailInput(x)
{
	if(emaildbclick%2!=0){
		var l = dummyEmailCounter+1;
	var emailIdx = 'i_plus_email'+x+id;
	if(document.getElementById(emailIdx) != null)
		document.getElementById(emailIdx).setAttribute('class','fa fa fa-envelope');
		
		var emailspan = document.createElement('span');
		emailspan.setAttribute('class','nforom-input');
		emailspan.setAttribute('style','margin:20px; line-height:12px;');  

		
		var emailinput = document.createElement('input');
		emailinput.setAttribute('type','text');
		emailinput.setAttribute('id','emailphone'+l+id);
		emailinput.setAttribute('value','');

		emailinput.setAttribute('placeholder','');
		var iconsId = "email_con_type"+l;
		
		//emailinput.setAttribute("onfocus", "showIcons("+iconsId+")");
		//emailinput.setAttribute("onblur" , "hideIcons("+iconsId+")");
		emailspan.appendChild(emailinput);
		var hidinput = document.createElement('input');
		hidinput.setAttribute('type','hidden');
		hidinput.setAttribute('id','emailphonehidden'+l+id);
		hidinput.setAttribute('value', '');
		emailspan.appendChild(hidinput);

		var emaili = document.createElement('i');
		var emailId = 'i_plus_email'+l+id;
		emaili.setAttribute('id',emailId);

		emaili.setAttribute('class','fa fa fa-plus fa-lg');	
		emaili.setAttribute('onClick','addNewEmailInput('+l+')');  
					
		emailspan.appendChild(emaili);

		var emaildiv = document.createElement('div');
			emaildiv.setAttribute('class','hide input-icons');
			emaildiv.setAttribute('id',iconsId);
			
			var emaila1 = document.createElement('a');
			emaila1.setAttribute('href','#');
			
			var emaila2 = document.createElement('a');
			emaila2.setAttribute('href','#');

			var emaila3 = document.createElement('a');
			emaila3.setAttribute('href','#');

			var emaila4 = document.createElement('a');
			emaila4.setAttribute('href','#');


			var friendEmailImgId = 'friendemail'+l; 
			var emailimg1 = document.createElement('img');
				emailimg1.setAttribute('src','css/imgs/icons/friends-in.png');

			emailimg1.setAttribute('class','edit-icons-friend');
			emailimg1.setAttribute('id', friendEmailImgId);
			emailimg1.addEventListener('click',function (e){
			  click_time = e['timeStamp'];	
			  if (click_time && (click_time - last_click_time) < 2000)
			  {
			    e.stopImmediatePropagation();
			    e.preventDefault();
			    return false;
			    }
			    else
			    {
					last_click_time = click_time;
					navigator.notification.vibrate(100);					
					toggleImg(friendEmailImgId,'friends');			
				}
			});
			
			var familyEmailImgId = 'familyemail'+l;
			var emailimg2 = document.createElement('img');
			emailimg2.setAttribute('src','css/imgs/icons/family-in.png');
			
			emailimg2.setAttribute('class','edit-icons-fam');
			emailimg2.setAttribute('id', familyEmailImgId);
			emailimg2.addEventListener('click',function (e){
			  click_time = e['timeStamp'];	
			  if (click_time && (click_time - last_click_time) < 2000)
			  {
	//		  	alert("time btw click is small");
			    e.stopImmediatePropagation();
			    e.preventDefault();
			    return false;
			    }
			    else
			    {
					last_click_time = click_time;
					navigator.notification.vibrate(100);
					toggleImg(familyEmailImgId,'family');
			
				}
			});
						
			var businessEmailImgId = 'businessemail'+l;
			var emailimg3 = document.createElement('img');
			emailimg3.setAttribute('src','css/imgs/icons/business-in.png');
			emailimg3.setAttribute('class','edit-icons-bus');
			emailimg3.setAttribute('id', businessEmailImgId);			
			emailimg3.addEventListener('click',function (e){
			  click_time = e['timeStamp'];	
			  if (click_time && (click_time - last_click_time) < 2000)
			  {
	//		  	alert("time btw click is small");
			    e.stopImmediatePropagation();
			    e.preventDefault();
			    return false;
			    }
			    else
			    {
					last_click_time = click_time;
					navigator.notification.vibrate(100);
					toggleImg(businessEmailImgId,'business');
			
				}
			});
			
						
			var publicEmailImgId = 'publicemail'+l;			
			var emailimg4 = document.createElement('img');
			emailimg4.setAttribute('src','css/imgs/icons/public-in.png');

			emailimg4.setAttribute('class','edit-icons-bus');
			emailimg4.setAttribute('id', publicEmailImgId);			
			
			emailimg4.addEventListener('click',function (e){
			  click_time = e['timeStamp'];	
			  if (click_time && (click_time - last_click_time) < 2000)
			  {
	//		  	alert("time btw click is small");
			    e.stopImmediatePropagation();
			    e.preventDefault();
			    return false;
			    }
			    else
			    {
					last_click_time = click_time;
					navigator.notification.vibrate(100);
					toggleImg(publicEmailImgId,'public');
			
				}
			});
			
			
			emaila1.appendChild(emailimg1);
			emaila2.appendChild(emailimg2);
			emaila3.appendChild(emailimg3);
			emaila4.appendChild(emailimg4);
			
			
			emaildiv.appendChild(emaila1);
			emaildiv.appendChild(emaila2);
			emaildiv.appendChild(emaila3);
			emaildiv.appendChild(emaila4);  
			emailspan.appendChild(emailinput);
			emailspan.appendChild(emaildiv);

			var br = document.createElement('br');
                      
		
		//	alert("array: "+phonesArr[l]);
			
		document.getElementById("emailDiv").appendChild(emailspan);			
		document.getElementById("emailDiv").appendChild(br);			
		dummyEmailCounter++;
	}
	emaildbclick++;
}
////////////////////////////////////////////
function showEmailDiv(l)
{
//			alert("adding email with hidden id: "+emailsArrid[l]);
			var emailspan = document.createElement('span');
			emailspan.setAttribute('class','nforom-input');
			emailspan.setAttribute('style','margin:20px; line-height:12px;');  

		
			var emailinput = document.createElement('input');
			emailinput.setAttribute('type','text');
			emailinput.setAttribute('id','emailphone'+l+id);
			emailinput.setAttribute('value',emailsArr[l]);

			emailinput.setAttribute('placeholder','email@example.com');
			var iconsId = "email_con_type"+l;
		
			//emailinput.setAttribute("onfocus", "showIcons("+iconsId+")");
			//emailinput.setAttribute("onblur" , "hideIcons("+iconsId+")");
			emailspan.appendChild(emailinput);
			var hidinput = document.createElement('input');
			hidinput.setAttribute('type','hidden');
			hidinput.setAttribute('id','emailphonehidden'+l+id);
			hidinput.setAttribute('value', emailsArrid[l]);
			emailspan.appendChild(hidinput);

			var emaili = document.createElement('i');
			emaili.setAttribute('class','fa fa fa-plus fa-lg');				
			var emailId = 'i_plus_email'+l+id;
			emaili.setAttribute('id',emailId);
			if(l==emailsArrCounter){
			emaili.setAttribute('class','fa fa fa-plus fa-lg');	
			var emailiElement = document.getElementById(emailId);
			emaili.setAttribute('onClick',"addNewEmailInput("+l+");");			  					
			}
			else
			{
			emaili.setAttribute('class','fa fa fa-envelope');	
			}
			emailspan.appendChild(emaili);


			var emaildiv = document.createElement('div');
			emaildiv.setAttribute('class','hide input-icons');
			emaildiv.setAttribute('id',iconsId);
			
			var emaila1 = document.createElement('a');
			emaila1.setAttribute('href','#');
			
			var emaila2 = document.createElement('a');
			emaila2.setAttribute('href','#');

			var emaila3 = document.createElement('a');
			emaila3.setAttribute('href','#');

			var emaila4 = document.createElement('a');
			emaila4.setAttribute('href','#');


			var friendEmailImgId = 'friendemail'+l; 
			var emailimg1 = document.createElement('img');
			if(emailstypeArr[l].search(friendsId) == -1)
				emailimg1.setAttribute('src','css/imgs/icons/friends-in.png');
			else
				emailimg1.setAttribute('src','css/imgs/icons/friends.png');

			emailimg1.setAttribute('class','edit-icons-friend');
			emailimg1.setAttribute('id', friendEmailImgId);
			emailimg1.addEventListener('click',function (e){
			  click_time = e['timeStamp'];	
			  if (click_time && (click_time - last_click_time) < 2000)
			  {
			    e.stopImmediatePropagation();
			    e.preventDefault();
			    return false;
			    }
			    else
			    {
					last_click_time = click_time;
					navigator.notification.vibrate(100);
					toggleImg(friendEmailImgId,'friends');			
				}
			});
			
			var familyEmailImgId = 'familyemail'+l;
			var emailimg2 = document.createElement('img');
		if(emailstypeArr[l].search(familyId) == -1)
			emailimg2.setAttribute('src','css/imgs/icons/family-in.png');
		else
			emailimg2.setAttribute('src','css/imgs/icons/family.png');
			
			emailimg2.setAttribute('class','edit-icons-fam');
			emailimg2.setAttribute('id', familyEmailImgId);
			emailimg2.addEventListener('click',function (e){
			  click_time = e['timeStamp'];	
			  if (click_time && (click_time - last_click_time) < 2000)
			  {
	//		  	alert("time btw click is small");
			    e.stopImmediatePropagation();
			    e.preventDefault();
			    return false;
			    }
			    else
			    {
					last_click_time = click_time;
					navigator.notification.vibrate(100);
					toggleImg(familyEmailImgId,'family');
			
				}
			});
						
			var businessEmailImgId = 'businessemail'+l;
			var emailimg3 = document.createElement('img');
			if(emailstypeArr[l].search(businessId) == -1)
				emailimg3.setAttribute('src','css/imgs/icons/business-in.png');
			else
				emailimg3.setAttribute('src','css/imgs/icons/business.png');
			emailimg3.setAttribute('class','edit-icons-bus');
			emailimg3.setAttribute('id', businessEmailImgId);			
			emailimg3.addEventListener('click',function (e){
			  click_time = e['timeStamp'];	
			  if (click_time && (click_time - last_click_time) < 2000)
			  {
	//		  	alert("time btw click is small");
			    e.stopImmediatePropagation();
			    e.preventDefault();
			    return false;
			    }
			    else
			    {
					last_click_time = click_time;
					navigator.notification.vibrate(100);
					toggleImg(businessEmailImgId,'business');
			
				}
			});
			
						
			var publicEmailImgId = 'publicemail'+l;			
			var emailimg4 = document.createElement('img');
if(emailstypeArr[l].search(publicId) == -1)			
			emailimg4.setAttribute('src','css/imgs/icons/public-in.png');
else
			emailimg4.setAttribute('src','css/imgs/icons/public.png');

			emailimg4.setAttribute('class','edit-icons-bus');
			emailimg4.setAttribute('id', publicEmailImgId);			
			
			emailimg4.addEventListener('click',function (e){
			  click_time = e['timeStamp'];	
			  if (click_time && (click_time - last_click_time) < 2000)
			  {
	//		  	alert("time btw click is small");
			    e.stopImmediatePropagation();
			    e.preventDefault();
			    return false;
			    }
			    else
			    {
					last_click_time = click_time;
					navigator.notification.vibrate(100);
					toggleImg(publicEmailImgId,'public');
			
				}
			});
			
			
			emaila1.appendChild(emailimg1);
			emaila2.appendChild(emailimg2);
			emaila3.appendChild(emailimg3);
			emaila4.appendChild(emailimg4);
						
			emaildiv.appendChild(emaila1);
			emaildiv.appendChild(emaila2);
			emaildiv.appendChild(emaila3);
			emaildiv.appendChild(emaila4);  
			emailspan.appendChild(emailinput);
			emailspan.appendChild(emaildiv);

			var br = document.createElement('br');
                      
		
		//	alert("array: "+phonesArr[l]);
			
		document.getElementById("emailDiv").appendChild(emailspan);			
		document.getElementById("emailDiv").appendChild(br);			
}

/******************************************************************************************/
function updateEmailData(emailphonehiddenvalue, value, emailconnectionRelations)
{
if(emailphonehiddenvalue!="")
var url = "http://cardsbond.com/BondSys/web/api/v1/contacts/update_email?bond_contact_email_id="+emailphonehiddenvalue+"&email="+value+"&arr_levels=&bond_contact_id="+id+"&change_levels=flase";
else
var url = "http://cardsbond.com/BondSys/web/api/v1/contacts/update_email?bond_contact_email_id=&email="+value+"&arr_levels=4&bond_contact_id="+id;
console.log("update email url: "+url);
	var ajaxRequest;
		try{
			ajaxRequest = new XMLHttpRequest();
		} catch (e){
			try{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
					//alert("Microsoft");
				} catch (e){
					alert("Your browser broke!");
				return false;
				}
			}
		}		
		ajaxRequest.onreadystatechange = function(){
			if(ajaxRequest.readyState == 4){
					var response=ajaxRequest.responseText;
					if(response.search('error') == -1){
						if(response.search('email_id') != -1){					
						var results = JSON.parse(response);
						var email_id = results.email_id;
					
						socialSQL = 'UPDATE EMAILS SET emailid=(?) WHERE emailvalue=(?)';	
						db.transaction(function(tx){
							tx.executeSql(socialSQL,[email_id, value],errorCB);	   		
					    	}, errorCB, successDB);
					    
						var param = emailphonehiddenvalue+"|>"+value+"|>"+emailconnectionRelations;
						deleteFromLocalUpdates("updateEmailData", param);
									
	            		}
	            	}
	        		    		
				}
			}
		ajaxRequest.open("GET", url, true);		
		ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		//ajaxRequest.setRequestHeader("Content-length", param.length);
		ajaxRequest.setRequestHeader("Connection", "close");
		ajaxRequest.send();	
}	