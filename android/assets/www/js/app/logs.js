function showNativeLog()
	{
	    window.MainActivity.showLog();
		var logArray = [20];
		var name, phone, totalSec=0,seconds=0, minutes=0, hours=0, duration="", type, date, photo;
/*		for(var i=0; i<20;i++)
		{
			name = window.MainActivity.getName(i);
			phone = window.MainActivity.getPhoneNumbr(i);
			totalSec = window.MainActivity.getCallDuration(i);
			hours = parseInt( totalSec / 3600 ) % 24;
			minutes = parseInt( totalSec / 60 ) % 60;
			seconds = totalSec % 60;
			
			duration = (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds  < 10 ? "0" + seconds : seconds);
			
			type = window.MainActivity.getType(i);
			date = window.MainActivity.getDate(i);
			photo = window.MainActivity.getLogPhoto(i);			
			whatsAppIcon = window.MainActivity.getLogWhatsapp(i);
			logArray[i]= [name, phone, duration, type, date, photo, whatsAppIcon];
		}
*/		
		for(var i=0; i<20;i++)
		{
			name = window.MainActivity.getName(i);
			phone = window.MainActivity.getPhoneNumbr(i);
			totalSec = window.MainActivity.getCallDuration(i);
			hours = parseInt( totalSec / 3600 ) % 24;
			minutes = parseInt( totalSec / 60 ) % 60;
			seconds = totalSec % 60;
			
			duration = (hours < 10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds  < 10 ? "0" + seconds : seconds);
			
			type = window.MainActivity.getType(i);
			date = window.MainActivity.getDate(i);
			photo = window.MainActivity.getLogPhoto(i);			
			whatsAppIcon = window.MainActivity.getLogWhatsapp(i);
			
			if(name.length > charNum)
			{
				name = name.substr(0,charNum);
			}						
			
			var t1 = document.createTextNode(name);
			var t2 = document.createTextNode(phone);
			var t3 = document.createTextNode(duration);
			var t4 = document.createTextNode(type);
			var t5 = document.createTextNode(date);
			
			var p_arrow = document.createElement("a");
			p_arrow.setAttribute('href', '#');
			
			var i_arrow = document.createElement("i");
			var color = "";
			var arrowType ="";

			if(type=="out"){			
				color = "color:#7fba00";//green
				arrowType ="fa fa-arrow-left";
			}
			else if(type=="in"){
				color = "color:#E60000";//red
				arrowType ="fa fa-arrow-right";				
			}
			else if(type=="missed"){
				color = "color:#000000";//black
				arrowType ="fa fa-arrow-right";				
			}
						
			i_arrow.setAttribute('style', color);
			i_arrow.setAttribute('class', arrowType);
			p_arrow.appendChild(i_arrow);			
			
			
			var photoSrc= 'css/imgs/contact-v.png';			
			if(photo!="null")
				photoSrc=photo;
			
			var div0= document.createElement("div");
			div0.setAttribute('class', 'member white');			
			
			var div1 = document.createElement("div");
			div1.setAttribute('class', 'left');
			var a1 = document.createElement("a");
			
			var image1 = document.createElement("img");
			image1.setAttribute('class', 'photo-small');
			image1.setAttribute('src', photoSrc);
			a1.appendChild(image1);
			div1.appendChild(a1);
			
			var div2 = document.createElement("div");
			div2.setAttribute('class', '#');
			var phoneIconId = "mobileLog"+i;
			var a2 = document.createElement("a");
			a2.setAttribute('href', '#');
			a2.setAttribute('onClick',"navigator.notification.vibrate(100); window.MainActivity.dialDirectly(\'"+phone+"\')");			
			a2.setAttribute('id',phoneIconId);			
			
			var image2 = document.createElement("img");
			image2.setAttribute('class', 'call-icon');
			image2.setAttribute('src', 'css/imgs/icons/cards-call4.png');
			a2.appendChild(image2);
			div2.appendChild(a2);
						
			var strong1 = document.createElement("strong");
			strong1.appendChild(t1);			
						
			var p1 = document.createElement("p");
			p1.appendChild(t2);
						
			var p2 = document.createElement("p");
			p2.appendChild(t3);

			var a10 = document.createElement("a");
			a10.setAttribute('href', '#');
			a10.setAttribute('id', 'sms'+i);			
			
			var i10 = document.createElement("i");
			i10.setAttribute('class', 'fa fa-envelope');
			a10.appendChild(i10);
		
			var a11 = document.createElement("a");
			a11.setAttribute('href', '#');
			a11.setAttribute('id', 'whatsapp'+i);			
			a11.setAttribute('onClick', 'whatsapp'+i);			
			
			var i11 = document.createElement("img");
			i11.setAttribute('src', whatsAppIcon);
			i11.setAttribute('class', 'filtration-icons');

			a11.appendChild(i11);
		
			div2.appendChild(strong1);
			div2.appendChild(p1);
			div2.appendChild(p2);
			div2.appendChild(p_arrow);
			div2.appendChild(a10);
			div2.appendChild(a11);

			var div3 = document.createElement("div");
			div3.setAttribute('class', 'clear');			
			div0.appendChild(div1);			
			div0.appendChild(div2);
			div0.appendChild(div3);
			document.getElementById('phonelog').appendChild(div0);
			document.getElementById('sms'+i).href = 'sms:'+phone;
						
		}
	}