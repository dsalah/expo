function showContactComments(conId, name, contact_companyid)
{
	edited_comment="";
	var totalComments=0;
	conId=''+conId+'';
	name = ''+name+'';
	var myNode = document.getElementById('contactProfComment');
	while (myNode.firstChild) {
		myNode.removeChild(myNode.firstChild);
	}		
		
	var comment= 'SELECT * FROM CONTACTSCONNECTIONCOMMENT WHERE contactid=(?) AND othercontactid=(?) ';
	var allComments = 'SELECT max(commentid) AS topId FROM CONTACTSCONNECTIONCOMMENT';
		db.transaction(function(tx){
	    	tx.executeSql(allComments,[],function(ax,res){
	    	totalComments = res.rows.item(0).topId;
			if(totalComments== "null" || totalComments== null)	
			totalComments=0;    	
	    	tx.executeSql(comment,[id, conId],function(tx,results){
		    	var span= document.createElement('span');
				span.setAttribute('id', "conProfComm"+conId);
				document.getElementById("contactProfComment").appendChild(span);
				document.getElementById("conProfComm"+conId).innerHTML = "Your Comments About "+ name;
				var div2= document.createElement('div');
				div2.setAttribute('class', "box white");
				div2.setAttribute('id', "commentsParent");
				
		        var len = results.rows.length;
		        var div1;
				var comId= totalComments+1;

		        for (var i=0; i<len; i++)     
        		{
        			var comment=results.rows.item(i).comment;
     				var time = results.rows.item(i).time;
     				var commentId = results.rows.item(i).commentid;
					console.log("commentId"+commentId);
        		
        			div1= document.createElement('div');
					div1.setAttribute('id', "commentContainer"+commentId);
					div1.setAttribute('class', "box white");
											     				
					var span1= document.createElement('span');
					var t1= document.createTextNode(time);
					span1.appendChild(t1);
					div1.appendChild(span1);
		
					var span2= document.createElement('span');
					var a1= document.createElement('a');
					a1.setAttribute('href','#');
					a1.setAttribute("onClick","deleteComment(\'"+ commentId+"\', \'"+comment+"'\, \'"+conId+"'\)");
					var i1= document.createElement('i');
					i1.setAttribute('style','float:right !important');
					i1.setAttribute('class','fa fa-times fa-lg');
					a1.appendChild(i1);
					span2.appendChild(a1);

					var a2= document.createElement('a');
					a2.setAttribute('href','#');
					a2.setAttribute('id','edit'+commentId);

					a2.setAttribute('onClick',"editComment(\'"+commentId+"\', \'"+comId+"'\)");				

/*					a2.addEventListener('click',function (e){	
						click_time = e['timeStamp'];					
					  	if (click_time && (click_time - last_click_time) < 2000)
					  	{	
						    e.stopImmediatePropagation();
						    e.preventDefault();
						    return false;
					    }
					    else
					    {
							last_click_time = click_time;
							navigator.notification.vibrate(100);
							console.log("onclick editComment"+commentId);
							editComment(commentId , totalComments);					
						}
					});
	*/				
					var i2= document.createElement('i');
					i2.setAttribute('style','float:right !important');
					i2.setAttribute('class','fa fa-pencil fa-lg');
					a2.appendChild(i2);
					span2.appendChild(a2);
					
					var p= document.createElement('p');
					p.setAttribute('id', "commentValue"+commentId);
					console.log(i+" commentValue"+commentId);
					var com = document.createTextNode(comment);
					
					p.appendChild(com);
					div1.appendChild(span2);
					div1.appendChild(document.createElement('br'));
					
					div1.appendChild(p);
					div1.appendChild(document.createElement('hr'));
					document.getElementById("contactProfComment").appendChild(div1);
				}
					var textareadId= comId+"contactcomment";
  					var textarea= document.createElement('textarea');
  					textarea.setAttribute('placeholder','Post your Comment');
  					textarea.setAttribute('id', textareadId);
  					textarea.setAttribute('class','tag-btn');
  					textarea.setAttribute('style',"width:100%; height: 60px;");
					var input = document.createElement('input');					
					input.setAttribute('type','button');
					input.setAttribute('value','Submit');
					input.setAttribute('class','tag-btn');//flatBtn2
					input.setAttribute('id','flatBtn');					
					input.setAttribute('onClick',"saveCommentAboutContact(\'"+conId+"\', \'"+name+"\', \'"+comId+"'\)");				
					document.getElementById("contactProfComment").appendChild(document.createElement('br'));					

					document.getElementById("contactProfComment").appendChild(textarea);					
					document.getElementById("contactProfComment").appendChild(document.createElement('br'));					

					document.getElementById("contactProfComment").appendChild(input);								
				},errorCB);
	    	},errorCB);
	        }, errorCB, successDB);   
showContactCommentsForCompany(conId, name, contact_companyid);
}
/************************************************************/
function saveCommentAboutContact(conId, name, totalComments)
{
//totalComments=parseInt(totalComments);
	conId = ''+conId+'';
	var today=new Date();
	var h=today.getHours();
	var m=today.getMinutes();
	var s=today.getSeconds();
	m = checkTime(m);
	s = checkTime(s);	
	
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yyyy = today.getFullYear();

	if(dd<10) {
    	dd='0'+dd;
    }	 

	if(mm<10) {
    	mm='0'+mm;
	} 

	today = dd+'-'+mm+'-'+yyyy;
	var time = h+":"+m+":"+s;
	var fullDate = today+' '+time;
	var comId = totalComments+"contactcomment";
	//alert(comId);
	var comment = document.getElementById(comId).value;
	if(edited_comment!="")
	{	
		var old_comment = edited_comment;
		var selectcomment= 'SELECT * FROM CONTACTSCONNECTIONCOMMENT WHERE othercontactid=(?) AND upper(comment)=upper(?)';		
		db.transaction(function(tx){
	    	tx.executeSql(selectcomment,[conId, edited_comment],function(tx,res){
	    		if(res.rows.length>0){
	    			updateCommentValue(res.rows.item(0).commentid, fullDate, comment, conId, edited_comment);
    	        	networkState = checkConnection();
        			if(networkState!="none" && networkState!="unknown")			
    				    sendNewCommentToServer(id, conId, comment, "update", old_comment);
    	    
    				else{
    		            var select_updates= 'SELECT * FROM UPDATESFUNCTIONS WHERE name = (?) AND parameters=(?)';					 		
    		            var updates= 'INSERT INTO UPDATESFUNCTIONS(updateid, name, parameters, datetime) VALUES (?,?,?,?)';
    					    var sql_max_updated = "SELECT MAX(updateid) as updateid FROM UPDATESFUNCTIONS";	    
    						db.transaction(function(tx){
    							var updatedId = 0, arr1=""; 
					    		arr1 = id+"|>"+conId+"|>"+comment+"|>"+"update"+"|>"+edited_comment;
    							 
    							tx.executeSql(select_updates,["sendNewCommentToServer", arr1],function(tx, rs){
    								if(rs.rows.length==0){
    							
    							    	tx.executeSql(sql_max_updated,[],function(tx, results){
    							    		var len = results.rows.length;
    							    		if(len>0)
    							    		updatedId = results.rows.item(0).updateid;
    							    		updatedId= updatedId+1;
    							    		arr1 = id+"|>"+conId+"|>"+comment+"|>"+"update"+"|>"+old_comment;
    							    		console.log("arr1"+arr1);
    							    		console.log("updated function id "+updatedId);
    									    tx.executeSql(updates,[updatedId, "sendNewCommentToServer", arr1, setLastUpdatesTime()]);
    				    				},errorCB);		
    								}
    							},errorCB);		
    						}, errorCB, successDB);	

    				}

	    		
	    		}
	    	},errorCB);
	    	},errorCB,successDB);
		
	}
	else if(comment!=""){
		document.getElementById(comId).value="";		
		var insertcomment= 'INSERT INTO CONTACTSCONNECTIONCOMMENT (commentid, contactid,  othercontactid, comment, time) VALUES (?, ?, ?, ?, ?)';
		var selectcomment= 'SELECT * FROM CONTACTSCONNECTIONCOMMENT WHERE othercontactid=(?) AND upper(comment)=upper(?)';		
		db.transaction(function(tx){
	    	tx.executeSql(selectcomment,[conId, comment],function(tx,res){
	    		if(res.rows.length==0){
	    			tx.executeSql(insertcomment,[totalComments, id, conId, comment, fullDate],errorCB);
	    	    	showContactComments(conId, name);
	    	    
	    	        	networkState = checkConnection();
	        			if(networkState!="none" && networkState!="unknown")			
	    				    sendNewCommentToServer(id, conId, comment, "insert", "");
	    	    
	    				else{
	    		            var select_updates= 'SELECT * FROM UPDATESFUNCTIONS WHERE name = (?) AND parameters=(?)';					 		
	    		            var updates= 'INSERT INTO UPDATESFUNCTIONS(updateid, name, parameters, datetime) VALUES (?,?,?,?)';
	    					    var sql_max_updated = "SELECT MAX(updateid) as updateid FROM UPDATESFUNCTIONS";	    
	    						db.transaction(function(tx){
	    							var updatedId = 0, arr1=""; 
						    		arr1 = id+"|>"+conId+"|>"+comment+"|>"+"insert"+"|>"+" ";
	    							 
	    							tx.executeSql(select_updates,["sendNewCommentToServer", arr1],function(tx, rs){
	    								if(rs.rows.length==0){
	    							
	    							    	tx.executeSql(sql_max_updated,[],function(tx, results){
	    							    		var len = results.rows.length;
	    							    		if(len>0)
	    							    		updatedId = results.rows.item(0).updateid;
	    							    		updatedId= updatedId+1;
	    							    		arr1 = id+"|>"+conId+"|>"+comment+"|>"+"insert"+"|>"+" ";
	    							    		
	    							    		console.log("arr1"+arr1);
	    							    		console.log("updated function id "+updatedId);
	    									    tx.executeSql(updates,[updatedId, "sendNewCommentToServer", arr1, setLastUpdatesTime()]);
	    				    				},errorCB);		
	    								}
	    							},errorCB);		
	    						}, errorCB, successDB);	

	    				}
	    		}
	    		else{
	    			   $.msg({ content: 'You have inserted this comment before' });	
	    		
	    		}
	    	},errorCB);
		},errorCB, successDB);
	}
	else
	   $.msg({ content: 'Please Enter a comment' });	
}
/*****************************************************************/
function sendNewCommentToServer(id, conId, comment, url_type, old_comment)
{	
	var server_comment="["+event_name+"] "+comment;
	var old_comment_to_server ="["+event_name+"] "+old_comment; 
	var url="";
	if(url_type=="update")
		url = "http://www.cardsbond.com/BondSys/web/api/v1/contacts/add_comment?id="+id+"&othercontact_id="+conId+"&comment="+server_comment+"&event_id="+event_id+"&update_comment=true&old_comment="+old_comment_to_server;
	else
		url = "http://www.cardsbond.com/BondSys/web/api/v1/contacts/add_comment?id="+id+"&othercontact_id="+conId+"&comment="+server_comment+"&event_id="+event_id;
	console.log("new comment url: "+url);
	var ajaxRequest;
		try{
			ajaxRequest = new XMLHttpRequest();
		} catch (e){
			try{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
				} catch (e){
					alert("Your browser broke!");
				return false;
				}
			}
		}
		ajaxRequest.onreadystatechange = function(){
			if(ajaxRequest.readyState == 4){
				var response=ajaxRequest.responseText;
				console.log("comment response"+response);
				
				if(response.search('error') == -1){
					var results = JSON.parse(response);
					console.log("comment id from server"+results.result);
					updateCommentId(results.result, comment, conId);
					var param = id+"|>"+conId+"|>"+comment+"|>"+url_type+"|>"+old_comment;
					deleteFromLocalUpdates("sendNewCommentToServer", param);
				}
			}
		}
		
		ajaxRequest.open("GET", url, true);		
		ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		//ajaxRequest.setRequestHeader("Content-length", param.length);
		ajaxRequest.setRequestHeader("Connection", "close");
		ajaxRequest.send();	
}
/****************************************************************************/
function updateCommentId(commentId, comment, otherid)
{
	console.log("in updateCommentId");
	commentId = parseInt(commentId);
	var updateComm= 'UPDATE CONTACTSCONNECTIONCOMMENT SET commentid=(?) WHERE comment=(?) AND contactid=(?) AND othercontactid=(?)';
   	db.transaction(function(tx){
   		tx.executeSql(updateComm,[commentId, comment, id, otherid]);
            //showContactComments(otherid, name);        
            console.log("comment updated");
    		}, errorCB, successDB);

}
/***********************************************************************/
function updateCommentValue(commentId, date, comment, otherid, old_comment)
{
	console.log("update commentId"+commentId);
	commentId = parseInt(commentId);
	var updateComm= 'UPDATE CONTACTSCONNECTIONCOMMENT SET comment=(?) ,time=(?) WHERE comment=(?) AND contactid=(?) AND othercontactid=(?)';

    	db.transaction(function(tx){
            tx.executeSql(updateComm,[comment, date, old_comment, id, otherid]);
            showContactComments(otherid, name);      
            console.log("comment updated");
    		}, errorCB, successDB);

}
/********************************************************************/
function deleteComment(commentId, comment, conId)
{
	var delComm= 'DELETE FROM CONTACTSCONNECTIONCOMMENT WHERE comment=(?) AND contactid=(?) AND othercontactid=(?)';
    	db.transaction(function(tx){
            tx.executeSql(delComm,[comment, id, conId]);
    		}, errorCB, function(tx,results){
    		var removedChild = document.getElementById("commentContainer"+commentId);
				document.getElementById("contactProfComment").removeChild(removedChild);
	        	networkState = checkConnection();
    			if(networkState!="none" && networkState!="unknown")			
    				deleteCommentFromServer(comment, conId);
	    
				else{
		            var select_updates= 'SELECT * FROM UPDATESFUNCTIONS WHERE name = (?) AND parameters=(?)';					 		
		            var updates= 'INSERT INTO UPDATESFUNCTIONS(updateid, name, parameters, datetime) VALUES (?,?,?,?)';
					    var sql_max_updated = "SELECT MAX(updateid) as updateid FROM UPDATESFUNCTIONS";	    
						db.transaction(function(tx){
							var updatedId = 0, arr1=""; 
				    		arr1 = comment+"|>"+conId;
							 
							tx.executeSql(select_updates,["deleteCommentFromServer", arr1],function(tx, rs){
								if(rs.rows.length==0){
							
							    	tx.executeSql(sql_max_updated,[],function(tx, results){
							    		var len = results.rows.length;
							    		if(len>0)
							    		updatedId = results.rows.item(0).updateid;
							    		updatedId= updatedId+1;
							    		arr1 = comment+"|>"+conId;
							    		console.log("arr1"+arr1);
							    		console.log("updated function id "+updatedId);
									    tx.executeSql(updates,[updatedId, "deleteCommentFromServer", arr1, setLastUpdatesTime()]);
				    				},errorCB);		
								}
							},errorCB);		
						}, errorCB, successDB);	

				}

				
				
    		});
	
}
/********************************************/
function deleteCommentFromServer(comment, conId)
{
	comment="["+event_name+"] "+comment;

	//var url = "http://www.cardsbond.com/BondSys/web/api/v1/contacts/delete_comment?comment_id="+commentId;
	var url = "http://www.cardsbond.com/BondSys/web/api/v1/contacts/delete_comment?id="+id+"&other_id="+conId+"&comment="+comment+"&event_id="+event_id;
	console.log("delete comment server url"+url);
	var ajaxRequest;
		try{
			ajaxRequest = new XMLHttpRequest();
		} catch (e){
			try{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
					//alert("Microsoft");
				} catch (e){
					alert("Your browser broke!");
				return false;
				}
			}
		}
		ajaxRequest.onreadystatechange = function(){
			if(ajaxRequest.readyState == 4){
				var response=ajaxRequest.responseText;
				if(response.search('error') == -1){
					console.log("delete comment "+response);
					var results = JSON.parse(response);
					var param = comment+"|>"+conId;
					deleteFromLocalUpdates("deleteCommentFromServer", param);
					
					
					}
			}
		}
		ajaxRequest.open("GET", url, true);		
		ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		//ajaxRequest.setRequestHeader("Content-length", param.length);
		ajaxRequest.setRequestHeader("Connection", "close");
		ajaxRequest.send();	
}
/************************************************/
function editComment(commentId, textboxId)
{	
	console.log("editComment commentId: "+commentId);
	var p = document.getElementById("commentValue"+commentId);
	var text = p.firstChild.nodeValue;
	document.getElementById(textboxId+"contactcomment").value= text;
	edited_comment = text;

	/*var delComm= 'DELETE FROM CONTACTSCONNECTIONCOMMENT WHERE commentid=(?)';
    	db.transaction(function(tx){
            tx.executeSql(delComm,[commentId]);
   		}, errorCB, function(tx,results){
//    		var removedChild = document.getElementById("commentContainer"+commentId);
//				document.getElementById("contactProfComment").removeChild(removedChild);
   	});*/
}
/*******************************************/
function deleteFromLocalUpdates(name, param)
{
	console.log("in deleteFrmLocalUpdates"+name+" param"+param)
	var updates= 'DELETE FROM UPDATESFUNCTIONS WHERE name =(?) AND parameters=(?)';
	db.transaction(function(tx){
	    tx.executeSql(updates,[name, param]);
	}, errorCB, successDB);		
		
}