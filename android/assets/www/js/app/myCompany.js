  function querySuccess_myCompany(tx, results) {
		flag="company-cont"+mycompanyid;	

		var len = results.rows.length;
    //    alert("CONTACTS table: " + len + " rows found.");
        for (var i=0; i<len; i++){        	
        	var	contactCompany=results.rows.item(i).company;
        	var contactPosition=results.rows.item(i).position;
        	var contactCompanyId = results.rows.item(i).companyid;
        	var bondId = results.rows.item(i).contactid;
      		var contactUsername = results.rows.item(i).username;
    		var contactImage = results.rows.item(i).imgsource;
    		//alert(results.rows.item(i).sort_counter);
		    var contactTimezone = results.rows.item(i).timezone;
		    var currentTime = getCurrentTime(contactTimezone);
		    var country_name = results.rows.item(i).country_name;

		    //svar mobileNumber = setMyCompanyContactNumberToSendSMS(bondId);			

		    if(parseInt(bondId) > my_company_contacts_max_id)
		    	my_company_contacts_max_id = bondId;
			
			var div0= document.createElement("div");
			div0.setAttribute('class', 'member white');			
			div0.setAttribute('id', 'member white'+bondId);			
			
			var div1 = document.createElement("div");
			div1.setAttribute('class', 'left');
			var a1 = document.createElement("a");
			var image1 = document.createElement("img");
			
			//alert(contactImage);
			image1.setAttribute('class', 'photo-small');
			image1.setAttribute('src', contactImage);
			a1.appendChild(image1);
			div1.appendChild(a1);
			
			var div2 = document.createElement("div");
			div2.setAttribute('class', '#');
			var phoneIconId = "mobile"+bondId;
			var a2 = document.createElement("a");
			a2.setAttribute('href', '#');
			a2.setAttribute('id',phoneIconId);
			a2.setAttribute('onClick','navigator.notification.vibrate(100); sortMyCompanyContacts('+bondId+'); setContactNumberToCall(' + bondId +')');
			
			var image_i = document.createElement("i");
			image_i.setAttribute('class', 'call-icon fa fa-phone-square fa-2x');
			//a2.appendChild(image_i);
			div2.appendChild(a2);
						
			var str1="bname";
			var name_id=str1+bondId;
//			alert(name_id);
			var strong1 = document.createElement("strong");
			strong1.setAttribute('id', name_id);
			
			str1="contactposition";
			
			var position_id=str1+bondId;
//			alert(position_id);
			
			var p1 = document.createElement("p");
			p1.setAttribute('id', position_id);
			
			str1="contactcompany";
			
			var company_id=str1+bondId;
//			alert(company_id);
			
			var p2 = document.createElement("p");
			p2.setAttribute('id', company_id);
			
			var a10 = document.createElement("a");
			a10.setAttribute('href', '#');
			a10.setAttribute('id', 'sms'+bondId);			
			a10.setAttribute('onClick','navigator.notification.vibrate(100); setMyCompanyContactNumberToSendSMS(' + bondId +')');
			
			var i10 = document.createElement("i");
			i10.setAttribute('class', 'fa fa-comment fa-lg');
			a10.appendChild(i10);
		
			var a11 = document.createElement("a");
			a11.setAttribute('href', '#');
			a11.setAttribute('id', 'whatsapp'+bondId);			
			
			var i11 = document.createElement("i");
			i11.setAttribute('class', 'fa fa-envelope');
			a11.appendChild(i11);
			
			var a3 = document.createElement("a");
			a3.setAttribute('href', '#');
			a3.setAttribute('id', 'facebook'+bondId);			
			
			var a4 = document.createElement("a");
			a4.setAttribute('href', '#');
			a4.setAttribute('id', 'twitter'+bondId);			
			
			
			var a5 = document.createElement("a");
			a5.setAttribute('href', '#');
			a5.setAttribute('id', 'linkedin'+bondId);			
			
			var a6 = document.createElement("a");
			a6.setAttribute('href', '#');
			a6.setAttribute('id', 'skype'+bondId);						
			
			var a7 = document.createElement("a");
			a7.setAttribute('href', '#');
			a7.setAttribute('id', 'google'+bondId);						

			var addIconId = "addIconToBond"+bondId;
			var addIcon = document.createElement("a");
			addIcon.setAttribute('href', '#');
			addIcon.setAttribute('id',addIconId);

			var time = "timezone"+bondId;
			var div4 = document.createElement("div");
			div4.setAttribute('class', 'clock');
			div4.setAttribute('id', time);			
			div2.appendChild(strong1);
			div2.appendChild(p1);
			div2.appendChild(addIcon);				
			div2.appendChild(p2);
			div2.appendChild(document.createElement("br"));
			
			//div2.appendChild(a10);
			//div2.appendChild(a11);			
			div2.appendChild(a3);
			div2.appendChild(a4);
			div2.appendChild(a5);
			div2.appendChild(a6);
			div2.appendChild(a7);

			div2.appendChild(div4);

			var div3 = document.createElement("div");
			div3.setAttribute('class', 'clear');
			
			div0.appendChild(div1);			
			div0.appendChild(div2);			
			div0.appendChild(div3);
			
			//touch_div.appendChild(div0);
			//document.getElementById(flag).appendChild(touch_div);
			if(document.getElementById('member white'+bondId)== null)
				document.getElementById("company_member").appendChild(div0);


			var firstName = results.rows.item(i).fname;
			var lastName = results.rows.item(i).lname;
			var conName = firstName + " " + lastName;
			document.getElementById(name_id).innerHTML= conName; 
			document.getElementById(company_id).innerHTML=contactCompany+"/"+country_name;
			document.getElementById(position_id).innerHTML=contactPosition;
			setInterval(function(){
			//alert("in fn");
			myTimer(time, contactTimezone);
			},1000);
			
			document.getElementById(time).innerHTML=currentTime;
	 	    attachSocial(results.rows.item(i).contactid);		  
		    attachAddIcon(results.rows.item(i).contactid, results.rows.item(i).username);

        }
    }
/*************************************************************************************************/
function sortMyCompanyContacts(bondId)
{
//alert(bondId);
bondId = ''+bondId+'';
var updatecontact = 'UPDATE MYCOMPANYCONTATCS SET sort_counter=(?) WHERE contactid=(?)';
    var selectcontact = 'SELECT contactid, sort_counter FROM MYCOMPANYCONTATCS WHERE contactid=(?)';

	db.transaction(function(tx){
		tx.executeSql(selectcontact,[bondId],function(tx,results){
			//alert(results.rows.length);
			if(results.rows.length>0){
				var sort_counter = results.rows.item(0).sort_counter;
				var new_sort_counter = (parseInt(sort_counter))+1;
					
		   		tx.executeSql(updatecontact,[new_sort_counter, bondId]);
		   	}
		},errorCB);
    }, errorCB, successDB);
}
/**********************************************************************************************/
function setMyCompanyContactNumberToSendSMS(bondId)
{
	bondId=''+bondId+'';
	var mycontactmobile='SELECT * FROM MOBILES WHERE contactid=(?)';

	db.transaction(function(tx){
	    tx.executeSql(mycontactmobile,[bondId],function (mobileax, mobileres){
		    var mobileLen = mobileres.rows.length;
		    if(mobileLen>=1){
		    document.getElementById('sms'+bondId).href = 'sms:'+mobileres.rows.item(0).mobilevalue;
	 	    }
		    else
		    	$.msg({ content : 'No Mobile Set' });	
		    
	    },errorCB);	    
 	}, errorCB, successDB);
}
/************************************************************************************/
function searcInMyCompany(val)
{
	flag="company_member";
/*	
	//alert("mycompany"+mycompany);
	var sqltxt= 'SELECT * FROM MYCOMPANYCONTATCS WHERE contactid!=(?) ORDER BY sort_counter DESC';
		db.transaction(function(tx){
           tx.executeSql(sqltxt,[id],querySuccess_myCompany,errorCB);
        }, errorCB, successDB);   
	*/
	my_company_contacts_max_id = 0;
	is_seacrh_mode_mycompany= true;
	document.getElementById("loadMoreMyCompanyContacts_p").innerHTML = "Load More Co-workers...";

	searchValueInServer_mycompany=val;
	val ='%'+val+'%';
	var my = document.getElementById("company_member");
	if(my){
		while (my.firstChild) {
			my.removeChild(my.firstChild);
		}	
	}		
	var contactSearchArray={};
	var counterSearch=0;
	var searchTag = 'SELECT * FROM CONTACTSCONNECTIONTAGS WHERE upper(tags) LIKE upper(?)';
	var searchComment = 'SELECT * FROM CONTACTSCONNECTIONCOMMENT WHERE upper(comment) LIKE upper(?)';
	var searchEmail = 'SELECT * FROM EMAILS WHERE upper(emailvalue) LIKE upper(?)';
    var search= 'SELECT * FROM MYCOMPANYCONTATCS WHERE upper(fname) LIKE upper(?) OR upper(lname) LIKE upper(?) OR upper(position) LIKE upper(?) ';
		db.transaction(function(tx){
	    	tx.executeSql(search,[val, val, val],function(tx,results)
	    	{
	    		console.log("my company length"+results.rows.length);
	    		querySuccess_myCompany(tx,results);
	    		 /* for (var i=0; i<results.rows.length; i++){        	
        				var bondId = results.rows.item(i).contactid;
        				contactSearchArray[counterSearch]=bondId;
        				counterSearch++;
						//getOtherContactData(bondId);
					}
					*/
					/*
					tx.executeSql(searchTag,[val],function(ax,res){
					for (var i=0; i<res.rows.length; i++){        	
	        				var bondId = res.rows.item(i).othercontactid;
	        				contactSearchArray[counterSearch]=bondId;
        					counterSearch++;
							//getOtherContactData(bondId);
						}
						tx.executeSql(searchEmail,[val],function(bx,emailresult){
   						for (var i=0; i<emailresult.rows.length; i++){        	
			        		var bondId = emailresult.rows.item(i).othercontactid;
			        		contactSearchArray[counterSearch]=bondId;
        					counterSearch++;
							//getOtherContactData(bondId);
						}
						tx.executeSql(searchComment,[val],function(cx,commentresult){
		   				for (var i=0; i<commentresult.rows.length; i++){        	
			        		var bondId = commentresult.rows.item(i).othercontactid;
			        		contactSearchArray[counterSearch]=bondId;
        					counterSearch++;
							//getOtherContactData(bondId);
						}
					},errorCB);
			
				},errorCB);						
			},errorCB);*/
			},errorCB);	    	
			
			}, errorCB, function(){
			/*
			var uniqueNames = [];
			$.each(contactSearchArray, function(i, el){
    			if($.inArray(el, uniqueNames) === -1) uniqueNames.push(el);
			});		

			for (var i=0; i<uniqueNames.length; i++){   
				if(uniqueNames[i] != id)
					getOtherContactData(uniqueNames[i]);
			}
			console.log("unique names"+uniqueNames.length);
			if(uniqueNames.length==0 && serachKeywordResult_mycompany==""){
				serachKeywordResult_mycompany="found";
				$.msg({ content : 'No Contacts matched..' });
			}*/			
		});   

}
/**************************/
function loadMoreMyCompanyContacts()
{	
	$.msg({ content : 'Please wait...' });	
if(document.getElementById('my_company_search'==""))
	is_seacrh_mode_mycompany=false;
	console.log("loadMoreMyCompanyContacts");
	 if(loadMoreMyCompanyContactsdbclk%2==0){
		 
	 my_company_contacts_limit=''+my_company_contacts_limit+'';
	 my_company_contacts_max_id=''+my_company_contacts_max_id+'';
	 var url="";
	 console.log("is_seacrh_mode_mycompany"+is_seacrh_mode_mycompany);
	
	 if(is_seacrh_mode_mycompany){
		url = "http://cardsbond.com/BondSys/web/api/v1/events/search_mycompany_contacts?searchwords="+searchValueInServer_mycompany+
		"&bond_contact_id="+id+"&company_id="+mycompanyid+"&limit="+my_company_contacts_limit+"&max_id="+my_company_contacts_max_id;
		console.log("in if");
		/*var my = document.getElementById("load_more_mycompany");
		if(my && my.firstChild.nodeName != "P"){
			while (my.firstChild) {		
				if(my.firstChild.nodeName != "P")
					my.removeChild(my.firstChild);

			}	
		}*/		

	 }
	 else{
		 console.log("in else"+url);
	 	url= "http://www.cardsbond.com/BondSys/web/api/v1/contacts/my_company_contacts?company_id="+mycompanyid+"&bond_contact_id="+id+"&limit="+my_company_contacts_limit+"&max_id="+my_company_contacts_max_id;
	 }
	 
	console.log("loadCustomContacts"+url);
	networkState = checkConnection();
    if(networkState!="none" && networkState!="unknown")
    {
		var ajaxRequest;
		try{
			ajaxRequest = new XMLHttpRequest();
		} catch (e){
			try{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
					//alert("Microsoft");
				} catch (e){
					alert("Your browser broke!");
				return false;
				}
			}
		}
		ajaxRequest.onreadystatechange = function(){		
 			if(ajaxRequest.readyState == 4){
				var response=ajaxRequest.responseText;
				console.log(response);
				
				if(response!=""){
					if(response.search('error')== -1){
					var results = JSON.parse(response);
					displayNewLoadedMyCompanyContacts(results,'contact');			
					}
					else{
						$.msg({ content : "No contacts to display" });
						document.getElementById("loadMoreMyCompanyContacts_p").innerHTML = "No more co-workers to load";

					}
				}
				if(response=="")
				{
				
					//$.msg({ content : "Network erroy, try again" });	
						
				}
				
		}
		}			
		ajaxRequest.open("GET", url, true);		
		ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		//ajaxRequest.setRequestHeader("Content-length", param.length);
		ajaxRequest.setRequestHeader("Connection", "close");
		ajaxRequest.send();
		}
		else
		{
			$.msg({ content : 'Please Check your Internet Connection' });	
		}		    			    					
	 }
	 	 loadMoreMyCompanyContactsdbclk++;
	 		 
}

function displayNewLoadedMyCompanyContacts(results, category)
{
	flag="company-cont"+mycompanyid;	
	if(results.result.length==0){
		//var text = document.createTextNode('No result Found');
		document.getElementById("loadMoreContactsText_p").innerHTML = "No more contacts to load";
	}
	else{
			for(var i=0; i<results.result.length;i++){
			    var	contactCompany= results.result[i].company;
	        	var contactPosition=results.result[i].position;	        	
	        	var contactCompanyId = results.result[i].bond_company_id;
	        	var bondId = results.result[i].bond_contact_id;
	      		var contactUsername = results.result[i].username;
	    		var contactImage = results.result[i].image;
	    		var country_name = results.result[i].country_name;

	    		if(contactImage=="")
			    contactImage="css/imgs/contact-v.png";
			    else
			    {
				    contactImage="http://www.cardsbond.com/"+contactImage;
			    
			    }
			    console.log("bondId"+bondId);
			    if(parseInt(bondId)> parseInt(my_company_contacts_max_id))
			    	my_company_contacts_max_id=bondId;
			    console.log("max_id"+my_company_contacts_max_id);
			    var contactTimezone =  parseInt(results.result[i].time_zone);
			    var currentTime = getCurrentTime(contactTimezone)

		
		var div0= document.createElement("div");
		div0.setAttribute('class', 'member white');			
		div0.setAttribute('id', 'member white'+bondId);			
		
		var div1 = document.createElement("div");
		div1.setAttribute('class', 'left');
		var a1 = document.createElement("a");
		var image1 = document.createElement("img");
		//alert(contactImage);
		image1.setAttribute('class', 'photo-small');
		image1.setAttribute('src', contactImage);
		a1.appendChild(image1);
		div1.appendChild(a1);
		
		var div2 = document.createElement("div");
		div2.setAttribute('class', '#');
		var phoneIconId = "mobile"+bondId;
		var a2 = document.createElement("a");
		a2.setAttribute('href', '#');
		a2.setAttribute('id',phoneIconId);
		a2.setAttribute('onClick','navigator.notification.vibrate(100); sortMyCompanyContacts('+bondId+'); setContactNumberToCall(' + bondId +')');
		
		var image_i = document.createElement("i");
		image_i.setAttribute('class', 'call-icon fa fa-phone-square fa-2x');
		//a2.appendChild(image_i);
		div2.appendChild(a2);
					
		var str1="bname";
		var name_id=str1+bondId;
//		alert(name_id);
		var strong1 = document.createElement("strong");
		strong1.setAttribute('id', name_id);
		
		str1="contactposition";
		
		var position_id=str1+bondId;
//		alert(position_id);
		
		var p1 = document.createElement("p");
		p1.setAttribute('id', position_id);
		
		str1="contactcompany";
		
		var company_id=str1+bondId;
//		alert(company_id);
		
		var p2 = document.createElement("p");
		p2.setAttribute('id', company_id);
		
		var a10 = document.createElement("a");
		a10.setAttribute('href', '#');
		a10.setAttribute('id', 'sms'+bondId);			
		a10.setAttribute('onClick','navigator.notification.vibrate(100); setMyCompanyContactNumberToSendSMS(' + bondId +')');
		
		var i10 = document.createElement("i");
		i10.setAttribute('class', 'fa fa-comment fa-lg');
		a10.appendChild(i10);
	
		var a11 = document.createElement("a");
		a11.setAttribute('href', '#');
		a11.setAttribute('id', 'whatsapp'+bondId);			
		
		var i11 = document.createElement("i");
		i11.setAttribute('class', 'fa fa-envelope');
		a11.appendChild(i11);
		
		var a3 = document.createElement("a");
		a3.setAttribute('href', '#');
		a3.setAttribute('id', 'facebook'+bondId);			
		
		var a4 = document.createElement("a");
		a4.setAttribute('href', '#');
		a4.setAttribute('id', 'twitter'+bondId);			
		
		
		var a5 = document.createElement("a");
		a5.setAttribute('href', '#');
		a5.setAttribute('id', 'linkedin'+bondId);			
		
		var a6 = document.createElement("a");
		a6.setAttribute('href', '#');
		a6.setAttribute('id', 'skype'+bondId);						
		
		var a7 = document.createElement("a");
		a7.setAttribute('href', '#');
		a7.setAttribute('id', 'google'+bondId);						

		var addIconId = "addIconToBond"+bondId;
		var addIcon = document.createElement("a");
		addIcon.setAttribute('href', '#');
		addIcon.setAttribute('id',addIconId);

		var time = "timezone"+bondId;
		var div4 = document.createElement("div");
		div4.setAttribute('class', 'clock');
		div4.setAttribute('id', time);			
		div2.appendChild(strong1);
		div2.appendChild(p1);
		div2.appendChild(addIcon);				
		div2.appendChild(p2);
		div2.appendChild(document.createElement("br"));
		
		//div2.appendChild(a10);
		//div2.appendChild(a11);			
		div2.appendChild(a3);
		div2.appendChild(a4);
		div2.appendChild(a5);
		div2.appendChild(a6);
		div2.appendChild(a7);

		div2.appendChild(div4);

		var div3 = document.createElement("div");
		div3.setAttribute('class', 'clear');
		
		div0.appendChild(div1);			
		div0.appendChild(div2);			
		div0.appendChild(div3);
		if(document.getElementById('member white'+bondId)== null)
		document.getElementById("company_member").appendChild(div0);
		var conName = results.result[i].fname + " " + results.result[i].lname;
		document.getElementById(name_id).innerHTML= conName; 
		document.getElementById(company_id).innerHTML=contactCompany+"/"+country_name;
		document.getElementById(position_id).innerHTML=contactPosition;
		setInterval(function(){
		//alert("in fn");
		myTimer(time, contactTimezone);
		},1000);
		
		document.getElementById(time).innerHTML=currentTime;
 	    attachSocial(bondId);		  
	    attachAddIcon(bondId, results.result[i].username);

    }
}
}