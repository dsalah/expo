function updateMyCompanyProfileData()
{

	console.log("in update my company local");
	var last_updated_id="", last_updated_by_name="", is_phone_changed= false, is_email_changed= false, is_info_changed= false;
	//var imgsource = document.getElementById('edit_company_profile_picture').src;	
	var info = document.getElementById('editBriefMyCompany').value;
	var address = document.getElementById('updated_company_address').value;

	var country  = (document.getElementById('updated_company_country').selectedIndex)+1;
	var city  = (document.getElementById('updated_company_city').selectedIndex)+1;

	var email = document.getElementById('updated_company_email').value;
	var phone = document.getElementById('updated_company_phone').value;
	console.log("email:"+email);
	console.log("phone:"+phone);
	var companies_update= 'UPDATE COMPANIES set info=(?), address=(?), last_updated_by_id=(?), last_updated_by_name=(?), city_id=(?), country_id=(?) WHERE companyid=(?)';
	var companies_select= 'SELECT * FROM COMPANIES WHERE companyid=(?)';
	var companies_insert= 'INSERT INTO COMPANIES (info, address, companyid, last_updated_by_id, last_updated_by_name, city_id, country_id) VALUES (?, ?, ?, ?, ?, ?, ?)';

	var emails_update= 'UPDATE COMPANIES_EMAILS set emailvalue=(?), typeid=(?) WHERE companyid=(?)';
	var emails_select= 'SELECT * FROM COMPANIES_EMAILS WHERE companyid=(?)';
	var emails_insert= 'INSERT INTO COMPANIES_EMAILS (emailid, companyid, emailvalue, typeid) VALUES (?, ?, ?, ?)';

	var phones_update= 'UPDATE COMPANIES_PHONES set phonevalue=(?), typeid=(?) WHERE companyid=(?)';
	var phones_select= 'SELECT * FROM COMPANIES_PHONES WHERE companyid=(?)';
	var phones_insert= 'INSERT INTO COMPANIES_PHONES (phoneid, companyid, phonevalue, typeid) VALUES(?, ?, ?, ?)';

	db.transaction(function(tx){
	   	tx.executeSql(companies_select,[mycompanyid],function(tx,results){
			console.log("comany results.rows.length: "+results.rows.length);
				if(results.rows.length >0){
					last_updated_id = results.rows.item(0).last_updated_by_id;
					last_updated_by_name = results.rows.item(0).last_updated_by_name;
					if(results.rows.item(0).info != info || results.rows.item(0).address != address || results.rows.item(0).imgsource != mycompanylogo || results.rows.item(0).city_id != city || results.rows.item(0).country_id != country){
						last_updated_id = id;
						last_updated_by_name = name;
						is_info_changed = true;
					}
					/////////////////////////////////////////////////////////////////////
					if(email!=""){										
						tx.executeSql(emails_select,[mycompanyid],function(ax,res){		
							if(res.rows.length==0){		
								console.log("insert email: "+email);
								tx.executeSql(emails_insert,["", mycompanyid, email, "4"],errorCB);
								last_updated_id = id;
								last_updated_by_name = name;
								is_email_changed = true;
							}
							else{
								console.log("update email: "+email);						
								if(res.rows.item(0).emailvalue != email ){
									last_updated_id = id;
									last_updated_by_name = name;
									is_email_changed = true;
									tx.executeSql(emails_update,[email, "4", mycompanyid],errorCB);
									
								}
								
							}	
						},errorCB);
					}
					///////////////////////////////
					if(phone!=""){
						tx.executeSql(phones_select,[mycompanyid],function(bx,resul){					
							if(resul.rows.length==0){
								last_updated_id = id;
								last_updated_by_name = name;
								is_phone_changed = true;

								tx.executeSql(phones_insert,["", mycompanyid, phone, ""],errorCB);

							}
							else
							{
								if(resul.rows.item(0).phonevalue != phone ){
									last_updated_id = id;
									last_updated_by_name = name;
									is_phone_changed = true;

									tx.executeSql(phones_update,[phone, "4", mycompanyid],errorCB);

								}	
		
							}
						},errorCB);
					}
					////////////////////////////////////////////
					
				if(results.rows.length == 0){
				   	tx.executeSql(companies_insert,[info, address, mycompanyid, id, name, city, country],errorCB);
					last_updated_id = id;
				}
				else{
					tx.executeSql(companies_update,[info, address, last_updated_id, last_updated_by_name, city, country,  mycompanyid],errorCB);					
				}
				
			}						
		},errorCB);
	},errorCB, function(){
				
	var companyImgOnServer="company_logo/company_"+mycompanyid+companyImageExt;
	if(companyImageExt == "")
		companyImgOnServer="";
	console.log("companyImgOnServer: "+companyImgOnServer);

	networkState = checkConnection();
	if(networkState!="none" && networkState!="unknown"){
		console.log("Calling server to updates");

		if(is_info_changed)
			updateCompanyInfoInServer(companyImgOnServer, info, address, city, country, last_updated_id, "");
		if(is_email_changed)
			updateCompanyEmailInServer(email, "");
		if(is_phone_changed)
			updateCompanyPhoneInServer(phone, "");
	}	
	else
	{
	
	    var updates= 'INSERT INTO UPDATESFUNCTIONS(updateid, name, parameters, datetime) VALUES (?,?,?, ?)';
				    var sql_max_updated = "SELECT MAX(updateid) as updateid FROM UPDATESFUNCTIONS";	    
					db.transaction(function(tx){
						var updatedId = 0;  
				    	tx.executeSql(sql_max_updated,[],function(tx, results){
				    		var len = results.rows.length;
			    		if(len>0)
			    			updatedId = results.rows.item(0).updateid;
			    		updatedId= updatedId+1;
			    		var emailUpdate = 	updatedId+1;
			    		var phoneUpdate = updatedId+2;		    		
			    		var arr1 = companyImgOnServer+"|>"+info+"|>"+address+"|>"+city+"|>"+country+"|>"+updatedId;			    		
			    		var arr2 = email+"|>"+emailUpdate;			    		
			    		var arr3 = phone+"|>"+phoneUpdate;			    		

			    		console.log("updated function id "+updatedId);
			    		if(is_info_changed)
			    			tx.executeSql(updates,[updatedId, "updateCompanyInfoInServer", arr1, setLastUpdatesTime()],errorCB);
			    		if(is_email_changed)
			    			tx.executeSql(updates,[emailUpdate, "updateCompanyEmailInServer", arr2, setLastUpdatesTime()],errorCB);
			    		if(is_phone_changed)
			    			tx.executeSql(updates,[phoneUpdate, "updateCompanyPhoneInServer", arr3, setLastUpdatesTime()],errorCB);
			    		
			    	},errorCB);
				    }, errorCB, successDB);   			
				}
	
	});
}
/**********************************************************************************************/
function updateCompanyInfoInServer(imgsource, info, address, city, country, last_updated_id, updatedId)
{
var newImage="0";
if(companyImageExt!="")
	newImage="1";
else
	newImage="0";	

var url = "http://cardsbond.com/BondSys/web/api/v1/events/update_company_data?logo="+imgsource+"&brief="+info+"&address="+address+"&city="+city+"&country="+country+"&last_updated_by_bond_id="+last_updated_id+"&bond_company_id="+mycompanyid+"&newImage="+newImage;
console.log("update comp: "+url);
	var ajaxRequest;
		try{
			ajaxRequest = new XMLHttpRequest();
		} catch (e){
			try{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
					//alert("Microsoft");
				} catch (e){
					alert("Your browser broke!");
				return false;
				}
			}
		}		
		
		ajaxRequest.onreadystatechange = function(){
		//alert(ajaxRequest.readyState);
			if(ajaxRequest.readyState == 4){
					var response=ajaxRequest.responseText;
					console.log("company updated response: "+response);
					if(response.search('socialnetwork_id') != -1){					
							var results = JSON.parse(response);
						
	            	}
	        		    		
				}
			}
		
		ajaxRequest.open("GET", url, true);		
		ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		//ajaxRequest.setRequestHeader("Content-length", param.length);
		ajaxRequest.setRequestHeader("Connection", "close");
		ajaxRequest.send();	

}
/********************************************************************************************************/
function updateCompanyEmailInServer(email, updatedId)
{
var url = "http://cardsbond.com/BondSys/web/api/v1/events/update_company_email?bond_company_id="+mycompanyid+"&email="+email;
console.log("update comp: "+url);
	var ajaxRequest;
		try{
			ajaxRequest = new XMLHttpRequest();
		} catch (e){
			try{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
					//alert("Microsoft");
				} catch (e){
					alert("Your browser broke!");
				return false;
				}
			}
		}		
		
		ajaxRequest.onreadystatechange = function(){
		//alert(ajaxRequest.readyState);
			if(ajaxRequest.readyState == 4){
					var response=ajaxRequest.responseText;
					console.log("company email updated response: "+response);
					if(response.search('socialnetwork_id') != -1){					
							var results = JSON.parse(response);
						
	            	}
	        		    		
				}
			}
		ajaxRequest.open("GET", url, true);		
		ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		//ajaxRequest.setRequestHeader("Content-length", param.length);
		ajaxRequest.setRequestHeader("Connection", "close");
		ajaxRequest.send();	

}
/********************************************************************************************************/
function updateCompanyPhoneInServer(phone, updatedId)
{
var url = "http://cardsbond.com/BondSys/web/api/v1/events/update_company_phone?bond_company_id="+mycompanyid+"&phone="+phone;
console.log("update comp: "+url);
	var ajaxRequest;
		try{
			ajaxRequest = new XMLHttpRequest();
		} catch (e){
			try{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
					//alert("Microsoft");
				} catch (e){
					alert("Your browser broke!");
				return false;
				}
			}
		}		
		
		ajaxRequest.onreadystatechange = function(){
		//alert(ajaxRequest.readyState);
			if(ajaxRequest.readyState == 4){
					var response=ajaxRequest.responseText;
					console.log("company updated response: "+response);
					if(response.search('socialnetwork_id') != -1){					
							var results = JSON.parse(response);
						
	            	}
	        		    		
				}
			}
		ajaxRequest.open("GET", url, true);		
		ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		//ajaxRequest.setRequestHeader("Content-length", param.length);
		ajaxRequest.setRequestHeader("Connection", "close");
		ajaxRequest.send();	

}
/****************************************/
function getMyCompanyProfileData()
{
	var companies= 'SELECT * FROM COMPANIES WHERE companyid=(?)';
	var emails= 'SELECT * FROM COMPANIES_EMAILS WHERE companyid=(?)';
	var phones= 'SELECT * FROM COMPANIES_PHONES WHERE companyid=(?)';
	var city, country;
		db.transaction(function(tx){
	    	tx.executeSql(companies,[mycompanyid],function(tx,results){
	    		var len = results.rows.length;
	    		if(len>0){
				var imgsource = results.rows.item(0).imgsource;
				var info = results.rows.item(0).info;
				var address = results.rows.item(0).address;
				city = results.rows.item(0).city_id;
				country = 	results.rows.item(0).country_id;
				document.getElementById('edit_company_profile_picture').src=imgsource;
				mycompanylogo = imgsource;
				document.getElementById('editBriefMyCompany').value=info;
				document.getElementById('updated_company_address').value=address;
				}
			},errorCB);
		
			tx.executeSql(emails,[mycompanyid],function(tx,results){
	    		var len = results.rows.length;
	    		if(len> 0){
				var email = results.rows.item(0).emailvalue;
				var typeid = results.rows.item(0).typeid;
				document.getElementById('updated_company_email').value= email;
				}
			},errorCB);
			
			tx.executeSql(phones,[mycompanyid],function(tx,results){
	    		var len = results.rows.length;
	    		if(len>0){
				var phone = results.rows.item(0).phonevalue;
				var typeid = results.rows.item(0).typeid;
					
				document.getElementById('updated_company_phone').value= phone;
				}
			},errorCB);
		
		},errorCB, successDB);

console.log("company city id"+city);
console.log("company country id"+country);
		var countries= 'SELECT * FROM COUNTRIES';
	
		db.transaction(function(tx){
	    	tx.executeSql(countries,[],function(tx,results){
	    		var len = results.rows.length;		     	   
		    	var myNode = document.getElementById('updated_company_country');
			while (myNode.firstChild) {
	    		myNode.removeChild(myNode.firstChild);
			}		     	   
		        for (var i=0; i<len; i++)     
        		{
     				var name=results.rows.item(i).name;
     				var cid= results.rows.item(i).countryid;
//     				alert("cid: "+cid);
    				var option = document.createElement('option');
     				var t=document.createTextNode(name);
				 	option.setAttribute('id', "company"+cid);
				 	option.appendChild(t);
	 				if(cid==country){
						console.log("mycountry: "+mycountry);					
					 	option.setAttribute('selected', 'true');
					}
					document.getElementById('updated_company_country').appendChild(option);
				}
	    	}
	    	,errorCB);
	        }, errorCB, successDB);		

		var cities= 'SELECT * FROM CITIES';
	
		db.transaction(function(tx){
	    	tx.executeSql(cities,[],function(tx,results){
	    		var len = results.rows.length;		     	   
		    	var myNode = document.getElementById('updated_company_city');
			while (myNode.firstChild) {
	    		myNode.removeChild(myNode.firstChild);
			}		     	   
		        for (var i=0; i<len; i++)     
        		{
     				var name=results.rows.item(i).name;
     				var cityid= results.rows.item(i).cityid;
					var option = document.createElement('option');
     				var t=document.createTextNode(name);
				 	option.setAttribute('id', "company"+cityid);
				 	option.appendChild(t);					
					if(cityid==city){
					 	option.setAttribute('selected', 'true');
					}
					document.getElementById('updated_company_city').appendChild(option);
				}
	    	}
	    	,errorCB);
	        }, errorCB, successDB);		
}
/**********************************************************/
function togglePlusInCompany(e)
{
click_time = e['timeStamp'];
if (click_time && (click_time - last_click_time) < 2000)
{
    e.stopImmediatePropagation()
	e.preventDefault()
	  	return false
}
	    else
	    {
	  		last_click_time = click_time;
            navigator.notification.vibrate(50);
            $('#company-info').show(); $('#pub-minus-company').show(); $('#pub-plus-company').hide();
            document.getElementById('pub-plus-company').style.color = 'black';
  	   	 	document.getElementById('pub-minus-company').style.color = 'blue';
                	

		}
		return true;
	
}
/*******************************************/
function toggleMinusInCompany(e)
{
	click_time = e['timeStamp'];
	if (click_time && (click_time - last_click_time) < 2000)
	{
	    e.stopImmediatePropagation()
		e.preventDefault()
		  	return false
	}
	else
	{
	  		last_click_time = click_time;
   
	navigator.notification.vibrate(50);
    $('#company-info').hide(); $('#pub-plus-company').show(); $('#pub-minus-company').hide();
    document.getElementById('pub-plus-company').style.color = 'blue';
    document.getElementById('pub-minus-company').style.color = 'black';
    
	}
		return true;

}
/****************************************************************************/
function toggleCompanyDivInProfile(e)
{
	click_time = e['timeStamp'];
	if (click_time && (click_time - last_click_time) < 2000)
	{
	    e.stopImmediatePropagation()
		e.preventDefault()
		  	return false
	}
	else
	{
 		last_click_time = click_time;
		navigator.notification.vibrate(50);

    	if(document.getElementById('pub-plus-company').style.color == 'blue'){
    	   	$('#company-info').show(); $('#pub-minus-company').show(); $('#pub-plus-company').hide();
    	   	document.getElementById('pub-plus-company').style.color = 'black';
    	   	document.getElementById('pub-minus-company').style.color = 'blue';
    	   	}
    	
    	else{
    	    $('#company-info').hide(); $('#pub-plus-company').show(); $('#pub-minus-company').hide();
    	    document.getElementById('pub-minus-company').style.color = 'black';
    	   	document.getElementById('pub-plus-company').style.color = 'blue';
    	    
    	}
                
 	}
	return true;

}

function getCompanyPhoto(source) {
	if(saveUpdatedCompanydbclick%2==0){
      navigator.camera.getPicture(onCompanyPhotoURISuccess, onFail, { //quality: 25,
      		//targetWidth: 135,
  			//targetHeight: 100,
  		    correctOrientation: true,
		    allowEdit: true,
		    
        destinationType: destinationType.FILE_URI,
        sourceType: source });
        }
        saveUpdatedCompanydbclick++;
}
/********************************************/
function onCompanyPhotoURISuccess(companyImageURI) {   
	console.log("companyImageURI"+companyImageURI);
	document.getElementById('edit_company_profile_picture').src = compa3nyImageURI;
	
	
    var download_link = encodeURI(companyImageURI);
	var date = new Date();
	var dateMillis = date.getTime();

	var fname=dateMillis+mycompanyid+id;
	var ext_ = download_link.substr(download_link.lastIndexOf('.')+1);
	console.log("ext_"+ ext_);		    	
	var ext = ext_.substr(0,3); //Get extension of URL
	console.log("ext"+ ext);
	var target = Folder_Name + "/" + fname + "." + ext; // fullpath and name of the file which we want to give			
	filetransfer(download_link, target);			
	
    companyImage.src= "file:/"+target;
        	
     mycompanylogo = target;
     console.log("mycompanyid:"+mycompanyid);
	var photo_update= 'UPDATE COMPANIES set imgsource=(?) WHERE companyid=(?)';
	db.transaction(function(tx){
	   	tx.executeSql(photo_update,[mycompanylogo, mycompanyid]);
	   		},errorCB, successDB);
	   	
     
    uploadCompanyPhoto(target);
}
/********************************************/
function uploadCompanyPhoto(company_imageURI) 
{   
	console.log("company uri: "+company_imageURI);
	networkState = checkConnection();
    if(networkState!="none" && networkState!="unknown"){
	var options = new FileUploadOptions();
    options.fileKey="file";
    options.fileName="company_"+mycompanyid;
    options.mimeType="image/jpeg";
    options.chunkedMode = false;
    var ft = new FileTransfer();
    ft.upload(company_imageURI, encodeURI("http://www.cardsbond.com/m/upload_company.php"), company_win, fail, options);
	}
	else
	{
	    var updates= 'INSERT INTO UPDATESFUNCTIONS(updateid, name, parameters, datetime) VALUES (?,?,?, ?)';
	    var sql_max_updated = "SELECT MAX(updateid) as updateid FROM UPDATESFUNCTIONS";	    
		db.transaction(function(tx){
			var updatedId = 0;  
	    	tx.executeSql(sql_max_updated,[],function(tx, results){
			var len = results.rows.length;
			if(len>0)
				updatedId = results.rows.item(0).updateid;
				updatedId= updatedId+1;			    		
				var arr1 = company_imageURI+"|>"+updatedId;			    		
			    	console.log("updated function id "+updatedId);
			    	tx.executeSql(updates,[updatedId, "uploadCompanyPhoto", arr1, setLastUpdatesTime()],errorCB);
			    		
			    	},errorCB);
				    }, errorCB, successDB);   								
	}
} 


function company_win(r) 
{ 
	companyImageExt = r.response;	
}
