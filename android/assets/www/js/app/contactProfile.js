function fun1(othercontactid)
{
	var mid=''+othercontactid+'';
	otherContactFamilyIcon = "false";
	otherContactFriendsIcon = "false";
	otherContactBusinessIcon = "false"; 
	otherContactFavoriteIcon = "false";
	is_there_relation = "false";
	
	var sqltxt= 'SELECT * FROM CONTACTSCONNECTIONLEVELS WHERE contactid=(?) AND othercontactid=(?)';
	db.transaction(function(ab){
	    ab.executeSql(sqltxt,[id, mid],
	    function(ab,result){
	    //   alert("len: "+result.rows.length);
	    	var len = result.rows.length;
		    for(var i=0; i<len;i++)
		    {
		    	console.log("type"+result.rows.item(i).type);
		    	is_there_relation="true";
		    	if(result.rows.item(i).type=="supplier"){
		    		otherContactFamilyIcon = result.rows.item(i).typevalue;
				}
				else if(result.rows.item(i).type=="customer"){
					otherContactFriendsIcon = result.rows.item(i).typevalue;
				}
				else if(result.rows.item(i).type=="partner")
				{
					otherContactBusinessIcon = result.rows.item(i).typevalue;
				}
				else if(result.rows.item(i).type=="favorite")
					otherContactFavoriteIcon = result.rows.item(i).typevalue;							
			}
			console.log("getIconsImages"+mid);	
			getIconsImages(mid);					
	    },errorCB);
	}, errorCB, successDB);	             
}
/**************************************************************************************************************************/
function getIconsImages(conId)
{
	var mid=''+conId+'';
	//	alert("getIconsImages: "+mid);
   	var sqltxt= 'SELECT * FROM CONTACTS, COUNTRIES, CITIES  WHERE contactid=(?) ';
	db.transaction(function(tx){
		tx.executeSql(sqltxt,[mid],conProfileSuccess,errorCB);
	}, errorCB, successDB);   
}	
/************************************************************************/	
function drawMyContactProfileImg(img) {
	console.log("img"+img);
	javascript:menu('17');
    var c = document.getElementById("myContactImageFull");
	c.src= img;
}
/************************************************************************/
function conProfileSuccess(tx, results){
	var len = results.rows.length;         
    if(len>=1)
    {
      	var contact_id = results.rows.item(0).contactid;
      	myCurrentContactId= contact_id;         	
     	var contact_company=results.rows.item(0).company;
     	var contact_companyid=results.rows.item(0).companyid;

    	var contact_position=results.rows.item(0).position;
    	var contact_name=results.rows.item(0).fname+" "+results.rows.item(0).lname;
    	var contact_image = results.rows.item(0).imgsource;
    	var contact_aboutme = results.rows.item(0).aboutme;
		var contact_wants = results.rows.item(0).wants;		
		var contact_address = results.rows.item(0).address;		
		var contact_countryid = results.rows.item(0).countryid;
		var contact_cityid = results.rows.item(0).cityid;
		myCurrentContactCompany=contact_company;
		myCurrentContactPosition=contact_position;
		
		var div1 = document.createElement("div");
		div1.setAttribute('class', 'prof-i');
		div1.setAttribute('id', 'contact_prof');
		
		var image1 = document.createElement("img");
		image1.setAttribute('class', 'profile_img');
		image1.setAttribute('src', contact_image);
		image1.setAttribute('id','conImage'+contact_id);
		//ref = window.open(facebookURL+url, '_system');
		// self, system gallery but cordova not defined
		//blank, top, parent web page and cant back
		image1.setAttribute("onClick", " drawMyContactProfileImg(\'"+contact_image+"'\)");

		div1.appendChild(image1);
			
		
		var div2 = document.createElement("div");
		div2.setAttribute('class', 'prof-d');
		var strong1 = document.createElement("strong");
		strong1.setAttribute('id', 'contact_profile_name');
		strong1.setAttribute('style', 'font-size: 20px; color:#ce3474; ');
		
		
		var p1 = document.createElement("p");
		p1.setAttribute('id', 'contact_profile_position');
		p1.setAttribute('style', 'font-size: 20px;');
		
		var p2 = document.createElement("p");
		p2.setAttribute('id', 'contact_profile_company');
		p2.setAttribute('style', 'font-size: 20px; font-weight: bold;');
		
		
		var p3 = document.createElement("input");
		p3.setAttribute('id', 'contact_profile_id');
		p3.setAttribute('type', 'hidden');
		p3.setAttribute('value', '');
		
		div2.appendChild(strong1);
		div2.appendChild(p1);
		div2.appendChild(p2);
		div2.appendChild(p3);
		
		
		var div3 = document.createElement("div");
		div3.setAttribute('class', 'clear');
		var div4 = document.createElement("div");
		div4.setAttribute('class', 'prof-rel');
		var a3 = document.createElement("a");
		a3.setAttribute('href', '#');
		
		var a4 = document.createElement("a");
		a4.setAttribute('href', '#');
		
		var a5 = document.createElement("a");
		a5.setAttribute('href', '#');
		
		var familyIconSrc="css/imgs/icons/family-in.png";
		
		if(otherContactFamilyIcon=="true")
		{
			 familyIconSrc='css/imgs/icons/family.png';
		}
		
		if(otherContactFamilyIcon=="false")
		{
			familyIconSrc='css/imgs/icons/family-in.png';
			
		}
			
		var a6 = document.createElement("a");
		a6.setAttribute('href', '#');
		
		var a7 = document.createElement("a");
		a7.setAttribute('href', '#');
		
		var img1 = document.createElement("img");
		img1.setAttribute('src', familyIconSrc);
		img1.setAttribute('id', 'family'+contact_id);

		img1.addEventListener('click',function (e){
			click_time = e['timeStamp'];
			if (click_time && (click_time - last_click_time) < 2000)
		  	{
			    e.stopImmediatePropagation();
			    e.preventDefault();
			    return false;
		    }
		    else
		    {
				last_click_time = click_time;
				navigator.notification.vibrate(100);
				addToFamilyGroup(contact_id,'supplier', contact_name);
		
			}
		});

var friendsIconSrc="css/imgs/icons/friends-in.png";

if(otherContactFriendsIcon=="true")
{
	 friendsIconSrc='css/imgs/icons/friends.png';
//	 alert("true : "+friendsIconSrc);
	}
if(otherContactFriendsIcon=="false")
{
	friendsIconSrc='css/imgs/icons/friends-in.png';
//		 alert("false : "+friendsIconSrc);
	
	}
	var img2 = document.createElement("img");
	img2.setAttribute('src', friendsIconSrc);
	img2.setAttribute('id', 'friends'+contact_id);
	img2.addEventListener('click',function (e){
		click_time = e['timeStamp'];
		  if (click_time && (click_time - last_click_time) < 2000)
		  {
		  	e.stopImmediatePropagation();
		    e.preventDefault();
		    return false;
		  }
		  else
		  {
				last_click_time = click_time;
//				alert("call add to family"+contact_id);
				navigator.notification.vibrate(100);
				addToFamilyGroup(contact_id, 'customer', contact_name);
		
			}
		});
		

var favoriteIconSrc="css/imgs/icons/favorite-in.png";

if(otherContactFavoriteIcon=="true")
{
	 favoriteIconSrc='css/imgs/icons/favorite.png';
	}
if(otherContactFavoriteIcon=="false")
{
	favoriteIconSrc='css/imgs/icons/favorite-in.png';	
}
var img3 = document.createElement("img");
img3.setAttribute('src', favoriteIconSrc);
img3.setAttribute('id', 'favorite'+contact_id);
	img3.addEventListener('click',function (e){
		  click_time = e['timeStamp'];		
		  if (click_time && (click_time - last_click_time) < 2000)
		  {
		    e.stopImmediatePropagation();
		    e.preventDefault();
		    return false;
		    }
		    else
		    {
				last_click_time = click_time;
				navigator.notification.vibrate(100);
				addToFamilyGroup(contact_id, 'favorite', contact_name);		
			}
		});

	var businessIconSrc="css/imgs/icons/business-in.png";
	if(otherContactBusinessIcon=="true")
	{
		 businessIconSrc='css/imgs/icons/business.png';
	}
	if(otherContactBusinessIcon=="false")
	{
	businessIconSrc='css/imgs/icons/business-in.png';
	
	}
	var busIcId = 'business'+contact_id;	
	var img5 = document.createElement("img");
	img5.setAttribute('src', businessIconSrc);
	img5.setAttribute('id', busIcId);
	img5.addEventListener('click',function (e){
		  click_time = e['timeStamp'];
		  if (click_time && (click_time - last_click_time) < 2000)
		  {
		    e.stopImmediatePropagation();
		    e.preventDefault();
		    return false;
		    }
		    else
		    {
				last_click_time = click_time;
				navigator.notification.vibrate(100);
				addToFamilyGroup(contact_id, 'partner', contact_name);
		
			}
		});

var img4 = document.createElement("img");
img4.setAttribute('src', 'css/imgs/icons/icons1.png');
img4.setAttribute('id', 'remove'+contact_id);
img4.setAttribute('onClick', 'removeContact('+ contact_id +')');

a3.appendChild(img1);
a4.appendChild(img2);
a5.appendChild(img3);
a6.appendChild(img4);
a7.appendChild(img5);

if(is_there_relation=="true"){
div4.appendChild(a3);
div4.appendChild(a4);
div4.appendChild(a7);
div4.appendChild(a5);
div4.appendChild(a6);
}


document.getElementById('myContactProfile').appendChild(div1);
document.getElementById('myContactProfile').appendChild(div3);
document.getElementById('myContactProfile').appendChild(div2);
document.getElementById('myContactProfile').appendChild(div4);

document.getElementById('contact_profile_name').innerHTML=contact_name; 
document.getElementById('contact_profile_company').innerHTML=contact_company;
document.getElementById('contact_profile_position').innerHTML=contact_position;
document.getElementById('contact_profile_id').value=contact_id;
  
	var city_name="", country_name=""; 

	if(contact_address=="null" || contact_address== null)
		contact_address="";

	if(is_there_relation=="true"){
	
        var country=   'SELECT * FROM COUNTRIES WHERE countryid=(?)';
        var city=   'SELECT * FROM CITIES WHERE cityid=(?)';

		tx.executeSql(city,[contact_cityid],function (cityax, cityres){
    	var cityLen = cityres.rows.length;
		if(cityLen >= 1){

			city_name = cityres.rows.item(0).name;
			if(city_name=="null" || city_name== null)
				city_name="";

			document.getElementById("addressvalues"+contact_id).innerHTML = contact_address + "<br>" + city_name + "  "+ country_name;

			}
	    },errorCB);	    

		tx.executeSql(country,[contact_countryid],function (countryax, countryres){
    	var countryLen = countryres.rows.length;
		if(countryLen >= 1)
			country_name = countryres.rows.item(0).name;
		if(country_name=="null" || country_name== null)
			country_name="";

			document.getElementById("addressvalues"+contact_id).innerHTML = contact_address + "<br>" + city_name +" "+ country_name;
	    	},errorCB);	    
		}  
		showContactTags(contact_id,contact_name,contact_aboutme,contact_wants, contact_address, city_name, country_name, contact_companyid);
    }
}
/**********************************************************************************************************************************/
function showContactTags(conId,contactName,contactAboutMe,contact_wants, contact_address, city_name, country_name, contact_companyid)
{
    var tags= 'SELECT * FROM CONTACTSCONNECTIONTAGS WHERE contactid=(?) AND othercontactid=(?)';

	db.transaction(function(tx){
    	tx.executeSql(tags,[id, conId],
    	function(tx,results)
    	{
	        var len = results.rows.length;
	        for (var i=0; i<len; i++)     
       		{
   				var tagValue=results.rows.item(i).tags;
   				if(tagValue!=""){
     				var t=document.createTextNode(tagValue);
					var aId= conId+i;   
//					alert(aId);	
									
				 	var param = document.createElement('a');
				 	param.setAttribute('id', aId);
				 	param.setAttribute('onClick', 'removeContactTag('+ aId +')');
				 	
				 	var j = document.createElement('i');
				 	j.setAttribute('style','margin-left:4px');
				 	j.setAttribute('class', "fa fa-times");
					param.appendChild(t);
				 	param.appendChild(j);
				 	
					document.getElementById('tags-l2').appendChild(param);
				}
			}
	    }
	    	,errorCB);
	        }, errorCB, successDB);   
	if(is_there_relation=="true"){
		completeConProfile(conId,contactName,contactAboutMe,contact_wants, contact_address, city_name, country_name, contact_companyid);
	}
}
/***********************************************************************************************************************************/
function completeConProfile(conId, contactName, contactAboutMe, contact_wants, contact_address, city_name, country_name, contact_companyid)
{
	var div1 = document.createElement('div');
	div1.setAttribute('class', "box white");
	
	var div2 = document.createElement('div');
	div2.setAttribute('class', "prof-menu");
	
	var div3 = document.createElement('div');
	div3.setAttribute('class', "prof-id");
	
	var span1 = document.createElement('span');
	span1.setAttribute('id', "aboutme"+conId);
	
	var p1 = document.createElement('p');
	p1.setAttribute('id', "paragraph"+conId);

	var div4 = document.createElement('div');
	div4.setAttribute('class', "prof-id");
	
	var span2 = document.createElement('span');
	span2.setAttribute('id', "wants"+conId);
	
	var p2 = document.createElement('p');
	p2.setAttribute('id', "wantsvalues"+conId);
	
	var span4 = document.createElement('span');
	span4.setAttribute('id', "address"+conId);

	var p4 = document.createElement('p');
	p4.setAttribute('id', "addressvalues"+conId);
	
	var ul1 = document.createElement('ul');
        var mycontactmobile='SELECT * FROM MOBILES WHERE contactid=(?)';
        var mycontactemail= 'SELECT * FROM EMAILS WHERE contactid=(?)';
        var mycontactphone= 'SELECT * FROM PHONES WHERE contactid=(?)';
        var mycontactfax=   'SELECT * FROM FAXES WHERE contactid=(?)';

	
		db.transaction(function(tx){
	    tx.executeSql(mycontactmobile,[conId],function (mobileax, mobileres){
    	var mobileLen = mobileres.rows.length;
   		if(mobileLen>0)
			ul1.appendChild(document.createElement('hr'));
   			
   			for (var i=0; i<mobileLen; i++){        
	    	var li1 = document.createElement('li');
			var i1 = document.createElement('i');
			i1.setAttribute('id','conmobi'+i+''+conId);
			i1.setAttribute('class','fa fa-mobile');
			li1.setAttribute('style','font-weight: bold;');
			
			var t1 = document.createTextNode("+"+mobileres.rows.item(i).mobilevalue);
			var mobiletypes = mobileres.rows.item(i).typeid.split(",");

			console.log(mobiletypes[0]+conId);

			i1.appendChild(document.createTextNode( '\u00A0' ));				
			i1.appendChild(t1);
			i1.setAttribute("onClick","addToMyLog(\'"+mobileres.rows.item(i).mobilevalue+"'\)");

			li1.appendChild(i1);
			var msg_i = document.createElement("i");
			msg_i.setAttribute('id',mobiletypes[0]+conId);

			msg_i.setAttribute('class','fa fa-comment com');					
			msg_i.setAttribute("onClick","browseSMS(\'"+mobileres.rows.item(i).mobilevalue+"'\)");
			li1.appendChild(msg_i);										
			ul1.appendChild(li1);
			

/*			for(var t=0;t<mobiletypes.length;t++)
			{
				if(mobiletypes[t]=="1" && otherContactFamilyIcon=="true")
				{
					console.log(mobiletypes[t]+conId+t);
					i1.appendChild(document.createTextNode( '\u00A0' ));				
					i1.appendChild(t1);
					li1.appendChild(i1);					
					var msg_i = document.createElement("i");
					msg_i.setAttribute('id',mobiletypes[t]+conId+t);

					msg_i.setAttribute('class','fa fa-comment com');
					//msg_i.setAttribute("onClick","window.MainActivity.browseSMS(\'"+mobileres.rows.item(i).mobilevalue+"'\)");
					msg_i.setAttribute("onClick","browseSMS(\'"+mobileres.rows.item(i).mobilevalue+"'\)");
					
					li1.appendChild(msg_i);										
					ul1.appendChild(li1);
				}
				else if(mobiletypes[t]=="4" && otherContactBusinessIcon=="true")
				{
					console.log(mobiletypes[t]+conId+t);

					i1.appendChild(document.createTextNode( '\u00A0' ));				
					i1.appendChild(t1);
					li1.appendChild(i1);
					var msg_i = document.createElement("i");
					msg_i.setAttribute('id',mobiletypes[t]+conId+t);

					msg_i.setAttribute('class','fa fa-comment com');

					msg_i.setAttribute("onClick","browseSMS(\'"+mobileres.rows.item(i).mobilevalue+"'\)");
					li1.appendChild(msg_i);					
					
					ul1.appendChild(li1);
				}
				else if(mobiletypes[t]=="5" && otherContactFriendsIcon=="true")
				{
					console.log(mobiletypes[t]+conId+t);

					i1.appendChild(document.createTextNode( '\u00A0' ));				
					i1.appendChild(t1);
					li1.appendChild(i1);

					var msg_i = document.createElement("i");
					msg_i.setAttribute('id',mobiletypes[t]+conId+t);
					msg_i.setAttribute('class','fa fa-comment com');					
					msg_i.setAttribute("onClick","browseSMS(\'"+mobileres.rows.item(i).mobilevalue+"'\)");
					li1.appendChild(msg_i);										
					ul1.appendChild(li1);
				}
			}*/
			/*if(i== mobileLen-1)
					ul1.appendChild(document.createElement('hr'));
			*/
	 
       	}
    },errorCB);	    

		///////////////////////////	

		tx.executeSql(mycontactphone,[conId],function (phoneax, phoneres){
    	var phoneLen = phoneres.rows.length;
    	if(phoneLen>0)
			ul1.appendChild(document.createElement('hr'));

    	for (var i=0; i<phoneLen; i++){        
		    	var li1 = document.createElement('li');
				var i1 = document.createElement('i');
				i1.setAttribute('id','conphone'+i+''+conId);
				i1.setAttribute('class','fa fa-phone');
				var t1 = document.createTextNode("+"+phoneres.rows.item(i).phonevalue);
				var phonetypes = phoneres.rows.item(i).typeid.split(",");
				for(var t=0;t<phonetypes.length;t++)
				{
					/*
					if(phonetypes[t]=="1" && otherContactFamilyIcon=="true")
					{
						i1.appendChild(document.createTextNode( '\u00A0' ));				
						i1.appendChild(t1);
						li1.appendChild(i1);
						ul1.appendChild(li1);
					}
					else if(phonetypes[t]=="4" && otherContactBusinessIcon=="true")
					{
						i1.appendChild(document.createTextNode( '\u00A0' ));
					
						i1.appendChild(t1);
						li1.appendChild(i1);
						ul1.appendChild(li1);
					}
					else if(phonetypes[t]=="5" && otherContactFriendsIcon=="true")
					{
						i1.appendChild(document.createTextNode( '\u00A0' ));
					
						i1.appendChild(t1);
						li1.appendChild(i1);
						ul1.appendChild(li1);
					}*/
					i1.appendChild(document.createTextNode( '\u00A0' ));
					
					i1.appendChild(t1);
					li1.appendChild(i1);
					ul1.appendChild(li1);
				}
					
	
        	}
	    },errorCB);	    
		///////////////////////////	
		tx.executeSql(mycontactemail,[conId],function (emailax, emailres){
    	var emailLen = emailres.rows.length;
    	if(emailLen>0)
			ul1.appendChild(document.createElement('hr'));

    	for (var i=0; i<emailLen; i++){
    			var li1 = document.createElement('li');
				var i1 = document.createElement('i');
				var a1 = document.createElement('a');
				a1.setAttribute('id','conemail_a'+i+''+conId);

				i1.setAttribute('id','conemail'+i+''+conId);
				i1.setAttribute('class','fa fa-envelope');
				var t1 = document.createTextNode(emailres.rows.item(i).emailvalue);
				var emailtypes = emailres.rows.item(i).typeid.split(",");

				i1.appendChild(document.createTextNode( '\u00A0' ));
				//i1.setAttribute("onClick", "alert('tete'); emailtypes[0]+conId :"+emailres.rows.item(i).emailvalue);
				i1.setAttribute("onClick", "browseEmail(\'"+emailres.rows.item(i).emailvalue+"'\ , \'"+'conemail_a'+i+''+conId+"'\)");							
					
				a1.appendChild(i1);				
				i1.appendChild(t1);
				li1.appendChild(a1);
				ul1.appendChild(li1);

/*			for(var t=0;t<emailtypes.length;t++)
			{
				if(emailtypes[t]=="1" && otherContactFamilyIcon=="true")
				{
					i1.appendChild(document.createTextNode( '\u00A0' ));
					i1.setAttribute("onClick", "mailto:"+emailres.rows.item(i).emailvalue);
				
					i1.appendChild(t1);
					li1.appendChild(i1);
					ul1.appendChild(li1);
				}
				else if(emailtypes[t]=="4" && otherContactBusinessIcon=="true")
				{
					i1.appendChild(document.createTextNode( '\u00A0' ));
					i1.setAttribute("onClick", "mailto:"+emailres.rows.item(i).emailvalue);
					i1.appendChild(t1);
					li1.appendChild(i1);
					ul1.appendChild(li1);
				}
				else if(emailtypes[t]=="5" && otherContactFriendsIcon=="true")
				{
					i1.appendChild(document.createTextNode( '\u00A0' ));
					i1.setAttribute("onClick", "mailto:"+emailres.rows.item(i).emailvalue);				
					i1.appendChild(t1);
					li1.appendChild(i1);
					ul1.appendChild(li1);
				}
			}	
*/
        }
	   },errorCB);	    

		///////////////////////////	
		tx.executeSql(mycontactfax,[conId],function (faxax, faxres){
    	var faxLen = faxres.rows.length;
    	if(faxLen>0)	
			ul1.appendChild(document.createElement('hr'));
    	for (var i=0; i<faxLen; i++){        	
    			var li1 = document.createElement('li');
				var i1 = document.createElement('i');
				var a1 = document.createElement('a');
				a1.setAttribute('id','confax_a'+i+''+conId);

				i1.setAttribute('id','confax'+i+''+conId);
				i1.setAttribute('class','fa fa fa-fax');
				var t1 = document.createTextNode("+"+faxres.rows.item(i).faxvalue);
				var faxtypes = faxres.rows.item(i).typeid.split(",");

				i1.appendChild(document.createTextNode( '\u00A0' ));
				//i1.setAttribute("onClick", "alert('tete'); emailtypes[0]+conId :"+emailres.rows.item(i).emailvalue);
				//i1.setAttribute("onClick", "browseEmail(\'"+faxres.rows.item(i).faxvalue+"'\ , \'"+'confax_a'+i+''+conId+"'\)");							
					
				a1.appendChild(i1);				
				i1.appendChild(t1);
				li1.appendChild(a1);
				ul1.appendChild(li1);
    		
    		
    		}
	
		
		},errorCB);	    

		
	}, errorCB, successDB);
	
	
/*
//fax not included in design
	for(var k=0;k<=confaxArrCounter;k++)
	{
		var li1 = document.createElement('li');
		var i1 = document.createElement('i');
		i1.setAttribute('id','confax'+k+''+conId);
		i1.setAttribute('class','fa fa fa-fax');
		var t1 = document.createTextNode(confaxArr[k]);
		i1.appendChild(t1);
		li1.appendChild(i1);
		ul1.appendChild(li1);		
	}
*/
	div3.appendChild(span1);
	div3.appendChild(p1);
	div3.appendChild(document.createElement('hr'));

	div4.appendChild(span2);
	div4.appendChild(p2);
	div4.appendChild(document.createElement('hr'));

	div4.appendChild(span4);
	div4.appendChild(p4);
	//div4.appendChild(document.createElement('hr'));

	div2.appendChild(div3);
	div2.appendChild(div4);

	div2.appendChild(ul1);
	
	document.getElementById('aboutContact').appendChild(div2);
	document.getElementById("aboutme"+conId).innerHTML = "About "+ contactName ;
	document.getElementById("paragraph"+conId).innerHTML = contactAboutMe;
	
	document.getElementById("wants"+conId).innerHTML = "Keywords ";
	document.getElementById("wantsvalues"+conId).innerHTML = contact_wants;

	document.getElementById("address"+conId).innerHTML = "Personal Address ";
	document.getElementById("addressvalues"+conId).innerHTML = contact_address + "<br>" + city_name +  country_name;

	showContactComments(conId,contactName, contact_companyid);
}
/**************************************************************/
function showTasks(conId, name)
{
	var input = document.createElement('input');
	input.setAttribute('type','button');
	input.setAttribute('value','Add Task to do with '+name);
	input.setAttribute('class','tag-btn');
	input.setAttribute("onClick","addTask(\'"+conId+"\' , \'"+name+"\' )");

	var sql = "SELECT * FROM CONTACTTASKS WHERE contactid=(?)";
	db.transaction(function(tx){
	    	tx.executeSql(sql,[conId],function(ax,res){
	    	var total_tasks = res.rows.length;
	    	console.log("total_tasks", total_tasks);
		},errorCB);
	}, errorCB, successDB);   
	document.getElementById("task_contact").appendChild(input);								
}
/*************************************************/
function addTask(conId, name){
if(addTaskdbclk%2==0){
	console.log(conId);
	/*window.MainActivity.openCalendar(conId);
	window.MainActivity.GetMaxID();
	window.MainActivity.list_tasks();
	showTasks(conId, name);
	*/
	var success = function(result) { alert(result) };
	var error = function(message) { console.log("Oopsie! " + message); };
	
task.createEvent(success, error);	
	showContactSocial(conId, name);
}
addTaskdbclk++;
}
/*************************************************/
function showContactSocial(conId)
{
	var my = document.getElementById("contactProfSocial");
	if(my){
		while (my.firstChild) {
		my.removeChild(my.firstChild);
	}	
	}		
confacebook="";
contwitter="";
conlinkedin="";
conskype="";
congoogle="";
var a1= document.createElement('a');
a1.setAttribute('href', '#');
a1.setAttribute('id','contactProfFacebook'+conId);
var i1= document.createElement('i');
i1.setAttribute('class','fa fa-facebook');
a1.appendChild(i1);

var a2= document.createElement('a');
a2.setAttribute('href', '#');
a2.setAttribute('id','contactProfTwitter'+conId);
var i2= document.createElement('i');
i2.setAttribute('class','fa fa-twitter');
a2.appendChild(i2);

var a3= document.createElement('a');
a3.setAttribute('href', '#');
a3.setAttribute('id','contactProfLinkedin'+conId);
var i3= document.createElement('i');
i3.setAttribute('class','fa fa-linkedin');
a3.appendChild(i3);

var a4= document.createElement('a');
a4.setAttribute('href', '#');
a4.setAttribute('id','contactProfSkype'+conId);
var i4= document.createElement('i');
i4.setAttribute('class','fa fa-skype');
a4.appendChild(i4);

        var mycontactsocial= 'SELECT * FROM CONTACTSOCIALNETWORKS WHERE contactid=(?)';

	
		db.transaction(function(tx){
	    tx.executeSql(mycontactsocial,[conId],function (ax, res){
    	var socialLen = res.rows.length;
    		for (var i=0; i<socialLen; i++){        
        	if(res.rows.item(i).socialname=="Facebook")
        	{
	    		confacebook=facebookURL+res.rows.item(i).url;
				document.getElementById("contactProfSocial").appendChild(a1);
				document.getElementById('contactProfFacebook'+conId).addEventListener('click', function (e) { 
								e.preventDefault();
								navigator.notification.vibrate(100); 
								
								ref = window.open(confacebook, '_system');
																				
				}, false);			
							
				//document.getElementById('contactProfFacebook'+conId).href=confacebook;
	    	}
	     	if(res.rows.item(i).socialname=="Twitter"){
	    		contwitter=twitterURL+res.rows.item(i).url;
				document.getElementById("contactProfSocial").appendChild(a2);								
				//document.getElementById('contactProfTwitter'+conId).href=contwitter;
				document.getElementById('contactProfTwitter'+conId).addEventListener('click', function (e) { 
								e.preventDefault();
								navigator.notification.vibrate(100); 
								
								ref = window.open(contwitter, '_system');
																				
				}, false);	
	    	}
	     	if(res.rows.item(i).socialname=="LinkedIn"){
	    		conlinkedin=linkedinURL+res.rows.item(i).url;
	    		document.getElementById("contactProfSocial").appendChild(a3);								
	    		//document.getElementById('contactProfLinkedin'+conId).href=conlinkedin;
	    		document.getElementById('contactProfLinkedin'+conId).addEventListener('click', function (e) { 
								e.preventDefault();
								navigator.notification.vibrate(100); 
								
								ref = window.open(conlinkedin, '_system');
																				
				}, false);	
	    		
	    	}
	     	if(res.rows.item(i).socialname=="Skype"){
	    		conskype=skypeURL+res.rows.item(i).url;
	    		document.getElementById("contactProfSocial").appendChild(a4);								
				//document.getElementById('contactProfSkype'+conId).href=conskype;
				document.getElementById('contactProfSkype'+conId).addEventListener('click', function (e) { 
								e.preventDefault();
								navigator.notification.vibrate(100); 
								
								ref = window.open(conskype, '_system');
																				
				}, false);		    		
	    	}
	     	if(res.rows.item(i).socialname=="Google"){
	    		congoogle=res.rows.item(i).url;
	    	//	document.getElementById("contactProfSocial").appendChild(a4);								
	    		
	    		//document.getElementById('contactProfGoogle'+conId).href=congoogle;
	    		
	    	}
	    	}
	    },errorCB);	    
}, errorCB, successDB);
}
/************************************************************/
function getCurrentDateAndTime()
{
	var today=new Date();
	var h=today.getHours();
	var m=today.getMinutes();
	var s=today.getSeconds();
	m = checkTime(m);
	s = checkTime(s);	
	
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yyyy = today.getFullYear();

	if(dd<10) {
    	dd='0'+dd;
    }	 

	if(mm<10) {
    	mm='0'+mm;
	} 

	today = dd+'-'+mm+'-'+yyyy;
	var time = h+":"+m+":"+s;
	var fullDate = today+' '+time;
	return fullDate;
}
/*************************************************/

function removeContactTag(x)
{
	if(dbclick%2 != 0){
		var tagValue = document.getElementById(x).textContent;
		var bb = document.getElementById(x);
 		document.getElementById('tags-l2').removeChild(bb);
 		var conId = document.getElementById('contact_profile_id').value;
		var otherContactId = ''+conId+'';
		var delTag= ' DELETE FROM CONTACTSCONNECTIONTAGS WHERE contactid=(?) AND othercontactid=(?) AND tags=(?)';	
    	db.transaction(function(tx){
            tx.executeSql(delTag,[id, conId, tagValue]);
    	}, errorCB, function(tx,results){

    	networkState = checkConnection();
		if(networkState!="none" && networkState!="unknown")			
		   	removeNewContactTagFromServer(conId, tagValue);
		else{
	 		var updates= 'INSERT INTO UPDATESFUNCTIONS(updateid, name, parameters, datetime) VALUES (?,?,?,?)';
		    var sql_max_updated = "SELECT MAX(updateid) as updateid FROM UPDATESFUNCTIONS";	    
			db.transaction(function(tx){
				var updatedId = 0, arr1="";  
		    	tx.executeSql(sql_max_updated,[],function(tx, results){
		    		var len = results.rows.length;
		    		if(len>0)
			    		updatedId = results.rows.item(0).updateid;
		    		updatedId= updatedId+1;
		    		arr1 = conId+"|>"+tagValue;
		    		console.log("arr1"+arr1);
		    		console.log("updated function id "+updatedId);
				    tx.executeSql(updates,[updatedId, "removeNewContactTagFromServer", arr1, setLastUpdatesTime()]);
		   			},errorCB);		
				}, errorCB, successDB);
	 		}			

   		});
 	}
    dbclick++;
}
/*******************************************************************************************************************/
function removeNewContactTagFromServer(conId, tag){
	var server_tag = tag;
	if(tag.charAt(0)=='#')
		server_tag = tag.substring(1, tag.length);
	
	var url = "http://www.cardsbond.com/BondSys/web/api/v1/events/delete_tag?id="+id+"&othercontact_id="+conId+"&tag="+server_tag+"&event_id="+event_id;
	console.log("delete tag server url"+url);
	var ajaxRequest;
		try{
			ajaxRequest = new XMLHttpRequest();
		} catch (e){
			try{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
					//alert("Microsoft");
				} catch (e){
					alert("Your browser broke!");
				return false;
				}
			}
		}
		ajaxRequest.onreadystatechange = function(){
			if(ajaxRequest.readyState == 4){
				var response=ajaxRequest.responseText;
				if(response.search('error') == -1){
					console.log("delete tag "+response);
					var results = JSON.parse(response);
					var param = conId+"|>"+tag;
					deleteFromLocalUpdates("removeNewContactTagFromServer", param);
					
					
					}
			}
		}
		ajaxRequest.open("GET", url, true);		
		ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		//ajaxRequest.setRequestHeader("Content-length", param.length);
		ajaxRequest.setRequestHeader("Connection", "close");
		ajaxRequest.send();	
}