function getContactDataFromServer(bondId, is_notification){
	console.log("in getContactDataFromServer"+is_notification);
	var url ="http://www.cardsbond.com/BondSys/web/api/v1/contacts?id="+bondId;
	var ajaxRequest;
	try{
			ajaxRequest = new XMLHttpRequest();
		} catch (e){
			try{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
					//alert("Microsoft");
				} catch (e){
					alert("Your browser broke!");
				return false;
				}
			}
		}
		
	ajaxRequest.onreadystatechange = function(){
		if(ajaxRequest.readyState == 4 && ajaxRequest.status==200){
			var response=ajaxRequest.responseText;
			console.log("mycontactsResponse: "+response);
			if(response!="" && response!="null"){
				if(response.search('error')==-1){
					var results = JSON.parse(response);
					var today=new Date();
					var h=today.getHours();
					var m=today.getMinutes();
					var s=today.getSeconds();
					m = checkTime(m);
					s = checkTime(s);	
					
					var dd = today.getDate();
					var mm = today.getMonth()+1; //January is 0!
					var yyyy = today.getFullYear();

					if(dd<10) {
				    	dd='0'+dd;
				    }	 

					if(mm<10) {
				    	mm='0'+mm;
					} 

					today = dd+'-'+mm+'-'+yyyy;
					var time = h+":"+m+":"+s;
					var fullDate = today+' '+time;
					var mycomment="", commentId="", commentIdForCompany="", contact_name="";
					contact_name = results.result[0].fname+" "+results.result[0].lname;
					var insertcontact= 'INSERT INTO CONTACTS(contactid , fname, lname, username, password, companyid, company, positionid, position, imgsource, timezone, address, issocial, behidden, countryid, cityid, aboutme, ispublic, wants, qrimage, country_name) VALUES (?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?)';
					var insertcomment= 'INSERT INTO CONTACTSCONNECTIONCOMMENT (commentid, contactid,  othercontactid, comment, time) VALUES (?, ?, ?, ?, ?)';	    
					var allComments = 'SELECT max(commentid) AS topId FROM CONTACTSCONNECTIONCOMMENT';
							    	
					var insertcomment_company= 'INSERT INTO CONTACTSCONNECTIONCOMMENTFORCOMPANY (commentid, contactid,  othercontactid, comment, time) VALUES (?, ?, ?, ?, ?)';	    
					var allComments_company = 'SELECT max(commentid) AS topId FROM CONTACTSCONNECTIONCOMMENTFORCOMPANY';
					console.log("image:"+results.result[0].image);
					db.transaction(function(tx){		
					var image="css/imgs/contact-v.png", qr_image="";
					if(results.result[0].image!="" && results.result[0].image!="null"){
						image= "http://www.cardsbond.com/"+results.result[0].image;
					var URL = image;
					var my_File_Name = "img"+results.result[0].bond_contact_id;
				    var download_link = encodeURI(URL);
					var ext = download_link.substr(download_link.lastIndexOf('.') + 1); //Get extension of URL
					var target = Folder_Name + "/" + my_File_Name + "." + ext; // fullpath and name of the file which we want to give			
					filetransfer(download_link, target);
					image= target;								
				}
				else
					image="css/imgs/contact-v.png";		
				var timezone = parseInt(results.result[0].time_zone);
				tx.executeSql(insertcontact,[
					results.result[0].bond_contact_id,
					results.result[0].fname,
					results.result[0].lname,
					results.result[0].username,
					"",
					results.result[0].bond_company_id,
					results.result[0].company_name, 
					results.result[0].bond_position_id, 
					results.result[0].position_name, 
					image, timezone, 
					results.result[0].address,
					results.result[0].be_social,
					results.result[0].hide_profile,
					results.result[0].bond_country_id,
					results.result[0].bond_city_id,
					results.result[0].about,
					results.result[0].is_public,
					results.result[0].wants,
					"", results.result[0].country_name]);
				
				fillInMyNewContactCompany(results.result[0].bond_company_id);
				
				tx.executeSql(allComments,[],function(ax,res){
			    	var totalComments = res.rows.item(0).topId;
			    	commentId = totalComments+1;
				},errorCB);
		    
				tx.executeSql(allComments_company,[],function(ax,res){		    	
					commentIdForCompany = (res.rows.item(0).topId)+1;
				},errorCB);
		    
            	mycomment ="You added "+contact_name+" when she/he was "+results.result[0].position_name+" @ "+results.result[0].company_name +
				" and you were "+position+" @ "+company;
				
				tx.executeSql(insertcomment,[commentId, id, results.result[0].bond_contact_id, mycomment, fullDate],errorCB);
            	tx.executeSql(insertcomment_company,[commentIdForCompany, id, results.result[0].bond_contact_id, mycomment, fullDate],errorCB);
	
	        	if(is_notification=="notification"){		
	        		console.log("will call addToNotification from addContact");
	        		var notifyId = id+"_"+results.result[0].bond_contact_id;
	        		addToNotification(results.result[0].bond_contact_id, notifyId , contact_name, "You are now connected");
	    	        window.setTimeout(function(){
	    	        	javascript:menu('9');
	    	        	fun1(results.result[0].bond_contact_id);
	    	        }, 5000);	        			
	        	}
	        	/*else if(is_notification=="invitation"){
	        		console.log("will call addToInvitation from addContact");
   					addToInvitation(contact_name, results.result[0].bond_contact_id, "");	        		
	        	}*/								
								
					}, errorCB, successDB);   				        		
				}
			}

		}
	}			
	ajaxRequest.open("GET", url, true);
	ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	//ajaxRequest.setRequestHeader("Content-length", param.length);
	ajaxRequest.setRequestHeader("Connection", "close");
	ajaxRequest.send();
}
/***********************************************************************************/
function getContactEmailsFromWebService(bondId){
console.log("getEmailsFromWebService");
var url = "http://www.cardsbond.com/BondSys/web/api/v1/events/display_emails_to_contact?id="+bondId+"&other_id="+id;
var ajaxRequest;
		try{
		
			ajaxRequest = new XMLHttpRequest();
		} catch (e){
			try{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
					//alert("Microsoft");
				} catch (e){
					alert("Your browser broke!");
				return false;
				}
			}
		}
		ajaxRequest.onreadystatechange = function(){
			if(ajaxRequest.readyState == 4 && ajaxRequest.status==200){
				var response=ajaxRequest.responseText;
			
				getContactMobilesFromWebService(bondId);
				if(response.search('error') == -1){
					var results = JSON.parse(response);
					var email, emailid, bId, typeId;
						for(var i=0; i<results.result.length;i++){
						emailid = results.result[i].bond_contact_email_id;						
						email =  results.result[i].email;
						bId= results.result[i].bond_contact_id;
						typeId = results.result[i].contact_levels;

						var delEmail= 'DELETE FROM EMAILS WHERE contactid=(?)';

						db.transaction(function(tx){
						tx.executeSql(delEmail,[bondId],function(tx,res){
							insertEmails(emailid, email, bId, typeId);
						},errorCB);		
					}, errorCB, successDB);

						}	        
				}

			}
		}
		ajaxRequest.open("GET", url, true);		
		ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		//ajaxRequest.setRequestHeader("Content-length", param.length);
		ajaxRequest.setRequestHeader("Connection", "close");
		ajaxRequest.send();	

}
/**************************************************************/	
function insertEmails(emailid, email, contactid, typeId)
{
		var insertEmail= 'INSERT INTO EMAILS(emailid, contactid, emailvalue, typeid) VALUES (?, ?, ?, ?)';
		var selectEmail= 'SELECT emailid FROM EMAILS WHERE contactid=(?)';

		var insertEmailType= 'INSERT INTO EMAILTYPE(emailid, typeid) VALUES (?, ?)';
		
			db.transaction(function(tx){
				tx.executeSql(selectEmail,[contactid],function(tx,res){
					if(res.rows.length==0){
						tx.executeSql(insertEmail,[emailid, contactid, email, typeId]);
					}
				},errorCB);		
			}, errorCB, successDB);   												
}
/**************************************************************/
function getContactMobilesFromWebService(bondId){
	var url = "http://www.cardsbond.com/BondSys/web/api/v1/events/display_mobiles_to_contact?id="+bondId+"&other_id="+id;
//alert("mobile"+url);
var ajaxRequest;
		try{
			ajaxRequest = new XMLHttpRequest();
		} catch (e){
			try{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
					//alert("Microsoft");
				} catch (e){
					alert("Your browser broke!");
				return false;
				}
			}
		}
		ajaxRequest.onreadystatechange = function(){
		//alert(ajaxRequest.readyState);
			if(ajaxRequest.readyState == 4 && ajaxRequest.status==200){
				var response=ajaxRequest.responseText;
				getContactPhonesFromWebService(bondId);
				//alert("res mobile"+response);						
				if(response.search('error') == -1){
					var results = JSON.parse(response);
						var phoneid, phone, bId, typeId;
						for(var i=0; i<results.result.length;i++){
							phoneid= results.result[i].bond_contact_mobile_id;
							phone= results.result[i].mobile;
							bId= results.result[i].bond_contact_id;
							typeId = results.result[i].contact_levels;
							insertMobiles(phoneid, phone, bId, typeId);
						}
		
				}
			}
			}
		ajaxRequest.open("GET", url, true);		
		ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		//ajaxRequest.setRequestHeader("Content-length", param.length);
		ajaxRequest.setRequestHeader("Connection", "close");
		ajaxRequest.send();	

}	
/******************************************************************/
function insertMobiles(mobileid, mobile, contactid, typeId)
{
	var insertMobile= 'INSERT INTO MOBILES(mobileid, contactid, mobilevalue, typeid) VALUES (?, ?, ?, ?)';

	var selectMobile= 'SELECT mobileid FROM MOBILES WHERE mobileid=(?)';
	db.transaction(function(tx){
		tx.executeSql(selectMobile,[mobileid],function(tx,res){
		if(res.rows.length==0){
			tx.executeSql(insertMobile,[mobileid, contactid, mobile, typeId]);

	//		alert("mobile: "+mobile);		
		}
		
		},errorCB);		
		
	}, errorCB, successDB);   												
}
/***************************************/
function getContactPhonesFromWebService(bondId){
	var url = "http://www.cardsbond.com/BondSys/web/api/v1/events/display_phones_to_contact?id="+bondId+"&other_id="+id;
//alert("phone"+url);
var ajaxRequest;
		try{
			ajaxRequest = new XMLHttpRequest();
		} catch (e){
			try{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
					//alert("Microsoft");
				} catch (e){
					alert("Your browser broke!");
				return false;
				}
			}
		}
		ajaxRequest.onreadystatechange = function(){
		//alert(ajaxRequest.readyState);
			if(ajaxRequest.readyState == 4 && ajaxRequest.status==200){
				var response=ajaxRequest.responseText;
				getContactFaxesFromWebService(bondId);
				//alert("res phone"+response);						
				if(response.search('error') == -1){
					var results = JSON.parse(response);
						var phoneid, phone, bId, typeId;
						for(var i=0; i<results.result.length;i++){
							phoneid= results.result[i].bond_contact_phone_id;
							phone= results.result[i].phone;
							bId= results.result[i].bond_contact_id;
							typeId = results.result[i].contact_levels;
							insertPhones(phoneid, phone, bId, typeId);
						}
		
				}
			}
			}
		ajaxRequest.open("GET", url, true);		
		ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		//ajaxRequest.setRequestHeader("Content-length", param.length);
		ajaxRequest.setRequestHeader("Connection", "close");
		ajaxRequest.send();	

}	
/******************************************************************/
function insertPhones(phoneid, phone, contactid, typeId)
{
	var insertPhones= 'INSERT INTO PHONES(phoneid, contactid, phonevalue, typeid) VALUES (?, ?, ?, ?)';
	var selectPhones= 'SELECT phoneid FROM PHONES WHERE phoneid=(?)';
	
	db.transaction(function(tx){
		tx.executeSql(selectPhones,[phoneid],function(tx,res){
		if(res.rows.length==0){
			tx.executeSql(insertPhones,[phoneid, contactid, phone, typeId]);
		}		
		},errorCB);		
		
	}, errorCB, successDB);   											
	
}
/***************************************/
function getContactFaxesFromWebService(bondId){
	var url = "http://www.cardsbond.com/BondSys/web/api/v1/events/display_faxes_to_contact?id="+bondId+"&other_id="+id;
//alert("fax"+url);
var ajaxRequest;
		try{
			ajaxRequest = new XMLHttpRequest();
		} catch (e){
			try{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
					//alert("Microsoft");
				} catch (e){
					alert("Your browser broke!");
				return false;
				}
			}
		}
		ajaxRequest.onreadystatechange = function(){
		//alert(ajaxRequest.readyState);
			if(ajaxRequest.readyState == 4 && ajaxRequest.status==200){
				var response=ajaxRequest.responseText;
					attachContactSocialFromWebService(bondId);			
				if(response.search('error') == -1){
					var results = JSON.parse(response);
						var phoneid, phone, bId, typeId;
						for(var i=0; i<results.result.length;i++){
							phoneid= results.result[i].bond_contact_fax_id;
							phone= results.result[i].fax;
							bId= results.result[i].bond_contact_id;
							typeId = results.result[i].contact_levels;
							insertFaxes(phoneid, phone, bId, typeId);
						}
		
				}
			}
			}
		
		ajaxRequest.open("GET", url, true);		
		ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		//ajaxRequest.setRequestHeader("Content-length", param.length);
		ajaxRequest.setRequestHeader("Connection", "close");
		ajaxRequest.send();	

}	
/******************************************************************/
function insertFaxes(faxid, fax, contactid, typeId)
{
console.log("fax inserted");
	var insertFaxes= 'INSERT INTO FAXES(faxid, contactid, faxvalue, typeid) VALUES (?, ?, ?, ?)';

	var selectFaxes= 'SELECT faxid FROM FAXES WHERE faxid=(?)';
	db.transaction(function(tx){
		tx.executeSql(selectFaxes,[faxid],function(tx,res){
		if(res.rows.length==0){
			tx.executeSql(insertFaxes,[faxid, contactid, fax, typeId]);
	//		tx.executeSql(insertFaxesType,[faxid, typeId]);

			//alert("phones"+phone);		
		}
		
		},errorCB);		
	}, errorCB, successDB);   											
}
/******************************************************************/
function attachContactSocialFromWebService(bondId)
{
console.log("attachSocialFromWebService");
var ajaxRequest;
	var url = "http://www.cardsbond.com/BondSys/web/api/v1/events/display_socialnetworks_to_contact?id="+bondId+"&other_id="+id;
//alert(url);
		try{
			ajaxRequest = new XMLHttpRequest();
		} catch (e){
			try{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
					//alert("Microsoft");
				} catch (e){
					alert("Your browser broke!");
					return false;
				}
			}
		}	
		ajaxRequest.onreadystatechange = function(){
		if(ajaxRequest.readyState == 4){		
			var response=ajaxRequest.responseText;			
				
				if(response.search('error')==-1){
					var results = JSON.parse(response);
					var bond_contact_socialnetwork_id, bond_socialnetwork_id, socialnetwork_type,socialnetwork,url, fullPath, conId, contact_levels;
					for(var i=0; i<results.result.length;i++){				
							bond_contact_socialnetwork_id = results.result[i].bond_contact_socialnetwork_id;
				            //bond_socialnetwork_id         = results.result[i].bond_socialnetwork_id;
				            socialnetwork_type            = results.result[i].socialnetwork_type;      // "Facebook"
				            socialnetwork                 = results.result[i].socialnetwork;  //   face/dinsalah 
				            url = results.result[i].url;      //www.facebook.com
				            //fullPath= url+"/"+socialnetwork;
				            fullPath= socialnetwork;
				            conId = results.result[i].bond_contact_id;
				            contact_levels = results.result[i].contact_levels;
				            insertSocialNetwork(bond_contact_socialnetwork_id, conId, socialnetwork_type, fullPath,  contact_levels);
					}			
				}
			}
		}
		ajaxRequest.open("GET", url, true);		
		ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		//ajaxRequest.setRequestHeader("Content-length", param.length);
		ajaxRequest.setRequestHeader("Connection", "close");
		ajaxRequest.send();
}
/********************************************/
function insertSocialNetwork(bond_contact_socialnetwork_id, contactId, socialnetwork_type, fullPath,  contact_levels)
{
    	var insertSocial = 'INSERT INTO CONTACTSOCIALNETWORKS (socialid, contactid, socialname, url, typeid) VALUES (?, ?, ?, ?, ?)';
    	var selectSocial = 'SELECT * FROM CONTACTSOCIALNETWORKS WHERE socialid=(?)';
		db.transaction(function(tx){
	   		tx.executeSql(selectSocial,[bond_contact_socialnetwork_id],function(tx,results){
	   		if(results.rows.length==0){
		   		tx.executeSql(insertSocial,[bond_contact_socialnetwork_id, contactId, socialnetwork_type, fullPath, contact_levels]);
	   		}
	   		},errorCB);	   		
    	}, errorCB, successDB);
    }	
					
		
	/************************************************************************/
function insertUserData(bondId)
{
	var url = "http://www.cardsbond.com/BondSys/web/api/v1/contacts?id="+bondId;
	console.log("insertUserData"+url);

	var ajaxRequest;
	try{
		ajaxRequest = new XMLHttpRequest();
	} catch (e){
		try{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
					//alert("Microsoft");
				} catch (e){
					alert("Your browser broke!");
				return false;
				}
			}
		}
		ajaxRequest.onreadystatechange = function(){				
			if(ajaxRequest.readyState == 4){
				var response=ajaxRequest.responseText;
				console.log("userdata response: "+response);
				if(response.search('error') == -1){
				var results = JSON.parse(response);
												
		var insertcontact= 'INSERT INTO CONTACTS (contactid , fname, lname, username, password, companyid, company, positionid, position, imgsource, timezone, address, issocial, behidden, countryid, cityid, aboutme, ispublic, wants, qrimage, city_name, country_name) VALUES (?,?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?)';
		
		var selectContact= 'SELECT contactid FROM CONTACTS WHERE contactid=(?)';
		db.transaction(function(tx){
		tx.executeSql(selectContact,[bondId],function(tx,res){
//		alert("contacts len b4 insert: "+res.rows.length);
		if(res.rows.length==0){
		var image="";
		if(results.result[0].image!="" && results.result[0].image!="null"){
			image= "http://www.cardsbond.com/"+results.result[0].image;
			var URL = image;
			File_Name = "profile_"+id;
		    var download_link = encodeURI(URL);
		    //alert("download_link: "+download_link);
		    var ext = download_link.substr(download_link.lastIndexOf('.') + 1); //Get extension of URL
		    var target = Folder_Name + "/" + File_Name + "." + ext; // fullpath and name of the file which we want to give			
		    filetransfer(download_link, target);
		    image=target;								
		}
		else
			image="css/imgs/contact-v.png";
		
		var timezone = parseInt(results.result[0].time_zone);
		tx.executeSql(insertcontact,[
		results.result[0].bond_contact_id,
		results.result[0].fname,
		results.result[0].lname,
		results.result[0].username,
		"",
		results.result[0].bond_company_id,
		results.result[0].company_name, 
		results.result[0].bond_position_id, 
		results.result[0].position_name, 
		image, timezone, 
		results.result[0].address,
		results.result[0].be_social,
	 	results.result[0].hide_profile,
		results.result[0].bond_country_id,
		results.result[0].bond_city_id,
		results.result[0].about,
		results.result[0].is_public,
		results.result[0].wants,
		"",
		results.result[0].city_name,
		results.result[0].country_name
		]);
		
		}
		},errorCB);		
	        }, errorCB, successDB);   											
				}
				}
				}
		ajaxRequest.open("GET", url, true);		
		ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		//ajaxRequest.setRequestHeader("Content-length", param.length);
		ajaxRequest.setRequestHeader("Connection", "close");
		ajaxRequest.send();		
	}
