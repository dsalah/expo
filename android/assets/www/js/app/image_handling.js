function getPhoto(source) {
	if(saveUpdatedProfiledbclick%2==0){
	navigator.camera.getPicture(onPhotoURISuccess, onFail, { //quality: 25,
    	targetWidth: 135,
  		targetHeight: 100,
  		//correctOrientation: true,
		//allowEdit: true,
        destinationType: destinationType.FILE_URI,
    sourceType: source });
    }
    saveUpdatedProfiledbclick++;
}
/********************************************/
function onPhotoURISuccess(imageURI) {
	alert(document.getElementById('edit_profile_picture').src);
    document.getElementById('edit_profile_picture').src= '';
    document.getElementById('edit_profile_picture').src= imageURI;

	var download_link = encodeURI(imageURI);
	var date = new Date();
	var dateMillis = date.getTime();
	
	var fname= dateMillis+id;
	var ext_ = download_link.substr(download_link.lastIndexOf('.')+1);
   	var ext = ext_.substr(0,3); //Get extension of URL
   	var target =  Folder_Name + "/" + fname + "." + ext; // fullpath and name of the file which we want to give			
   	console.log("target"+ target);
   	filetransfer(download_link, target);			    	

	var profileImage = document.getElementById('edit_profile_picture');
    myProfImage = target;    
    profileImage.src= imageURI;
    document.getElementById('edit_profile_picture').src= imageURI;
   	console.log("myProfImage"+ myProfImage);

	var photo_update= 'UPDATE CONTACTS set imgsource=(?) WHERE contactid=(?)';
	db.transaction(function(tx){
	   	tx.executeSql(photo_update,[myProfImage, id]);
	   		},errorCB, successDB);
	networkState = checkConnection();
	if(networkState!="none" && networkState!="unknown"){			

    uploadPhoto(target);
	}
	else
	{
		
	    var updates= 'INSERT INTO UPDATESFUNCTIONS(updateid, name, parameters, datetime) VALUES (?,?,?, ?)';
	    var sql_max_updated = "SELECT MAX(updateid) as updateid FROM UPDATESFUNCTIONS";	    
		db.transaction(function(tx){
			var updatedId = 0;  
	    	tx.executeSql(sql_max_updated,[],function(tx, results){
			var len = results.rows.length;
			if(len>0)
				updatedId = results.rows.item(0).updateid;
				updatedId= updatedId+1;			    		
				var arr1 = target;			    		
			    		console.log("updated function id "+updatedId);
			    		tx.executeSql(updates,[updatedId, "uploadPhoto", arr1, setLastUpdatesTime()],errorCB);
			    		
			    	},errorCB);
				    }, errorCB, successDB);   			
				    imageExt=".jpg";
	}
}
/********************************************/
function uploadPhoto(imageURI) 
{   
	console.log("personal uri: "+imageURI);
	networkState = checkConnection();
    if(networkState!="none" && networkState!="unknown"){
		var options = new FileUploadOptions();
	    options.fileKey="file";
	    var modifiedImage = imageURI.substr(imageURI.lastIndexOf('/')+1);
	    
	    options.fileName="profile_"+id;
	    options.mimeType="image/jpeg";
	    options.chunkedMode = false;
	    var ft = new FileTransfer();
	    ft.upload(imageURI, encodeURI("http://www.cardsbond.com/m/upload.php"), win, fail, options);
	    imageExt=".jpg";
	
		var param = imageURI;
		deleteFromLocalUpdates("uploadPhoto", param);

	}
} 
/************************************************************************************/
function getCompanyPhoto(source) {
	if(saveUpdatedCompanydbclick%2==0){
      navigator.camera.getPicture(onCompanyPhotoURISuccess, onFail, { //quality: 25,
      		targetWidth: 135,
  			targetHeight: 100,
  		    //correctOrientation: true,
		   // allowEdit: true,
		    
        destinationType: destinationType.FILE_URI,
        sourceType: source });
        }
        saveUpdatedCompanydbclick++;
}
/********************************************/
function onCompanyPhotoURISuccess(companyImageURI) {     
    var download_link = encodeURI(companyImageURI);
	var date = new Date();
	var dateMillis = date.getTime();

	var fname=dateMillis+mycompanyid+id;
	var ext_ = download_link.substr(download_link.lastIndexOf('.')+1);
	console.log("ext_"+ ext_);		    	
	var ext = ext_.substr(0,3); //Get extension of URL
	console.log("ext"+ ext);
	var target = Folder_Name + "/" + fname + "." + ext; // fullpath and name of the file which we want to give			
	filetransfer(download_link, target);			
	
	var companyImage = document.getElementById('edit_company_profile_picture');
    companyImage.src= "file:/"+target;
    document.getElementById('edit_company_profile_picture').src = companyImageURI;
	    	
     mycompanylogo = target;
     console.log("mycompanyid:"+mycompanyid);
	var photo_update= 'UPDATE COMPANIES set imgsource=(?) WHERE companyid=(?)';
	var db = window.openDatabase("Database", "1.0", "Demo", 200000);
	db.transaction(function(tx){
	   	tx.executeSql(photo_update,[mycompanylogo, mycompanyid]);
	   		},errorCB, successDB);
	   	
     
    uploadCompanyPhoto(target);
}
/********************************************/
function uploadCompanyPhoto(company_imageURI) 
{   
	console.log("company uri: "+company_imageURI);
	networkState = checkConnection();
    if(networkState!="none" && networkState!="unknown"){
	var options = new FileUploadOptions();
    options.fileKey="file";
    options.fileName="company_"+mycompanyid;
    options.mimeType="image/jpeg";
    options.chunkedMode = false;
    var ft = new FileTransfer();
    ft.upload(company_imageURI, encodeURI("http://www.cardsbond.com/m/upload_company.php"), company_win, fail, options);
	}
	else
	{
	    var updates= 'INSERT INTO UPDATESFUNCTIONS(updateid, name, parameters, datetime) VALUES (?,?,?, ?)';
	    var sql_max_updated = "SELECT MAX(updateid) as updateid FROM UPDATESFUNCTIONS";	    
		db.transaction(function(tx){
			var updatedId = 0;  
	    	tx.executeSql(sql_max_updated,[],function(tx, results){
			var len = results.rows.length;
			if(len>0)
				updatedId = results.rows.item(0).updateid;
				updatedId= updatedId+1;			    		
				var arr1 = company_imageURI+"|>"+updatedId;			    		
			    	console.log("updated function id "+updatedId);
			    	tx.executeSql(updates,[updatedId, "uploadCompanyPhoto", arr1, setLastUpdatesTime()],errorCB);
			    		
			    	},errorCB);
				    }, errorCB, successDB);   			
	
					
	}
} 


function company_win(r) 
{ 
    console.log("Sent = " + r.bytesSent); 
    console.log(r.responseCode);

	companyImageExt = r.response;	
	
}

function win(r) 
{ 
//    alert("Sent = " + r.bytesSent); 
//    alert(r.responseCode);
	imageExt = r.response;	
}
/****************************/
function fail(error) 
{ 
    switch (error.code) 
    {  
     case FileTransferError.FILE_NOT_FOUND_ERR: 
    //  alert("Photo file not found"); 
      break; 
     case FileTransferError.INVALID_URL_ERR: 
     // alert("Bad Photo URL"); 
      break; 
     case FileTransferError.CONNECTION_ERR: 
      //alert("Connection error"); 
      break; 
    } 
    //alert("An error has occurred: Code = " + error.code);     
}