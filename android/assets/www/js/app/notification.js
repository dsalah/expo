function openNotificationScreen()
{
	selectNotifications();
	selectInvitations();
}
/******************************************************************************/
function showNotification(){
	showInvitationList();
	selectNotifications();
	networkState = checkConnection();
   	if(networkState!="none" && networkState!="unknown"){

 	var url = "http://www.cardsbond.com/BondSys/web/api/v1/contacts/get_update_notifications?bond_contact_id="+id;
	var ajaxRequest;
		try{
			ajaxRequest = new XMLHttpRequest();
		} catch (e){
			try{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
					//alert("Microsoft");
				} catch (e){
					alert("Your browser broke!");
				return false;
				}
			}
		}

		ajaxRequest.onreadystatechange = function(){
			if(ajaxRequest.readyState == 4){
					var response=ajaxRequest.responseText;
					console.log("response notification"+response);
					if(response.search('error')== -1){
					var results = JSON.parse(response);
						for(var i=0; i<results.length;i++){
							var notify_id =  results[i].bond_updatenotification_id;
							var name = results[i].name;
							var description = results[i].description;
							var updater_contact_id = results[i].updater_contact_id;
							//alert("from server"+notify_id);
							insertNotifications(updater_contact_id, notify_id, name, description);
		            	}
	            	}

				}
			}
		ajaxRequest.open("GET", url, true);
		ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		//ajaxRequest.setRequestHeader("Content-length", param.length);
		ajaxRequest.setRequestHeader("Connection", "close");
		ajaxRequest.send();
}

/*	else
		{
			$.msg({ content : 'Please Check your Internet Connection' });
		}*/
 }
/******************************************************************************/
function insertNotifications(updater_contact_id, notify_id, name, description)
{
	var insertNotification = 'INSERT INTO NOTIFICATIONS (contactid, bond_updatenotification_id, name, description) VALUES (?, ?, ?, ?)';
    var selectNotification = 'SELECT bond_updatenotification_id FROM NOTIFICATIONS WHERE contactid=(?) AND bond_updatenotification_id=(?)';

	db.transaction(function(tx){
		tx.executeSql(selectNotification,[updater_contact_id, notify_id],function(tx,results){
			console.log("insert notify "+results.rows.length);
			if(results.rows.length==0){
		   		tx.executeSql(insertNotification,[updater_contact_id, notify_id, name, description]);
		   		displayNotificationPerContact(updater_contact_id, notify_id, name, description);
		   		//alert(notify_id + "inserted");
		   	}
		},errorCB);
    }, errorCB, successDB);
}
/****************************************************************************/
function selectNotifications()
{
    var selectNotification = 'SELECT * FROM NOTIFICATIONS';
	db.transaction(function(tx){
		tx.executeSql(selectNotification,[],function(tx,results){
			var len = results.rows.length;
			for(var i=0; i<len; i++)
			{
				displayNotificationPerContact(results.rows.item(i).updater_contact_id, results.rows.item(i).bond_updatenotification_id, results.rows.item(i).name, results.rows.item(i).description);
			}

		},errorCB);
    }, errorCB, successDB);

}
/********************************************************************************/
function displayNotificationPerContact(updater_contact_id, notify_id, name, notificationValue)
{
	console.log("notify_id to show"+notify_id);
	console.log("updater_contact_id to show"+updater_contact_id);
notify_id=""+notify_id+"";
 var div = document.createElement('div');
 div.setAttribute('class','box notify notify-user');
 div.setAttribute('id','notification'+notify_id);
  var i = document.createElement('i');
 i.setAttribute('class','fa fa-times fa-lg');
 i.setAttribute("onClick","deleteNotification(\'"+notify_id+"\'); ");

 var img = document.createElement('img');
 img.setAttribute('src','css/imgs/icons/contact.png');
 img.setAttribute('height', '25');

 var span = document.createElement('span');
 var br = document.createElement('br');

 var text = document.createTextNode(name);
 span.appendChild(text);

 var p = document.createElement('p');
 text = document.createTextNode(notificationValue);
 p.setAttribute("style","font-size:15px;");

 p.appendChild(text);
 div.appendChild(i);

 div.appendChild(img);
 div.appendChild(span);
 div.appendChild(p);

 div.appendChild(br);
 
document.getElementById("notifyDiv").appendChild(div);
var invitation= 'SELECT * FROM CONTACTS WHERE contactid=(?)';
db.transaction(function(tx){
	tx.executeSql(invitation,[updater_contact_id],function(tx,results){
		var len = results.rows.length;
		console.log("len of notifiaction"+len);
		if(len>0){
				var company=results.rows.item(0).company;
				var position=results.rows.item(0).position;
				var image = results.rows.item(0).imgsource;
				console.log("will call showNotifyContactCard"+notify_id);
				showNotifyContactCard(name, notify_id, position, company, image, updater_contact_id);
		}
		
	},errorCB);
    }, errorCB, successDB);
}
/*******************/
function showNotifyContactCard(displayName, notify_id, position, company, image, bondId)
{
	notify_id=""+notify_id+"";
	console.log(" call showNotifyContactCard"+notify_id);

	var div0= document.createElement("div");
	div0.setAttribute('class', 'member white');			
	div0.setAttribute('id', 'member white'+bondId);			
	var div1 = document.createElement("div");
	div1.setAttribute('class', 'left');
	var a1 = document.createElement("a");
	a1.setAttribute('href', "#");
	
	var image1 = document.createElement("img");
	image1.setAttribute('class', 'photo-small');
	image1.setAttribute('src', image);
	a1.appendChild(image1);
	div1.appendChild(a1);
	
	var div2 = document.createElement("div");
	var phoneIconId = "mobile"+bondId;
	var a2 = document.createElement("a");
	a2.setAttribute('href', '#');
	a2.setAttribute('id',phoneIconId);
	
		
	div2.appendChild(a2);
	var name_id="bname"+bondId;
	
	
	var strong1 = document.createElement("strong");
	strong1.setAttribute('id', name_id);
	
	div2.appendChild(strong1);
	
	var position_id="contactposition"+bondId;			
	var p1 = document.createElement("p");
	p1.setAttribute('id', position_id);
				
	var company_id="contactcompany"+bondId;			
	var p2 = document.createElement("p");
	p2.setAttribute('id', company_id);
	
	var addIconId = "addIconToBond"+bondId;
	var addIcon = document.createElement("a");
	addIcon.setAttribute('href', '#');
	addIcon.setAttribute('id',addIconId);				
	
	div2.appendChild(p1);
	div2.appendChild(addIcon);
	
	div2.appendChild(p2);
	var br = document.createElement('br');	
	div2.appendChild(br);
	

		
		
	var a11 = document.createElement("a");
	a11.setAttribute('href', '#');
	a11.setAttribute('id', 'whatsapp'+bondId);			
	
	var i11 = document.createElement("i");
	i11.setAttribute('class', 'fa fa-envelope');
	a11.appendChild(i11);

	var a3 = document.createElement("a");
	a3.setAttribute('href', '#');
	a3.setAttribute('id', 'facebook'+bondId);
		
	var a4 = document.createElement("a");
	a4.setAttribute('href', '#');
	a4.setAttribute('id', 'twitter'+bondId);			
	
	var a5 = document.createElement("a");
	a5.setAttribute('href', '#');
	a5.setAttribute('id', 'linkedin'+bondId);			
	var a6 = document.createElement("a");
	a6.setAttribute('href', '#');
	a6.setAttribute('id', 'skype'+bondId);						
	var a7 = document.createElement("a");
	a7.setAttribute('href', '#');
	a7.setAttribute('id', 'google'+bondId);						

	var time = "timezone"+bondId;
	var div4 = document.createElement("div");
	div4.setAttribute('class', 'clock');
	div4.setAttribute('id', time);
	
	var strong1_div = document.createElement("div");
	strong1_div.setAttribute('class', 'test');
/*	div2.appendChild(a3);
	div2.appendChild(a4);							
	div2.appendChild(a5);
	div2.appendChild(a6);
	div2.appendChild(a7);
*/
	//div2.appendChild(div4);
	
	var div3 = document.createElement("div");
	div3.setAttribute('class', 'clear');
	
	div0.appendChild(div1);			
	div0.appendChild(div2);			
	div0.appendChild(div3);
console.log(document.getElementById('notification'+notify_id));
	document.getElementById('notification'+notify_id).appendChild(div0);
	document.getElementById(name_id).innerHTML= displayName; 
	document.getElementById(company_id).innerHTML=company;
	document.getElementById(position_id).innerHTML=position;

}


function showInvitationList(){
selectInvitations();
 	var url = "http://www.cardsbond.com/BondSys/web/api/v1/events/get_invitation_list?id="+id;
	var ajaxRequest;
		try{
			ajaxRequest = new XMLHttpRequest();
		} catch (e){
			try{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
					//alert("Microsoft");
				} catch (e){
					alert("Your browser broke!");
				return false;
				}
			}
		}

		ajaxRequest.onreadystatechange = function(){
		//alert(ajaxRequest.readyState);
			if(ajaxRequest.readyState == 4){
					var response=ajaxRequest.responseText;
					if(response.search('error')== -1){
						console.log("response"+response);
						var results = JSON.parse(response);
						for(var i=0; i<results.result.length;i++){
							var fname = results.result[i].fname;
							var lname = results.result[i].lname;
							var otherbondid = results.result[i].bond_contact_id;
							var invitationId = results.result[i].bond_connectioninvitation_id;
							var invitedName = fname+' '+lname;
							var position = results.result[i].position;
							var company = results.result[i].company;
							
							var image="";
							if(results.result[i].image!="" && results.result[i].image!="null"){
								var File_Name = "profile_"+id;
							    var download_link = encodeURI("http://www.cardsbond.com/"+results.result[0].image);
							    //alert("download_link: "+download_link);
							    var ext = download_link.substr(download_link.lastIndexOf('.') + 1); //Get extension of URL
							    var target = Folder_Name + "/" + File_Name + "." + ext; // fullpath and name of the file which we want to give			
							    filetransfer(download_link, target);
							    image=target;								
							}
							else
								image="css/imgs/contact-v.png";
							

							
							//alert("invitedName: "+invitedName);
							
							insertInvitation(invitedName, otherbondid, invitationId, company, position, image);
		            	}
	            	}
				}
			}
		ajaxRequest.open("GET", url, true);
		ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		//ajaxRequest.setRequestHeader("Content-length", param.length);
		ajaxRequest.setRequestHeader("Connection", "close");
		ajaxRequest.send();
 }
 /************************************************************************/
function selectInvitations()
{
    var invitation= 'SELECT * FROM INVITATION WHERE contactid=(?)';
	db.transaction(function(tx){
    	tx.executeSql(invitation,[id],function(tx,results){
    		var len = results.rows.length;		        
	        for (var i=0; i<len; i++)
       		{
    				var tags=results.rows.item(i).tags;
     				var invitationId=results.rows.item(i).invitaionid;
     				var comments=results.rows.item(i).comments;
     				var displayName=results.rows.item(i).name;
     				var othercontactid=results.rows.item(i).othercontactid;
     				//alert("Select: "+invitationId);
     				console.log("in select invitston"+results.rows.item(i).company_name);
     				showInvitation(displayName, othercontactid, invitationId, results.rows.item(i).company_name, results.rows.item(i).position_name, results.rows.item(i).image);
				}
	    	},errorCB);
	        }, errorCB, successDB);
}
/************************************************************************************/
function insertInvitation(name, othercontactid, invitationid, company_name, position_name, image)
{
	console.log('invitationid'+invitationid);
	var insertNotification = 'INSERT INTO INVITATION (invitaionid, contactid, othercontactid, name, company_name, position_name, image, tags, comments) VALUES (?, ?, ?, ?, ?, ?, ?,?,?)';
    var selectNotification = 'SELECT invitaionid FROM INVITATION WHERE contactid=(?) AND othercontactid=(?) AND invitaionid=(?)';
    var selectContact = 'SELECT * FROM CONTACTS WHERE contactid=(?) ';

	db.transaction(function(tx){
		/*tx.executeSql(selectContact,[othercontactid],function(tx,res){
			if(res.rows.length==0){getContactDataFromServer(othercontactid,""); }
			},errorCB);
		*/
		tx.executeSql(selectNotification,[id, othercontactid, invitationid],function(tx,results){
			console.log("b4 insert invitation len "+results.rows.length);
			if(results.rows.length==0){
		   		tx.executeSql(insertNotification,[invitationid, id, othercontactid, name, company_name, position_name, image, "", ""]);
		   		var bell_count = document.getElementById("bellCounter").innerHTML;
				if(bell_count=="")
				 	bell_count=parseInt("0");
				bell_count = parseInt(bell_count)+1;
				document.getElementById("bellCounter").innerHTML = bell_count;
						
     			showInvitation(name, othercontactid, invitationid, company_name, position_name, image);											 
		   	}
		},errorCB);
		
		}, errorCB, successDB);

}
/************************************************************************************/
function deleteInvitation(invitationid)
{
invitationid=''+invitationid+'';
var selectNotification = 'SELECT * FROM INVITATION WHERE invitaionid=(?)';

var deleteNotification = 'DELETE FROM INVITATION WHERE invitaionid=(?)';

	db.transaction(function(tx){
		tx.executeSql(selectNotification,[invitationid],function(tx,results){
			var bondId = results.rows.item(0).othercontactid;
			tx.executeSql(deleteNotification,[invitationid],errorCB);

			hideInvitation(invitationid);
			refresh_bell();			
			javascript:menu('9');
			fun1(bondId);
		},errorCB);

/*		tx.executeSql(deleteNotification,[invitationid],function(tx,results){
			hideInvitation(invitationid);
			refresh_bell();			
			
		},errorCB);*/
    }, errorCB, successDB);

}
/***************************************************************************************/ 
function deleteIgnoreInvitation(invitationid)
{
invitationid=''+invitationid+'';
	//alert(invitationid);
	var deleteNotification = 'DELETE FROM INVITATION WHERE invitaionid=(?)';

	db.transaction(function(tx){
		tx.executeSql(deleteNotification,[invitationid],function(tx,results){
		ignoreInvitation(invitationid);
		hideInvitation(invitationid);
		refresh_bell();
		
		},errorCB);
    }, errorCB, successDB);

}
/***********************************************************************************/
function showInvitation(displayName, otherbondid, invitationId, company_name, position_name, image){
		
	
	showInvitedContactCard(displayName, otherbondid, invitationId, position_name, company_name, image);

}
function showNormalInvitation(displayName, otherbondid, invitationId){

	var div = document.createElement('div');
	div.setAttribute('class','box notify-unread notify-user');
	div.setAttribute('id','invitation'+invitationId);

	var i = document.createElement('i');
	i.setAttribute('class','fa fa-times fa-2x');
 i.setAttribute('onClick','hideInvitation('+invitationId+')');

 var img = document.createElement('img');
 img.setAttribute('src','css/imgs/icons/contact.png');
 img.setAttribute('height', '25');

 var span = document.createElement('span');
 var text = document.createTextNode(displayName+" wants to Bond With you");
 span.appendChild(text);

 var p = document.createElement('p');
 text = document.createTextNode('');
 p.appendChild(text);


 var div2 = document.createElement('div');
 div2.setAttribute('class','box-notify');

 var form = document.createElement('form');

 var familyLabel = document.createTextNode('Supplier ');
 var friendsLabel = document.createTextNode('Customer ');
 var businessLabel = document.createTextNode('Partner');


 var input1 = document.createElement('input');
 input1.setAttribute('type', 'checkbox');
 input1.setAttribute('id', 'inviteAsFamily'+otherbondid);

 var input2 = document.createElement('input');
 input2.setAttribute('type', 'checkbox');
 input2.setAttribute('id', 'inviteAsFriends'+otherbondid);

 var input3 = document.createElement('input');
 input3.setAttribute('type', 'checkbox');
 input3.setAttribute('id', 'inviteAsBusiness'+otherbondid);

 var br = document.createElement('br');

 var input4 = document.createElement('input');
 input4.setAttribute('type', 'text');
 input4.setAttribute('id', 'invitationTag'+otherbondid);
 input4.setAttribute('placeholder', 'Add Tags(Optional)');

 var tagText = document.createTextNode('Add Tags');

 var tagLabel = document.createElement('label');
 tagLabel.appendChild(tagText);


 var textarea = document.createElement('textarea');
 textarea.setAttribute('id', 'invitationComment'+otherbondid);
 textarea.setAttribute('placeholder', 'Add Comment(Optional)');

 var commentText = document.createTextNode('Add Comments');

 var commentLabel = document.createElement('label');
 commentLabel.appendChild(commentText);

 var input5 = document.createElement('input');
 input5.setAttribute('type','button');
 input5.setAttribute('class', 'Btn-acc');
 input5.setAttribute('value', 'Accept');
 input5.setAttribute("onClick", "acceptInvitation("+invitationId+","+otherbondid+")");

 var input6 = document.createElement('input');
 input6.setAttribute('type','button');
 input6.setAttribute('class', 'Btn-ign');
 input6.setAttribute('value', 'Ignore');
 input6.setAttribute('onClick', "deleteIgnoreInvitation("+invitationId+")");


 form.appendChild(input1);
 form.appendChild(familyLabel);

 form.appendChild(input2);
 form.appendChild(friendsLabel);

 form.appendChild(input3);
 form.appendChild(businessLabel);
 var br2 = document.createElement('br');
 var br3 = document.createElement('br');
 var br4 = document.createElement('br');
 var br5 = document.createElement('br');
 var br6 = document.createElement('br');
 var br7 = document.createElement('br');

 form.appendChild(br2);

form.appendChild(tagLabel);
form.appendChild(br3);
form.appendChild(input4);
form.appendChild(br4);

form.appendChild(commentLabel);
form.appendChild(br5);
form.appendChild(textarea);
form.appendChild(br6);

 form.appendChild(input5);
 form.appendChild(input6);

 div.appendChild(i);
 div.appendChild(img);
 div.appendChild(span);
div.appendChild(br);

 div.appendChild(p);
div.appendChild(br7);
	div2.appendChild(form);
	div.appendChild(div2);
	document.getElementById("notifyDiv").appendChild(div);
}
/****************************/
function showInvitedContactCard(displayName, bondId, invitationId, position, company, image)
{
	console.log("in showInvitedContactCard "+invitationId);
	console.log("in showInvitedContactCard "+position);
	console.log("in showInvitedContactCard "+image);

	
	var div0= document.createElement("div");
	div0.setAttribute('class', 'member white');			
	div0.setAttribute('id', 'member white'+bondId);		
	
	var div1 = document.createElement("div");
	div1.setAttribute('class', 'left');
	var a1 = document.createElement("a");
	a1.setAttribute('href', "#");
	
	var image1 = document.createElement("img");
	image1.setAttribute('class', 'photo-small');
	image1.setAttribute('src', image);
	a1.appendChild(image1);
	div1.appendChild(a1);
	
	var div2 = document.createElement("div");
	var phoneIconId = "mobile"+bondId;
	var a2 = document.createElement("a");
	a2.setAttribute('href', '#');
	a2.setAttribute('id',phoneIconId);
	
		
	div2.appendChild(a2);
	var name_id="bname"+bondId;
	
	
	var strong1 = document.createElement("strong");
	strong1.setAttribute('id', name_id);
	
	div2.appendChild(strong1);
	
	var position_id="contactposition"+bondId;			
	var p1 = document.createElement("p");
	p1.setAttribute('id', position_id);
				
	var company_id="contactcompany"+bondId;			
	var p2 = document.createElement("p");
	p2.setAttribute('id', company_id);
	
	var addIconId = "addIconToBond"+bondId;
	var addIcon = document.createElement("a");
	addIcon.setAttribute('href', '#');
	addIcon.setAttribute('id',addIconId);				
	
	div2.appendChild(p1);
	div2.appendChild(addIcon);
	
	div2.appendChild(p2);
	var br = document.createElement('br');	
	div2.appendChild(br);
	

	 var input5 = document.createElement('input');
	 input5.setAttribute('type','button');
	 input5.setAttribute('class', 'invite-accept-btn');
	 input5.setAttribute('style', 'width:25%; float: right; margin-right:2px;');

	 input5.setAttribute('value', 'Accept');
	 input5.setAttribute("onClick", "acceptInvitation("+invitationId+","+bondId+")");

	 var input6 = document.createElement('input');
	 input6.setAttribute('type','button');
	 input6.setAttribute('class', 'invite-reject-btn');
	 input6.setAttribute('style', 'width:25%');

	 input6.setAttribute('value', 'Ignore');
	 input6.setAttribute('onClick', "deleteIgnoreInvitation("+invitationId+")");

	
	div2.appendChild(input6);
	
		
	var a11 = document.createElement("a");
	a11.setAttribute('href', '#');
	a11.setAttribute('id', 'whatsapp'+bondId);			
	
	var i11 = document.createElement("i");
	i11.setAttribute('class', 'fa fa-envelope');
	a11.appendChild(i11);

	var a3 = document.createElement("a");
	a3.setAttribute('href', '#');
	a3.setAttribute('id', 'facebook'+bondId);
		
	var a4 = document.createElement("a");
	a4.setAttribute('href', '#');
	a4.setAttribute('id', 'twitter'+bondId);			
	
	var a5 = document.createElement("a");
	a5.setAttribute('href', '#');
	a5.setAttribute('id', 'linkedin'+bondId);			
	var a6 = document.createElement("a");
	a6.setAttribute('href', '#');
	a6.setAttribute('id', 'skype'+bondId);						
	var a7 = document.createElement("a");
	a7.setAttribute('href', '#');
	a7.setAttribute('id', 'google'+bondId);						

	var time = "timezone"+bondId;
	var div4 = document.createElement("div");
	div4.setAttribute('class', 'clock');
	div4.setAttribute('id', time);
	
	var strong1_div = document.createElement("div");
	strong1_div.setAttribute('class', 'test');
/*	div2.appendChild(a3);
	div2.appendChild(a4);							
	div2.appendChild(a5);
	div2.appendChild(a6);
	div2.appendChild(a7);
*/
	//div2.appendChild(div4);
	div2.appendChild(input5);

	var div3 = document.createElement("div");
	div3.setAttribute('class', 'clear');
	
	div0.appendChild(div1);			
	div0.appendChild(div2);			
	div0.appendChild(div3);
	if(document.getElementById('member white'+bondId)== null)
		document.getElementById("notifyDiv").appendChild(div0);
	document.getElementById(name_id).innerHTML= displayName; 
	document.getElementById(company_id).innerHTML=company;
	document.getElementById(position_id).innerHTML=position;

}
/***********************************/
function acceptInvitation(invitationId, otherbondid)
{
	console.log("invitationId"+invitationId);
	otherbondid =''+otherbondid+'';
	invitationId = ''+invitationId+'';
	var invitedfamilyId= 'inviteAsFamily'+otherbondid;
	var invitedfriendsId= 'inviteAsFriends'+otherbondid;
	var invitedbusinessId= 'inviteAsBusiness'+otherbondid;
	var invitationTag = "";//document.getElementById('invitationTag'+otherbondid).value;
	var invitationComment = "";//document.getElementById('invitationComment'+otherbondid).value;
	
	var family = false; //document.getElementById(invitedfamilyId).checked;
	var friends = false; //document.getElementById(invitedfriendsId).checked;
	var business = false; //document.getElementById(invitedbusinessId).checked;

	var familyLevel="", friendLevel="", businessLevel="";
	
	  var url = "http://cardsbond.com/BondSys/web/api/v1/events/accept_connection/?bond_connectioninvitation_id="+invitationId+"&bond_connectiontype_level_id=";

	  var contact_levels="", contact_levels_names="";
	if(family == true)
	{
	  	familyLevel = familyId;
    	contact_levels += familyLevel;
    	contact_levels_names += "supplier";

	}

    if(friends == true)
    {
    	friendLevel = friendsId;
    	contact_levels += ","+friendLevel;
    	contact_levels_names += "customer";

    }

    if(business == true)
    {
    	businessLevel = businessId;
    	contact_levels += ","+businessLevel;
    	contact_levels_names += "partner";
    }

	if(family == false && business == false && friends == false){
    	contact_levels += "4";
    	contact_levels_names += "favorite";
    	
	}

    url = url+contact_levels+"&comment="+invitationComment+"&tags="+invitationTag+"&event_id="+event_id;
    console.log("accept_conn: "+url);
	
		networkState = checkConnection();
   		if(networkState!="none" && networkState!="unknown"){
	
		var ajaxRequest;
		try{
			ajaxRequest = new XMLHttpRequest();
		} catch (e){
			try{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
					//alert("Microsoft");
				} catch (e){
					alert("Your browser broke!");
				return false;
				}
			}
		}

		ajaxRequest.onreadystatechange = function(){
			if(ajaxRequest.readyState == 4){
				var response=ajaxRequest.responseText;
				console.log("accept response"+response);
				if(response!=""){
					if(response.search('error') == -1)
					{
						var results = JSON.parse(response);
						var commentid = new Date(); 
						addQuickBondToMyContacts(results.contactconnectionid, otherbondid, contact_levels, invitationTag, invitationComment, contact_levels_names, "notification");
						deleteInvitation(invitationId);
	
					}

				}
			}
		}
		ajaxRequest.open("GET", url, true);
		ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		//ajaxRequest.setRequestHeader("Content-length", param.length);
		ajaxRequest.setRequestHeader("Connection", "close");
		ajaxRequest.send();
   		}
else
		$.msg({ content : 'You have to enable your Internet Connection' });

}
/***************************/
function ignoreInvitation(invitationid)
{
invitationid=''+invitationid+'';
//otherbondid=''+otherbondid+'';
//alert(otherbondid);
//var url = "http://cardsbond.com/BondSys/web/api/v1/contacts/reject_connection/?bond_contact_id="+id+"&other_contact_id="+otherbondid;
  var url = "http://cardsbond.com/BondSys/web/api/v1/events/reject_connection?bond_connectioninvitation_id="+invitationid;
console.log("ignore invitation "+url);
	networkState = checkConnection();
   	if(networkState!="none" && networkState!="unknown"){
		var ajaxRequest;
		try{
			ajaxRequest = new XMLHttpRequest();
		} catch (e){
			try{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
					//alert("Microsoft");
				} catch (e){
					alert("Your browser broke!");
				return false;
				}
			}
		}

		ajaxRequest.onreadystatechange = function(){
		//alert("req: "+ajaxRequest.readyState);
			if(ajaxRequest.readyState == 4){
				var response=ajaxRequest.responseText;
			//	alert(response);
				if(response.search('error') == -1)
				{
					var results = JSON.parse(response);
		//			alert(results);
					
					//deleteInvitation(invitationid);

				}

				}
				}
		ajaxRequest.open("GET", url, true);
		ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		//ajaxRequest.setRequestHeader("Content-length", param.length);
		ajaxRequest.setRequestHeader("Connection", "close");
		ajaxRequest.send();
	}
	else
		$.msg({ content : 'No Internet Connection' });

}
/***********************************************************/
function addAcceptedBond(commentId, otherbondid, family, friends, business, invitationComment,invitationTag)
{
	otherbondid=''+otherbondid+'';
	//alert(otherbondid);
	var fullDate = getCurrentDateAndTime();
	var url = "http://cardsbond.com/BondSys/web/api/v1/contacts?id="+otherbondid;
	var ajaxRequest;
	try{
		ajaxRequest = new XMLHttpRequest();
	} catch (e){
		try{
			ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try{
				ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
				//alert("Microsoft");
			} catch (e){
				alert("Your browser broke!");
				return false;
			}
		}
	}

	ajaxRequest.onreadystatechange = function(){
	//alert("req: "+ajaxRequest.readyState);
	if(ajaxRequest.readyState == 4){
		var response=ajaxRequest.responseText;
		//alert(response);
		if(response.search('error') == -1)
		{
			var results = JSON.parse(response);
			var insertcontact= 'INSERT INTO CONTACTS (contactid , fname, lname, username, password, companyid, company, positionid, position, imgsource, timezone, address, issocial, behidden, countryid, cityid, aboutme, ispublic, wants, city_name, country_name) VALUES (?,?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)';

			var insertcomment= 'INSERT INTO CONTACTSCONNECTIONCOMMENT (commentid, contactid,  othercontactid, comment, time) VALUES (?, ?, ?, ?, ?)';
			
			var inserttag= 'INSERT INTO CONTACTSCONNECTIONTAGS (contactid,  othercontactid, tags) VALUES (?, ?, ?)';
			var insertconn= 'INSERT INTO CONTACTSCONNECTION (contactid,  othercontactid, type, typevalue) VALUES (?, ?, ?, ?)';

    	var insertcomment_company= 'INSERT INTO CONTACTSCONNECTIONCOMMENTFORCOMPANY (commentid, contactid,  othercontactid, comment, time) VALUES (?, ?, ?, ?, ?)';	    
		var allComments_company = 'SELECT max(commentid) AS topId FROM CONTACTSCONNECTIONCOMMENTFORCOMPANY';
    	

			var selectContact= 'SELECT contactid FROM CONTACTS WHERE contactid=(?)';
			var db = window.openDatabase("Database", "1.0", "Demo", 200000);

			var image="css/imgs/contact-v.png";
var commentIdForCompany="";
    	
			db.transaction(function(tx){
			tx.executeSql(allComments_company,[],function(ax,res){
		    	
		    	commentIdForCompany = (res.rows.item(0).topId)+1;
		    },errorCB);
		    
				tx.executeSql(selectContact,[otherbondid],function(tx,res){
				//alert("contacts len b4 insert: "+res.rows.length);
				if(res.rows.length==0){
				if(results.result[0].image!="" && results.result[0].image!=null){

					image= "http://www.cardsbond.com/"+results.result[0].image;
					var URL = image;
					File_Name = "img"+results.result[0].bond_contact_id;
					var download_link = encodeURI(URL);
					//alert("download_link: "+download_link);
					var ext = download_link.substr(download_link.lastIndexOf('.') + 1); //Get extension of URL
					var target = Folder_Name + "/" + File_Name + "." + ext; // fullpath and name of the file which we want to give
					filetransfer(download_link, target);
					image=target;

		}
		else
			image="css/imgs/contact-v.png"


				var timezone = parseInt(results.result[0].time_zone);
					tx.executeSql(insertcontact,[
					results.result[0].bond_contact_id,
					results.result[0].fname,
					results.result[0].lname,
					results.result[0].username,
					"",
					results.result[0].bond_company_id,
					results.result[0].company_name,
					results.result[0].bond_position_id,
					results.result[0].position_name,
					image, timezone,
					results.result[0].address,
					results.result[0].be_social,
				 	results.result[0].hide_profile,
					results.result[0].bond_country_id,
					results.result[0].bond_city_id,
					results.result[0].about,
					results.result[0].is_public,
					results.result[0].wants,
					results.result[0].city_name,
					results.result[0].country_name
					]);
			        //alert("fullDate: "+fullDate);
		        if(invitationComment!="")
		        	tx.executeSql(insertcomment,[commentId, id, otherbondid, invitationComment, fullDate]);
		        
				var contact_name = results.result[0].fname+" "+results.result[0].lname;
					
            	var mycomment ="You added "+contact_name+" when she/he was "+results.result[0].position_name+" @ "+results.result[0].company_name +
				" and you were "+position+" @ "+company;

				tx.executeSql(insertcomment,[commentId, id, results.result[0].bond_contact_id, mycomment, fullDate],errorCB);
            	tx.executeSql(insertcomment_company,[commentId, id, results.result[0].bond_contact_id, mycomment, fullDate],errorCB);

			        tx.executeSql(inserttag,[id, otherbondid, invitationTag]);
			        
			        if(family == true){
						tx.executeSql(insertconn,[id, otherbondid, "supplier", "true"]);
	//					alert("family inserted");
					}
					if(friends == true){
						tx.executeSql(insertconn,[id, otherbondid, "customer", "true"]);
	//					alert("frineds inserted");

					}
					if(business == true){
						tx.executeSql(insertconn,[id, otherbondid, "partner", "true"]);
	//					alert("business inserted");

					}
			        attachSocialFromWebService(results.result[0].bond_contact_id, "", "", "");
			        fillInMyNewContactCompany(results.result[0].bond_company_id);
			        getMyCompanyContactsEmailsFromWebService(results.result[0].bond_contact_id);
					// get new contact mobiles ..... ?? Done			       
					// accept_inv API not add tags and comments  ??  not implemented	

					}
					},errorCB);
			    }, errorCB, successDB);
			}
		}
		}
		ajaxRequest.open("GET", url, true);
		ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		//ajaxRequest.setRequestHeader("Content-length", param.length);
		ajaxRequest.setRequestHeader("Connection", "close");
		ajaxRequest.send();
}
/******************************************************************************************/
function fillInMyNewContactCompany(companyId)
{
    console.log("fillInMyNewContactCompany: "+companyId);
	var url = "http://www.cardsbond.com/BondSys/web/api/v1/companies?id="+companyId;
	var ajaxRequest;
	try{
			ajaxRequest = new XMLHttpRequest();
		} catch (e){
			try{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
					//alert("Microsoft");
				} catch (e){
					alert("Your browser broke!");
				return false;
				}
			}
		}
	ajaxRequest.onreadystatechange = function(){		
		if(ajaxRequest.readyState == 4){
			var response=ajaxRequest.responseText;			
			console.log("new company response"+response);
			if(response.search('error')==-1){  	
				var results = JSON.parse(response);
				var comId = results.result[0].bond_company_id;
            	var erp = results.result[0].erp_country_id;
            	var companyName = results.result[0].name;
            	var logo = results.result[0].logo;
            	var field = "";//results.result[0].business_field;
            	var brief = results.result[0].brief;
            	var address = results.result[0].address;
				var has_account = results.result[0].has_account;
            	var timezone = results.result[0].timezone;

        		//alert(companyName);
            	if(logo!="")
            	{
					logo= "http://www.cardsbond.com/"+results.result[0].logo;
					var URL = logo;
					File_Name = "bondedCompany"+comId;
		    		var download_link = encodeURI(URL);
		    		//alert("download_link: "+download_link);
		    		var ext = download_link.substr(download_link.lastIndexOf('.') + 1); //Get extension of URL
		    		var target = Folder_Name + "/" + File_Name + "." + ext; // fullpath and name of the file which we want to give			
		    		filetransfer(download_link, target);
		    		logo=target;								
		    	//	alert("my company logo: "+logo);	    										
				}
            	else            	
            		logo = "css/imgs/icons/default-company.png";
            		
		var insertcompanysql = 'INSERT INTO COMPANIES (companyid, name, imgsource, timezone, field, info, address, has_account) VALUES (?,?,?,?,?,?,?,?)';
    	var bondedCompanies= 'INSERT INTO CONTACTSCOMPANIESCONNECTION (contactid, othercompanyid, type) VALUES (?, ?,?)';
    	var selectBonded  = 'SELECT * FROM CONTACTSCOMPANIESCONNECTION WHERE contactid=(?) AND othercompanyid=(?) AND type=(?)'
    	var selectCompany = 'SELECT * FROM COMPANIES WHERE companyid=(?)';

			db.transaction(function(tx){
	   		tx.executeSql(selectCompany,[comId],function(tx,res){
	   		if(res.rows.length==0){
	            tx.executeSql(insertcompanysql,[comId, companyName, logo, timezone , field, brief, address, has_account]);
	   		}
	   		},errorCB);
	   		
	   		tx.executeSql(selectBonded,[id, comId, "Business"],
	   		function(tx,results){
	   		if(results.rows.length==0){

		   		tx.executeSql(bondedCompanies,[id, comId, "Business"]);
	             //javascript:menu('1');
		         //	prepareDB("false");
		         console.log('company new inserted');

		   		}	   		
	   		},errorCB);
	   		

    	}, errorCB, successDB);
            	}
				}
		}
		ajaxRequest.open("GET", url, true);		
		ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		//ajaxRequest.setRequestHeader("Content-length", param.length);
		ajaxRequest.setRequestHeader("Connection", "close");
		ajaxRequest.send();	
}
/********************************************************************************************/
function deleteNotification(notifyId)
{
//alert(notifyId);
notifyId = ''+notifyId+'';
console.log(notifyId);
	var delNotify = 'DELETE FROM NOTIFICATIONS WHERE contactid=(?) AND bond_updatenotification_id=(?)';
	
    	db.transaction(function(tx){
            tx.executeSql(delNotify,[id, notifyId]);
    		}, errorCB, function(tx,results){
    		var removedChild = document.getElementById("notification"+notifyId);
    		if(removedChild!= null)
				document.getElementById("notifyDiv").removeChild(removedChild);
				//alert(document.getElementById("notifyDiv"));
				deleteNotificationFromServer(notifyId);
				refresh_bell();
    		});
}
/***********************************************************************************************/
function deleteNotificationFromServer(notifyId)
{
	var url ="http://cardsbond.com/BondSys/web/api/v1/contacts/delete_update_notifications?bond_updatenotification_id="+notifyId;
		var ajaxRequest;
		try{
			ajaxRequest = new XMLHttpRequest();
		} catch (e){
			try{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
					//alert("Microsoft");
				} catch (e){
					alert("Your browser broke!");
				return false;
				}
			}
		}

		ajaxRequest.onreadystatechange = function(){
		//alert("req: "+ajaxRequest.readyState);
			if(ajaxRequest.readyState == 4){
				var response=ajaxRequest.responseText;
				//alert(response);
				

				}
				}
		ajaxRequest.open("GET", url, true);
		ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		//ajaxRequest.setRequestHeader("Content-length", param.length);
		ajaxRequest.setRequestHeader("Connection", "close");
		ajaxRequest.send();
}
/**********************************************************************/
function hideInvitation(invitationId){
	var removedChild = document.getElementById("invitation"+invitationId);
    if(removedChild!= null)
		document.getElementById("notifyDiv").removeChild(removedChild);

}
