function showReplacement()
{
	document.getElementById('updated_replacement').type="text";
}
/***********************************************************************************************/
function saveUpdatedProfieData()
{
	if(saveUpdatedProfiledbclick%2!=0){
		savedChanges="true";
		var myImg = document.getElementById('myProfImage');
		myImg.src = myProfImage;
		//var editImage = document.getElementById('edit_profile_picture');
		//editImage.src = myProfImage;
		//	alert(myProfImage);
		var upFname = document.getElementById('updated_fname').value;
		var upLname = document.getElementById('updated_lname').value;
		var uppassword  = document.getElementById('updated_password').value;
		var upconfpassword  = document.getElementById('updated_conf_password').value;
		
		var upCompany   = document.getElementById('updated_company').value;
		//alert(upCompany);
		console.log("company id"+available_companies.indexOf(upCompany));	
		var upCompanyid="";
		if(available_companies.indexOf(upCompany) > -1)
			upCompanyid = available_companies_id[available_companies.indexOf(upCompany)];
		else
			upCompanyid	= "-1";
			
		var upPosition  = document.getElementById('updated_position').value;
		var upPositionid = "";
		if(available_positions.indexOf(upPosition) >-1)
			upPositionid = available_positions_id[available_positions.indexOf(upPosition)];
		else
			upPositionid="-1";
		var upReplacement = document.getElementById('updated_replacement').value;
		var upAddress  = document.getElementById('updated_address').value;
		var upCountry  = (document.getElementById('updated_country').selectedIndex)+1;
		var upCity  = (document.getElementById('updated_city').selectedIndex)+1;
		var city = document.getElementById('updated_city').value
		var country = document.getElementById('updated_country').value
		
		var hidden  = document.getElementById('behidden').checked;
		var social  = document.getElementById('besocial').checked;
		var upAboutme  = document.getElementById('editAboutMe').value;
		var upwants  = document.getElementById('wants').value;
		var imageOnSever = "contacts_images/"+"profile_"+id+imageExt;
		console.log("hidden"+hidden);
		console.log("social"+social	);
		if(imageExt=="")
			imageOnSever="";

		updateDB(upFname, upLname, uppassword, upCompanyid, upCompany,upPositionid, upPosition, myProfImage, upAddress,
			          upCountry, upCity, hidden, social, upAboutme, upwants, city, country);	

		networkState = checkConnection();
		if(networkState!="none" && networkState!="unknown"){			
			updateProfileInServer(upFname, upLname, uppassword, upCompanyid, upCompany,upPositionid, upPosition, imageOnSever,
					 upAddress, upCountry, upCity, hidden, social, upAboutme, upwants, upReplacement, imageExt, "");

		}
			
		updateMyCompanyProfileData();         	
	}
	saveUpdatedProfiledbclick++;
}
/***********************************************************************************************/
function updateDB(fn, ln, pass, comid, com, posid, pos, prof, address, cou, cit, hid, soc, about, wants, city, country)
{	
	var	sqltxt = 'UPDATE CONTACTS SET fname=(?) , lname=(?), password=(?), companyid=(?), company=(?), positionid=(?), position=(?), imgsource=(?), address=(?), countryid=(?), cityid=(?), behidden=(?), issocial=(?), aboutme=(?), wants=(?) WHERE contactid=(?)';											
   	db.transaction(function(tx){
		tx.executeSql(sqltxt,[fn, ln,  pass, comid, com, posid, pos, prof, address, cou, cit, hid, soc, about, wants, id]);
		mycityname = city;
		mycountryname =country;
		console.log('updated Locally');
    }, errorCB, successDB); 
}
/***********************************************************************************************/
function updateEmails(emailid, emailvalue, type)
{	
	var socialSQL="";
	var currentTime = new Date();	

	if(emailid!=""){
	    socialSQL = 'UPDATE EMAILS SET emailvalue=(?), typeid=(?) WHERE emailid=(?)';	
		db.transaction(function(tx){
			tx.executeSql(socialSQL,[emailvalue, type, emailid],errorCB);	   		
	    	}, errorCB, successDB);
	}	
	else
	{
		socialSQL = 'INSERT INTO EMAILS (emailid, contactid, emailvalue, typeid) VALUES (?, ?, ?, ?)';
		db.transaction(function(tx){
			tx.executeSql(socialSQL,[currentTime, id, emailvalue, type]);	   // "00" must be changed to the inserted id on server		
	    	}, errorCB, successDB);
	    
	}
	
	networkState = checkConnection();
    if(networkState!="none" && networkState!="unknown"){			
		updateEmailData(emailid, emailvalue, type);

    }
	else{
		    var updates= 'INSERT INTO UPDATESFUNCTIONS(updateid, name, parameters, datetime) VALUES (?,?,?,?)';
		    var sql_max_updated = "SELECT MAX(updateid) as updateid FROM UPDATESFUNCTIONS";	    
			db.transaction(function(tx){
				var updatedId = 0, arr1="";  
		    	tx.executeSql(sql_max_updated,[],function(tx, results){
		    		var len = results.rows.length;
		    		if(len>0)
		    		updatedId = results.rows.item(0).updateid;
		    		updatedId= updatedId+1;
		    		arr1 = emailid+"|>"+emailvalue+"|>"+type;
		    		console.log("arr1"+arr1);
		    		console.log("updated function id "+updatedId);
				    tx.executeSql(updates,[updatedId, "updateEmailData", arr1, setLastUpdatesTime()]);
    			},errorCB);		
		}, errorCB, successDB);   
	}

	
}
/***********************************************************************************************/
function updatePhones(phoneid, phonevalue, type)
{	
	var socialSQL="";
	var currentTime = new Date();	

	if(phoneid!=""){
		socialSQL = 'UPDATE PHONES SET phonevalue=(?), typeid=(?) WHERE phoneid=(?)';	
		db.transaction(function(tx){
			tx.executeSql(socialSQL,[phonevalue, type, phoneid],errorCB);	   		
	    	}, errorCB, successDB);
	}	
	else
	{
		socialSQL = 'INSERT INTO PHONES (phoneid, contactid, phonevalue, typeid) VALUES (?, ?, ?, ?)';
		db.transaction(function(tx){
			tx.executeSql(socialSQL,[currentTime, id, phonevalue, type]);	   // "00" must be changed to the inserted id on server		
	    	}, errorCB, successDB);
	    
	}
	networkState = checkConnection();
    if(networkState!="none" && networkState!="unknown"){			
		updatePhoneData(phoneid, phonevalue, type);

    }
	else{
		    var updates= 'INSERT INTO UPDATESFUNCTIONS(updateid, name, parameters, datetime) VALUES (?,?,?,?)';
		    var sql_max_updated = "SELECT MAX(updateid) as updateid FROM UPDATESFUNCTIONS";	    
			db.transaction(function(tx){
				var updatedId = 0, arr1="";  
		    	tx.executeSql(sql_max_updated,[],function(tx, results){
		    		var len = results.rows.length;
		    		if(len>0)
		    		updatedId = results.rows.item(0).updateid;
		    		updatedId= updatedId+1;
		    		arr1 = phoneid+"|>"+phonevalue+"|>"+type;
		    		console.log("arr1"+arr1);
		    		console.log("updated function id "+updatedId);
				    tx.executeSql(updates,[updatedId, "updatePhoneData", arr1, setLastUpdatesTime()]);
    			},errorCB);		
		}, errorCB, successDB);   
	}

}
/***********************************************************************************************/
function updateMobiles(mobileid, mobilevalue, type)
{	
	var socialSQL="";
	var currentTime = new Date();	
	console.log("mobile id local: "+mobileid);
	if(mobileid!=""){
		socialSQL = 'UPDATE MOBILES SET mobilevalue=(?), typeid=(?) WHERE mobileid=(?)';	
		db.transaction(function(tx){
			tx.executeSql(socialSQL,[mobilevalue, type, mobileid],errorCB);	   		
	    	}, errorCB, successDB);
	}	
	else
	{
		socialSQL = 'INSERT INTO MOBILES (mobileid, contactid, mobilevalue, typeid) VALUES (?, ?, ?, ?)';
		db.transaction(function(tx){
			tx.executeSql(socialSQL,[currentTime, id, mobilevalue, type]);	   // "00" must be changed to the inserted id on server		
	    	}, errorCB, successDB);
	    
	}
	
	networkState = checkConnection();
    if(networkState!="none" && networkState!="unknown"){		
		updateMobileData(mobileid, mobilevalue, type);
    }
	else{
		    var updates= 'INSERT INTO UPDATESFUNCTIONS(updateid, name, parameters, datetime) VALUES (?,?,?,?)';
		    var sql_max_updated = "SELECT MAX(updateid) as updateid FROM UPDATESFUNCTIONS";	    
			db.transaction(function(tx){
				var updatedId = 0, arr1="";  
		    	tx.executeSql(sql_max_updated,[],function(tx, results){
		    		var len = results.rows.length;
		    		if(len>0)
		    		updatedId = results.rows.item(0).updateid;
		    		updatedId= updatedId+1;
		    		arr1 = mobileid+"|>"+mobilevalue+"|>"+type;
		    		console.log("arr1"+arr1);
		    		console.log("updated function id "+updatedId);
				    tx.executeSql(updates,[updatedId, "updateMobileData", arr1, setLastUpdatesTime()]);
    			},errorCB);		
		}, errorCB, successDB);   
	}

	
}	
/***********************************************************************************************/
function updateFaxes(faxid, faxvalue, type)
{	
	var socialSQL="";
	var currentTime = new Date();	

	if(faxid!=""){
		socialSQL = 'UPDATE FAXES SET faxvalue=(?), typeid=(?) WHERE faxid=(?)';	
		db.transaction(function(tx){
			tx.executeSql(socialSQL,[faxvalue, type, faxid],errorCB);	   		
	    	}, errorCB, successDB);
	}	
	else
	{
		socialSQL = 'INSERT INTO FAXES (faxid, contactid, faxvalue, typeid) VALUES (?, ?, ?, ?)';
		db.transaction(function(tx){
			tx.executeSql(socialSQL,[currentTime, id, faxvalue, type]);	 	
	    	}, errorCB, successDB);
	    
	}
	
	networkState = checkConnection();
    if(networkState!="none" && networkState!="unknown"){			
		updateFaxData(faxid, faxvalue, type);

    }
	else{
		    var updates= 'INSERT INTO UPDATESFUNCTIONS(updateid, name, parameters, datetime) VALUES (?,?,?,?)';
		    var sql_max_updated = "SELECT MAX(updateid) as updateid FROM UPDATESFUNCTIONS";	    
			db.transaction(function(tx){
				var updatedId = 0, arr1="";  
		    	tx.executeSql(sql_max_updated,[],function(tx, results){
		    		var len = results.rows.length;
		    		if(len>0)
		    		updatedId = results.rows.item(0).updateid;
		    		updatedId= updatedId+1;
		    		arr1 = faxid+"|>"+faxvalue+"|>"+type;
		    		console.log("arr1"+arr1);
		    		console.log("updated function id "+updatedId);
				    tx.executeSql(updates,[updatedId, "updateFaxData", arr1, setLastUpdatesTime()]);
    			},errorCB);		
		}, errorCB, successDB);   
	}

}
/***********************************************************************************************/
function saveSocialData()
{
//if(savedSocialDatadbClick%2 != 0){
	savedChanges="true";

	if(dummyEmailCounter>=0)
	{
		emailsArrCounter=-1;
		var emailphoneid = 'emailphone', value="", emailphonehiddenid="emailphonehidden", emailphonehiddenvalue="", 
		emailconnectionRelations="",emailfamilyvalue="",
		emailfriendvalue="", emailbusinessvalue="", emailpublicvalue="", emailfamilyImgId='familyemail', 
		emailfriendImgId='friendemail', 
		emailbusinessImgId='businessemail', emailpublicImgId='publicemail';
		for(var l=0;l<=dummyEmailCounter;l++)
		{
			emailconnectionRelations="";
			emailphoneid="emailphone";
			emailphonehiddenid="emailphonehidden";
			emailphoneid= emailphoneid+l+id;
			emailphonehiddenid = emailphonehiddenid+l+id; 
			emailfamilyImgId='familyemail';
			emailfriendImgId='friendemail'; 
			emailbusinessImgId='businessemail';
			emailpublicImgId='publicemail';
				
			emailpublicImgId=emailpublicImgId+l;
			emailfamilyImgId=emailfamilyImgId+l;
			emailbusinessImgId=emailbusinessImgId+l;
			emailfriendImgId=emailfriendImgId+l;
				//alert(document.getElementById(emailphoneid).value);
			if(document.getElementById(emailphoneid)!= null){
				value = document.getElementById(emailphoneid).value;
				if(value!=""){
					var emailphonehiddenvalue = document.getElementById(emailphonehiddenid).value;				
					emailfamilyvalue = document.getElementById(emailfamilyImgId).src;
					if(emailfamilyvalue=='file:///android_asset/www/css/imgs/icons/family.png')
						emailconnectionRelations+=familyId;
							
					emailfriendvalue = document.getElementById(emailfriendImgId).src;
					if(emailfriendvalue=='file:///android_asset/www/css/imgs/icons/friends.png')
						emailconnectionRelations+=','+friendsId;
						
					emailbusinessvalue = document.getElementById(emailbusinessImgId).src;
					if(emailbusinessvalue=='file:///android_asset/www/css/imgs/icons/business.png')
						emailconnectionRelations+=','+businessId;
		
					emailpublicvalue = document.getElementById(emailpublicImgId).src;
					if(publicvalue=='file:///android_asset/www/css/imgs/icons/public.png')
						emailconnectionRelations+=','+publicId;
				
					emailsArrCounter++;		
					emailconnectionRelations="4";
					updateEmails(emailphonehiddenvalue, value, emailconnectionRelations);
				}
			}
			else
				dummyEmailCounter--;				
		}
	}	
	
	if(dummyMobileCounter>=0)
		{
			mobilesArrCounter= -1;
			var mobilephoneid = 'mobilephone', value="", mobilephonehiddenid="mobilephonehidden", mobilephonehiddenvalue="", 
			connectionRelations="",familyvalue="",
			friendvalue="", businessvalue="", publicvalue="", familyImgId='familymobile', friendImgId='friendmobile', 
			businessImgId='businessmobile', publicImgId='publicmobile';
			for(var l=0;l<=dummyMobileCounter;l++)
			{
				connectionRelations="";
				mobilephoneid="mobilephone";
				mobilephonehiddenid="mobilephonehidden";
				mobilephoneid= mobilephoneid+l+id;
				mobilephonehiddenid = mobilephonehiddenid+l+id; 

				familyImgId='familymobile';
				friendImgId='friendmobile'; 
				businessImgId='businessmobile';
				publicImgId='publicmobile';
				
				publicImgId=publicImgId+l;
				familyImgId=familyImgId+l;
				businessImgId=businessImgId+l;
				friendImgId=friendImgId+l;
				//alert(document.getElementById(mobilephoneid));
				if(document.getElementById(mobilephoneid) != null){
						value = document.getElementById(mobilephoneid).value;
						//alert(value);
						if(value !=""){
						mobilephonehiddenvalue = document.getElementById(mobilephonehiddenid).value;
						
						/*familyvalue = document.getElementById(familyImgId).src;
						if(familyvalue=='file:///android_asset/www/css/imgs/icons/family.png')
						{
							connectionRelations+=','+familyId;
						}
							
						friendvalue = document.getElementById(friendImgId).src;
						if(friendvalue=='file:///android_asset/www/css/imgs/icons/friends.png')
							connectionRelations+=','+friendsId;
						
						businessvalue = document.getElementById(businessImgId).src;
						if(businessvalue=='file:///android_asset/www/css/imgs/icons/business.png')
							connectionRelations+=','+businessId;
		
						publicvalue = document.getElementById(publicImgId).src;
						if(publicvalue=='file:///android_asset/www/css/imgs/icons/public.png')
							connectionRelations+=','+publicId;
						*/
						//	url = "http://cardsbond.com/BondSys/web/api/v1/contacts/update_mobiles?bond_contact_id="+id+"&bond_contact_mobile_id="+mobilephonehiddenvalue+"&mobile="+value+"&arr_levels="+connectionRelations;
						mobilesArrCounter++;
						//alert(mobilesArrCounter);
						connectionRelations="4";
						updateMobiles(mobilephonehiddenvalue, value, connectionRelations);
						
						
					}
				}
				else
				dummyMobileCounter--;
			}
	}	
	if(dummyPhoneCounter>=0)
	{
		phonesArrCounter=-1;
		var phonephoneid = 'phonephone', value="", phonephonehiddenid="phonephonehidden", phonephonehiddenvalue="", 
			phoneconnectionRelations="", phonefamilyvalue="",
			phonefriendvalue="", phonebusinessvalue="", phonepublicvalue="", phonefamilyImgId='familyphone', 
			phonefriendImgId='friendphone', phonebusinessImgId='businessphone', phonepublicImgId='publicphone';
		for(var l=0;l<=dummyPhoneCounter;l++)
		{
			phoneconnectionRelations="";
			phonephoneid="phonephone";
			phonephonehiddenid="phonephonehidden";
			phonephoneid= phonephoneid+l+id;
			phonephonehiddenid = phonephonehiddenid+l+id; 
			phonefamilyImgId='familyphone';
			phonefriendImgId='friendphone'; 
			phonebusinessImgId='businessphone';
			phonepublicImgId='publicphone';
				
			phonepublicImgId=phonepublicImgId+l;
			phonefamilyImgId=phonefamilyImgId+l;
			phonebusinessImgId=phonebusinessImgId+l;
			phonefriendImgId=phonefriendImgId+l;
			//alert(document.getElementById(phonephoneid).value);
			if(document.getElementById(phonephoneid) != null){				
				value = document.getElementById(phonephoneid).value;
				if(value!=""){
					var phonephonehiddenvalue = document.getElementById(phonephonehiddenid).value;
					
					/*phonefamilyvalue = document.getElementById(phonefamilyImgId).src;
					if(phonefamilyvalue=='file:///android_asset/www/css/imgs/icons/family.png')
					{
						phoneconnectionRelations+=familyId;
					}
						
					phonefriendvalue = document.getElementById(phonefriendImgId).src;
					if(phonefriendvalue=='file:///android_asset/www/css/imgs/icons/friends.png')
						phoneconnectionRelations+=','+friendsId;
					
					phonebusinessvalue = document.getElementById(phonebusinessImgId).src;
					if(phonebusinessvalue=='file:///android_asset/www/css/imgs/icons/business.png')
						phoneconnectionRelations+=','+businessId;
	
					phonepublicvalue = document.getElementById(phonepublicImgId).src;
					if(phonepublicvalue=='file:///android_asset/www/css/imgs/icons/public.png')
						phoneconnectionRelations+=','+publicId;
					*/
					
					phonesArrCounter++;
					phoneconnectionRelations="4";
					updatePhones(phonephonehiddenvalue, value, phoneconnectionRelations);
					
					
					
				}
			}
		else
			dummyPhoneCounter--;
					
		}
	}	

	if(dummyFaxCounter>=0)
	{
		faxesArrCounter=-1;
		var faxphoneid = 'faxphone', value="", faxphonehiddenid="faxphonehidden", faxphonehiddenvalue="", 
		faxconnectionRelations="",faxfamilyvalue="",
		faxfriendvalue="", faxbusinessvalue="", faxpublicvalue="", faxfamilyImgId='familyfax', 
		faxfriendImgId='friendfax', 
		faxbusinessImgId='businessfax', faxpublicImgId='publicfax';
		for(var l=0;l<=dummyFaxCounter;l++)
		{
			faxconnectionRelations="";
			faxphoneid="faxphone";
			faxphonehiddenid="faxphonehidden";
			faxphoneid= faxphoneid+l+id;
			faxphonehiddenid = faxphonehiddenid+l+id; 
			faxfamilyImgId='familyfax';
			faxfriendImgId='friendfax'; 
			faxbusinessImgId='businessfax';
			faxpublicImgId='publicfax';
				
			faxpublicImgId=faxpublicImgId+l;
			faxfamilyImgId=faxfamilyImgId+l;
			faxbusinessImgId=faxbusinessImgId+l;
			faxfriendImgId=faxfriendImgId+l;
			var value="";
			if(document.getElementById(faxphoneid)!= null){			
				value = document.getElementById(faxphoneid).value;
				if(value!=""){			
					var faxphonehiddenvalue = document.getElementById(faxphonehiddenid).value;				
					faxfamilyvalue = document.getElementById(faxfamilyImgId).src;
					if(faxfamilyvalue=='file:///android_asset/www/css/imgs/icons/family.png')
						faxconnectionRelations+=familyId;
						
					faxfriendvalue = document.getElementById(faxfriendImgId).src;
					if(faxfriendvalue=='file:///android_asset/www/css/imgs/icons/friends.png')
						faxconnectionRelations+=','+friendsId;
					
					faxbusinessvalue = document.getElementById(faxbusinessImgId).src;
					if(faxbusinessvalue=='file:///android_asset/www/css/imgs/icons/business.png')
						faxconnectionRelations+=','+businessId;
	
					faxpublicvalue = document.getElementById(faxpublicImgId).src;
					if(publicvalue=='file:///android_asset/www/css/imgs/icons/public.png')
						faxconnectionRelations+=','+publicId;
					faxesArrCounter++;				
					//var url = "http://cardsbond.com/BondSys/web/api/v1/contacts/update_faxes?bond_contact_fax_id="+faxphonehiddenvalue+"&fax="+value+"&arr_levels="+faxconnectionRelations+"&bond_contact_id="+id;
					//alert(url);
					faxconnectionRelations="4";
					updateFaxes(faxphonehiddenvalue, value, faxconnectionRelations);
					
					
					
				}
			}
			else
				dummyFaxCounter--;
		}
	}

	var facebook="";
	if(document.getElementById('editfacebook') != null){	
		facebook = document.getElementById('editfacebook').value;
		var facebookId = document.getElementById('editfacebookhidden').value;	
	}
	var twitter="";
	if(document.getElementById('edittwitter') != null){
		twitter = document.getElementById('edittwitter').value;
		var twitterId = document.getElementById('edittwitterhidden').value;
	}
	var skype="";
	if(document.getElementById('editskype') != null){	
		skype= document.getElementById('editskype').value;
		var skypeId = document.getElementById('editskypehidden').value;
	}
	var linkedin="";
	if(document.getElementById('editlinkedin') != null){	
		linkedin = document.getElementById('editlinkedin').value;
		var linkedinId = document.getElementById('editlinkedinhidden').value;
		
	}
			
	if(skype!=""){	
		var skypeContactsLevel="4";		
		/*if(parseInt(document.getElementById('skypeProfileFamilyIcon').alt) > 0)
			skypeContactsLevel=skypeContactsLevel+document.getElementById('skypeProfileFamilyIcon').alt+",";
		
		if(parseInt(document.getElementById('skypeProfileBusinessIcon').alt) > 0)
			skypeContactsLevel=skypeContactsLevel+document.getElementById('skypeProfileBusinessIcon').alt+",";

		if(parseInt(document.getElementById('skypeProfileFriendsIcon').alt) > 0)
			skypeContactsLevel=skypeContactsLevel+document.getElementById('skypeProfileFriendsIcon').alt+",";
		
		if(parseInt(document.getElementById('skypeProfilePublicIcon').alt) > 0)
			skypeContactsLevel=skypeContactsLevel+document.getElementById('skypeProfilePublicIcon').alt+",";
	*/
		url = "http://cardsbond.com/BondSys/web/api/v1/contacts/update_socialnetworks?bond_contact_socialnetwork_id="+skypeId+"&socialnetwork="+skype+"&arr_levels="+skypeContactsLevel+"&bond_contact_id="+id+"&bond_socialnetwork_id="+skypeIdOnServer;
		skypeContactsLevel="4";
		updateSocialLocal(skypeId, skype, "Skype", skypeContactsLevel);
		updateSocialData(skypeId, skype, "Skype", skypeContactsLevel, skypeIdOnServer);
	}	

	if(facebook!=""){
		var facebookContactsLevel="";
		
		/*if(parseInt(document.getElementById('facebookProfileFamilyIcon').alt) > 0)
			facebookContactsLevel=facebookContactsLevel+document.getElementById('facebookProfileFamilyIcon').alt+",";
		
		if(parseInt(document.getElementById('facebookProfileBusinessIcon').alt) > 0)
			facebookContactsLevel=facebookContactsLevel+document.getElementById('facebookProfileBusinessIcon').alt+",";

		if(parseInt(document.getElementById('facebookProfileFriendsIcon').alt) > 0)
			facebookContactsLevel=facebookContactsLevel+document.getElementById('facebookProfileFriendsIcon').alt+",";
		
		if(parseInt(document.getElementById('facebookProfilePublicIcon').alt) > 0)
			facebookContactsLevel=facebookContactsLevel+document.getElementById('facebookProfilePublicIcon').alt+",";
*/
//		url = "http://cardsbond.com/BondSys/web/api/v1/contacts/update_socialnetworks?bond_contact_socialnetwork_id="+facebookId+"&socialnetwork="+facebook+"&arr_levels="+facebookContactsLevel+"&bond_contact_id="+id+"&bond_socialnetwork_id="+facebookIdOnServer;
//		alert(url)
		facebookContactsLevel="4";
		updateSocialLocal(facebookId, facebook, "Facebook", facebookContactsLevel);		
		updateSocialData(facebookId, facebook, "Facebook", facebookContactsLevel, facebookIdOnServer);

//		updateSocialData(url);
	}
	
	if(twitter!=""){
		var twitterContactsLevel="";
		/*
		if(parseInt(document.getElementById('twitterProfileFamilyIcon').alt) > 0)
			twitterContactsLevel=twitterContactsLevel+document.getElementById('twitterProfileFamilyIcon').alt+",";
		
		if(parseInt(document.getElementById('twitterProfileBusinessIcon').alt) > 0)
			twitterContactsLevel=twitterContactsLevel+document.getElementById('twitterProfileBusinessIcon').alt+",";

		if(parseInt(document.getElementById('twitterProfileFriendsIcon').alt) > 0)
			twitterContactsLevel=twitterContactsLevel+document.getElementById('twitterProfileFriendsIcon').alt+",";
		
		if(parseInt(document.getElementById('twitterProfilePublicIcon').alt) > 0)
			twitterContactsLevel=twitterContactsLevel+document.getElementById('twitterProfilePublicIcon').alt+",";
*/
//		url = "http://cardsbond.com/BondSys/web/api/v1/contacts/update_socialnetworks?bond_contact_socialnetwork_id="+twitterId+"&socialnetwork="+twitter+"&arr_levels="+twitterContactsLevel+"&bond_contact_id="+id+"&bond_socialnetwork_id="+twitterIdOnServer;
//		alert(url); 
		twitterContactsLevel="4";
		updateSocialLocal(twitterId, twitter, "Twitter", twitterContactsLevel);		
		updateSocialData(twitterId, twitter, "Twitter", twitterContactsLevel, twitterIdOnServer);
//		updateSocialData(url);
	}

	if(linkedin!=""){
		var linkedinContactsLevel="";
		
		if(parseInt(document.getElementById('linkedinProfileFamilyIcon').alt) > 0)
			linkedinContactsLevel=linkedinContactsLevel+document.getElementById('linkedinProfileFamilyIcon').alt+",";
		
		if(parseInt(document.getElementById('linkedinProfileBusinessIcon').alt) > 0)
			linkedinContactsLevel=linkedinContactsLevel+document.getElementById('linkedinProfileBusinessIcon').alt+",";

		if(parseInt(document.getElementById('linkedinProfileFriendsIcon').alt) > 0)
			linkedinContactsLevel=linkedinContactsLevel+document.getElementById('linkedinProfileFriendsIcon').alt+",";
		
		if(parseInt(document.getElementById('linkedinProfilePublicIcon').alt) > 0)
			linkedinContactsLevel=linkedinContactsLevel+document.getElementById('linkedinProfilePublicIcon').alt+",";
	
	
//		url = "http://cardsbond.com/BondSys/web/api/v1/contacts/update_socialnetworks?bond_contact_socialnetwork_id="+linkedinId+"&socialnetwork="+linkedin+"&arr_levels="+linkedinContactsLevel+"&bond_contact_id="+id+"&bond_socialnetwork_id="+linkedinIdOnServer;
//		alert(url);
		linkedinContactsLevel="4";
		updateSocialLocal(linkedinId, linkedin, "LinkedIn", linkedinContactsLevel);
		updateSocialData(linkedinId, linkedin, "Linkedin", linkedinContactsLevel, linkedinIdOnServer);
//		updateSocialData(url);
	}

 	javascript:menu('0');
//	fillInMyProfile();   			
//	showMyProfile();				  
//}
//savedSocialDatadbClick++;
}
/***********************************************************************************************/
function updateSocialLocal(socialId, socialVal, type, contactsLevels)
{
	var socialSQL="";
	var currentTime = new Date();
	if(socialId!=""){
	    socialSQL = 'UPDATE CONTACTSOCIALNETWORKS SET url = (?) , typeid=(?) WHERE contactid=(?) AND socialid=(?) AND socialname=(?) ';
		db.transaction(function(tx){
			tx.executeSql(socialSQL,[socialVal, contactsLevels, id, socialId, type],errorCB);	   		
	    	}, errorCB, successDB);
	}	
	else
	{
		socialSQL = 'INSERT INTO CONTACTSOCIALNETWORKS (socialid, contactid, socialname, url, typeid) VALUES (?, ?, ?, ?, ?)';
		db.transaction(function(tx){
			tx.executeSql(socialSQL,[currentTime, id, type, socialVal, contactsLevels]);	   // "00" must be changed to the inserted id on server		
	    	}, errorCB, successDB);   
	}
}
/***********************************************************************************************/
function updateSocialData(srv_socialId, srv_socialUrl, socialName, srv_contactsLevel, socialIdOnServer, updatedId)
{
	if(srv_socialId!="")
		var url = "http://cardsbond.com/BondSys/web/api/v1/contacts/update_socialnetworks?bond_contact_socialnetwork_id="+srv_socialId+"&socialnetwork="+srv_socialUrl+"&arr_levels=&bond_contact_id="+id+"&bond_socialnetwork_id="+socialIdOnServer+"&change_levels=flase";
	else
		var url = "http://cardsbond.com/BondSys/web/api/v1/contacts/update_socialnetworks?bond_contact_socialnetwork_id="+srv_socialId+"&socialnetwork="+srv_socialUrl+"&arr_levels=4&bond_contact_id="+id+"&bond_socialnetwork_id="+socialIdOnServer;
console.log("social network"+url);
	var ajaxRequest;
		try{
			ajaxRequest = new XMLHttpRequest();
		} catch (e){
			try{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
					//alert("Microsoft");
				} catch (e){
					alert("Your browser broke!");
				return false;
				}
			}
		}		
		
		ajaxRequest.onreadystatechange = function(){
		//alert(ajaxRequest.readyState);
			if(ajaxRequest.readyState == 4){
					var response=ajaxRequest.responseText;
				//	alert(response);
					if(response.search('socialnetwork_id') != -1){					
							var results = JSON.parse(response);
							var socialnetwork_id = results.socialnetwork_id;
							
							var socialSQL = 'UPDATE CONTACTSOCIALNETWORKS SET socialId =(?) WHERE url=(?)  AND contactid=(?)';
							db.transaction(function(tx){
							tx.executeSql(socialSQL,[socialnetwork_id, srv_socialUrl, id]);	   		
					    	}, errorCB, successDB);
					    	
							var update_update = 'DELETE FROM UPDATESFUNCTIONS WHERE updateid=(?)';
							db.transaction(function(tx){
							tx.executeSql(update_update,[updatedId],errorCB);	   		
					    	}, errorCB, successDB);
					    							
	            	}
	        		    		
				}
			}
		ajaxRequest.open("GET", url, true);		
		ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		//ajaxRequest.setRequestHeader("Content-length", param.length);
		ajaxRequest.setRequestHeader("Connection", "close");
		ajaxRequest.send();	
}	
/***********************************************************************************************/
function updateProfileInServer(upFname, upLname, uppassword, upCompanyid, upCompany,upPositionid, upPosition,
		 profileImgsrc, upAddress, upCountry, upCity, hidden, social, upAboutme, wants, upReplacement, imageExt_, updatedId)
{
var other_position="", other_company="";
if(social==true)
social = "1";
else
social ="0";

if(hidden==true)
hidden = "1";
else
hidden ="0";

if(upReplacement!="")
updateReplacement(upReplacement);
var newImage="0";
if(imageExt_!="")
	newImage="1";
else
	newImage="0";	
if(upPositionid == "-1")
	other_position = upPosition;

if(upCompanyid == "-1")
	other_company = upCompany;
	
	var url = "http://cardsbond.com/BondSys/web/api/v1/events/edit_profile?fname="+upFname+"&lname="+upLname+
	"&password="+uppassword+"&company_id="+upCompanyid+"&position_id="+	upPositionid+"&replacement_bond_id="+"&country_id="+upCountry+
	"&city_id="+upCity+"&address="+upAddress+"&wants="+wants+"&tags=&about="+upAboutme+"&position="+upPosition+"&company="+upCompany+
	"&dir=contacts_images&serial=1&image="+profileImgsrc+"&unique_number=1&be_social="+social+"&bond_contact_id="+id+
	"&newImage="+newImage+"&other_position="+other_position+"&other_company="+other_company;
	console.log("editProfile "+url);
	var ajaxRequest;
		try{
			ajaxRequest = new XMLHttpRequest();
		} catch (e){
			try{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
					//alert("Microsoft");
				} catch (e){
					alert("Your browser broke!");
				return false;
				}
			}
		}		
		ajaxRequest.onreadystatechange = function(){
		//alert(ajaxRequest.readyState);
			if(ajaxRequest.readyState == 4){
					var response=ajaxRequest.responseText;
					console.log("edit profile response "+response);
					if(response!=""){
						var results = JSON.parse(response);
						
						if(response.search('error')== -1){
							if(upCompanyid == "-1"){
								insertSystemCompanies(results.result.company_id, "", other_company, "", "", "", "");
								var	sqltxt = 'UPDATE CONTACTS SET companyid=(?), company=(?) WHERE contactid=(?)';											
								var	selecttxt = 'SELECT companyid, company FROM CONTACTS WHERE contactid=(?)';											

							   	db.transaction(function(tx){
									tx.executeSql(sqltxt,[results.result.company_id, other_company, id]);
									
							    }, errorCB, function(){
									mycompanyid = results.result.company_id;
							    
							    }); 
								

							}
		            	}
		            	else
		            		$.msg({ content : results.error.message });	    
	            	}
	        		    		
				}
			}
		ajaxRequest.open("GET", url, true);		
		ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		//ajaxRequest.setRequestHeader("Content-length", param.length);
		ajaxRequest.setRequestHeader("Connection", "close");
		ajaxRequest.send();	
}
/***********************************************************************************************/
function insertSystemCompanies(companyId, erp, companyName, logo, field, brief, address){	
	console.log("insertSystemCompanies"+companyId);
	var insertcompanysql = 'INSERT INTO SYSTEMCOMPANIES (companyid, name, imgsource, timezone, field, info, address) VALUES (?,?,?,?,?,?,?)';
    var selectcompanysql = 'SELECT companyid FROM SYSTEMCOMPANIES WHERE companyid=(?)';
	db.transaction(function(tx){
		tx.executeSql(selectcompanysql,[companyId],function(tx,results){
			if(results.rows.length==0){
		   		tx.executeSql(insertcompanysql,[companyId, companyName, logo, 0 , field, brief, address]);
		   		console.log(companyName + "inserted");
		   	}
		},errorCB);
    	
    }, errorCB, successDB);
}
/***********************************************************************************************/
function updateReplacement(upReplacement)
{
	var url = "http://www.cardsbond.com/BondSys/web/api/v1/contacts/replace_contact?successor_bond="+upReplacement+"&bond_contact_id="+id;
	//alert(url);
	var ajaxRequest;
		try{
			ajaxRequest = new XMLHttpRequest();
		} catch (e){
			try{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
					//alert("Microsoft");
				} catch (e){
					alert("Your browser broke!");
				return false;
				}
			}
		}		
		ajaxRequest.onreadystatechange = function(){
		//alert(ajaxRequest.readyState);
			if(ajaxRequest.readyState == 4){
					var response=ajaxRequest.responseText;
					//alert(response);
					if(response.search('error')== -1){
						//var results = JSON.parse(response);
						//alert(results);
	            	}
	        		    		
				}
			}
		ajaxRequest.open("GET", url, true);		
		ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		//ajaxRequest.setRequestHeader("Content-length", param.length);
		ajaxRequest.setRequestHeader("Connection", "close");
		ajaxRequest.send();	
}
/***********************************************************************************************/
function fillInEditProfileData()
{		
	getMyCompanyProfileData();
	document.getElementById('edit_profile_picture').src = myProfImage;	 	
	document.getElementById('updated_fname').value = fname;
	document.getElementById('updated_lname').value = lname;
	document.getElementById('updated_password').value = password;		
	document.getElementById('updated_conf_password').value = password;
	document.getElementById('wants').value = mywants;		
	document.getElementById('updated_address').value = myaddress;

		if(mybehidden=="true")
			document.getElementById("behidden").checked =true;
		else
			document.getElementById("behidden").checked =false;
			
		if(myissocial=="true")
			document.getElementById("besocial").checked =true;
		else
			document.getElementById("besocial").checked =false;

		document.getElementById('editAboutMe').value=myAboutme;
/*		
	alert("email c"+emailsArrCounter);
	alert("MOBILLE c"+mobilesArrCounter);
	alert("PHONE c"+phonesArrCounter);
	alert("FAX c"+faxesArrCounter);
*/
	dummyMobileCounter= mobilesArrCounter;
	console.log("mobilesArrCounter"+mobilesArrCounter);
	if(mobilesArrCounter == -1){
		
		addNewMobileInput("-1");
	}
	else if(mobilesArrCounter>=0)
	{

		for(var l=0;l<=mobilesArrCounter;l++)
		{
			showMobileDiv(l);
		}
	}
////////////////////////////////////////////

	dummyEmailCounter=emailsArrCounter;
	if(emailsArrCounter == -1){
		addNewEmailInput("-1");
	}
	else if(emailsArrCounter>=0)
	{
		for(var l=0;l<=emailsArrCounter;l++)
		{
			showEmailDiv(l);
		}
	}
//////////////////////////////////////////

	dummyPhoneCounter=phonesArrCounter;
	if(phonesArrCounter == -1){
		addNewPhoneInput("-1");
	}
	else if(phonesArrCounter>=0)
	{
		for(var l=0;l<=phonesArrCounter;l++)
		{		
			showPhoneDiv(l);
		}
	}
//////////////////////////////////////////

	dummyFaxCounter=faxesArrCounter;
	if(faxesArrCounter == -1){
		addNewFaxInput("-1");
	}
	else if(faxesArrCounter>=0)
	{
		for(var l=0;l<=faxesArrCounter;l++)
		{
			showFaxDiv(l);
		}
	}
//////////////////////////////////////////		
		document.getElementById('editfacebook').value=myfacebook;
		document.getElementById('editfacebookhidden').value=myfacebookId;
		if(myfacebook!=""){
			var facebookLevelsArray =myfacebookLevels.split(",");
			for(var i=0; i<facebookLevelsArray.length;i++)
			{
				if(facebookLevelsArray[i]== "1"){
					document.getElementById('facebookProfileFamilyIcon').src="css/imgs/icons/family.png";
					document.getElementById('facebookProfileFamilyIcon').alt="1";
					}
				else if(facebookLevelsArray[i]== "4"){
					document.getElementById('facebookProfileBusinessIcon').src="css/imgs/icons/business.png";
					document.getElementById('facebookProfileBusinessIcon').alt="4";
					}
	
				else if(facebookLevelsArray[i]== "5"){
					document.getElementById('facebookProfileFriendsIcon').src="css/imgs/icons/friends.png";
					document.getElementById('facebookProfileFriendsIcon').alt="5";
				}
	
				else if(facebookLevelsArray[i]== "6"){
					document.getElementById('facebookProfilePublicIcon').src="css/imgs/icons/public.png";
					document.getElementById('facebookProfilePublicIcon').alt="6";
				}
	
			}
		}		
//		alert("myfacebookId: "+myfacebookId);
//		alert("myfacebookValue: "+myfacebook);
		
		
		document.getElementById('edittwitter').value=mytwitter;
		document.getElementById('edittwitterhidden').value=mytwitterId;
		if(mytwitter!=""){
			var twitterLevelsArray =mytwitterLevels.split(",");
			for(var i=0; i<twitterLevelsArray.length;i++)
			{
				if(twitterLevelsArray[i]== "1"){
					document.getElementById('twitterProfileFamilyIcon').src="css/imgs/icons/family.png";
					document.getElementById('twitterProfileFamilyIcon').alt="1";
				}
				else if(twitterLevelsArray[i]== "4"){
					document.getElementById('twitterProfileBusinessIcon').src="css/imgs/icons/business.png";
					document.getElementById('twitterProfileBusinessIcon').alt="4";
				}
	
				else if(twitterLevelsArray[i]== "5"){
					document.getElementById('twitterProfileFriendsIcon').src="css/imgs/icons/friends.png";
					document.getElementById('twitterProfileFriendsIcon').alt="5";
				}
	
				else if(twitterLevelsArray[i]== "6"){
					document.getElementById('twitterProfilePublicIcon').src="css/imgs/icons/public.png";
					document.getElementById('twitterProfilePublicIcon').alt="6";
				}
			}
		
		}		
		
//		alert("mytwitterId: "+mytwitterId);
//		alert("mytwitterValue: "+mytwitter);
		
		document.getElementById('editlinkedin').value=mylinkedin;
		document.getElementById('editlinkedinhidden').value=mylinkedinId;
		if(mylinkedin!=""){
			var linkedinLevelsArray =mylinkedinLevels.split(",");
			for(var i=0; i<linkedinLevelsArray.length;i++)
			{
				if(linkedinLevelsArray[i]== "1"){
					document.getElementById('linkedinProfileFamilyIcon').src="css/imgs/icons/family.png";
					document.getElementById('linkedinProfileFamilyIcon').alt="1";
				}
				else if(linkedinLevelsArray[i]== "4"){
					document.getElementById('linkedinProfileBusinessIcon').src="css/imgs/icons/business.png";
					document.getElementById('linkedinProfileBusinessIcon').alt="4";
				}
	
				else if(linkedinLevelsArray[i]== "5"){
					document.getElementById('linkedinProfileFriendsIcon').src="css/imgs/icons/friends.png";
					document.getElementById('linkedinProfileFriendsIcon').alt="5";
				}
	
				else if(linkedinLevelsArray[i]== "6"){
					document.getElementById('linkedinProfilePublicIcon').src="css/imgs/icons/public.png";
					document.getElementById('linkedinProfilePublicIcon').alt="6";
				}
			}
		
		}
//		alert("mylinkedinId: "+mylinkedinId);
//		alert("mylinkedinIdValue: "+mylinkedin);
		
		document.getElementById('editskype').value=myskype;
		document.getElementById('editskypehidden').value=myskypeId;
		if(myskype!=""){
			var skypeLevelsArray =myskypeLevels.split(",");
			for(var i=0; i<skypeLevelsArray.length;i++)
			{
				if(skypeLevelsArray[i]== "1"){
					document.getElementById('skypeProfileFamilyIcon').src="css/imgs/icons/family.png";
					document.getElementById('skypeProfileFamilyIcon').alt="1";
				}
				else if(skypeLevelsArray[i]== "4"){
					document.getElementById('skypeProfileBusinessIcon').src="css/imgs/icons/business.png";
					document.getElementById('skypeProfileBusinessIcon').alt="4";
				}
	
				else if(skypeLevelsArray[i]== "5"){
					document.getElementById('skypeProfileFriendsIcon').src="css/imgs/icons/friends.png";
					document.getElementById('skypeProfileFriendsIcon').alt="5";
				}
	
				else if(skypeLevelsArray[i]== "6"){
					document.getElementById('skypeProfilePublicIcon').src="css/imgs/icons/public.png";
					document.getElementById('skypeProfilePublicIcon').alt="6";
				}
			}
		
		}
//		alert("myskypeId: "+myskypeId);
//		alert("myskypeValue: "+myskype);
		
//		document.getElementById('editgplus').value=mygoogle;
		var availableTags={};
		var companies= 'SELECT * FROM SYSTEMCOMPANIES';
	
		db.transaction(function(tx){
	    	tx.executeSql(companies,[],function(tx,results){
	    		var len = results.rows.length;
	    		var myNode = document.getElementById('updated_company');
			 	console.log("mycompanyid: fill "+mycompanyid);
		
		        for (var i=0; i<len; i++)     
        		{
     				var name = results.rows.item(i).name;
					var comid = results.rows.item(i).companyid;
					console.log("comid: "+comid);
					var option = document.createElement('option');
     				var t=document.createTextNode(name);
				 	option.setAttribute('id', comid);
				 	option.appendChild(t);
					available_companies[i] = name;
					available_companies_id[i] = comid;

				 	if(comid==mycompanyid){	
				 		//option.setAttribute('selected', 'true');
				 		//alert(name);
				 		console.log("mycompanyid: set "+mycompanyid);

				 		myNode.value = name;
				 		
				 	}
				 					
	//				myNode.appendChild(option);
				}
				$( "#updated_company" ).autocomplete({
			      source: available_companies
			    });
				
	    	}
	    	,errorCB);
	        }, errorCB, successDB);		
		
		var positions= 'SELECT * FROM POSITIONS';
	
		db.transaction(function(tx){
	    	tx.executeSql(positions,[],function(tx,results){
	    		var len = results.rows.length;		     	   
				var myNode = document.getElementById('updated_position');
				while (myNode.firstChild) {
		    		myNode.removeChild(myNode.firstChild);
				}		
		        for (var i=0; i<len; i++)     
        		{
     				var name = results.rows.item(i).name;
					var posid = results.rows.item(i).positionid;
//					alert("posid: "+posid);
//					var option = document.createElement('option');
//     				var t=document.createTextNode(name);
//				 	option.setAttribute('id', posid);
//				 	option.appendChild(t);
					available_positions[i] = name;
					available_positions_id[i] = posid;

					if(posid==mypositionid){
//				 		alert("mypositionid: "+mypositionid);
				 		//option.setAttribute('selected', 'true');
				 		document.getElementById('updated_position').value = name;
				 		
				 	}
//					document.getElementById('updated_position').appendChild(option);
				}
				 $( "#updated_position" ).autocomplete({
			      source: available_positions
			    });
	    	}
	    	,errorCB);
	        }, errorCB, successDB);		
		

		var countries= 'SELECT * FROM COUNTRIES';
	
		db.transaction(function(tx){
	    	tx.executeSql(countries,[],function(tx,results){
	    		var len = results.rows.length;		     	   
		    	var myNode = document.getElementById('updated_country');
			while (myNode.firstChild) {
	    		myNode.removeChild(myNode.firstChild);
			}		     	   
		        for (var i=0; i<len; i++)     
        		{
     				var name=results.rows.item(i).name;
     				var cid= results.rows.item(i).countryid;
//     				alert("cid: "+cid);
    				var option = document.createElement('option');
     				var t=document.createTextNode(name);
				 	option.setAttribute('id', cid);
				 	option.appendChild(t);
	 				if(cid==mycountry){
//						alert("mycountry: "+mycountry);
					
					 	option.setAttribute('selected', 'true');
					}
					document.getElementById('updated_country').appendChild(option);
				}
	    	}
	    	,errorCB);
	        }, errorCB, successDB);		

		var cities= 'SELECT * FROM CITIES';
	
		db.transaction(function(tx){
	    	tx.executeSql(cities,[],function(tx,results){
	    		var len = results.rows.length;		     	   
		    	var myNode = document.getElementById('updated_city');
			while (myNode.firstChild) {
	    		myNode.removeChild(myNode.firstChild);
			}		     	   
		        for (var i=0; i<len; i++)     
        		{
     				var name=results.rows.item(i).name;
     				var cityid= results.rows.item(i).cityid;
 //    				alert("cityid: "+cityid);
					var option = document.createElement('option');
     				var t=document.createTextNode(name);
				 	option.setAttribute('id', cityid);
				 	option.appendChild(t);					
					if(cityid==mycity){
//						alert("mycityy: "+mycity);
					 	option.setAttribute('selected', 'true');
					}
					document.getElementById('updated_city').appendChild(option);
				}
	    	}
	    	,errorCB);
	        }, errorCB, successDB);		

	}
/***********************************************************************************************/
function showIcons(iconId)
{
	for(var l=0;l<=mobilesArrCounter;l++)
	{
		var conId = "mobile_con_type"+l;
		$(conId).hide();
	}

	$(iconId).show();
}
/***********************************************************************************************/
function hideIcons(iconId)
{
	for(var l=0;l<=mobilesArrCounter;l++)
	{
		var conId = "mobile_con_type"+l;
		$(conId).hide();
	}
	$(iconId).show();
}
/***********************************************************************************************/
function toggleSocialProfileIcon(imgId, type)
{
	var imgIdToToggle= document.getElementById(imgId).src;
	if(type=='family'){	
		if(imgIdToToggle== 'file:///android_asset/www/css/imgs/icons/family-in.png') 
		{
			setTimeout(function(){
			document.getElementById(imgId).src= 'file:///android_asset/www/css/imgs/icons/family.png';
			document.getElementById(imgId).alt="1";
			},300);
		}	
		else if(imgIdToToggle=='file:///android_asset/www/css/imgs/icons/family.png') 	
		{
			setTimeout(function(){
			document.getElementById(imgId).src='file:///android_asset/www/css/imgs/icons/family-in.png';
			document.getElementById(imgId).alt="-1";
			},300);
		}		
	}
	else if(type=='friends'){	
		if(imgIdToToggle== 'file:///android_asset/www/css/imgs/icons/friends-in.png') 
		{
			setTimeout(function(){		
			document.getElementById(imgId).src= 'file:///android_asset/www/css/imgs/icons/friends.png';
			document.getElementById(imgId).alt="5";
			},300);
			
		}	
		else if(imgIdToToggle== 'file:///android_asset/www/css/imgs/icons/friends.png') 
		{
			setTimeout(function(){					
			document.getElementById(imgId).src= 'file:///android_asset/www/css/imgs/icons/friends-in.png';
			document.getElementById(imgId).alt="-5";
			},300);
			
		}	
	}
	else if(type=='public'){	
		if(imgIdToToggle== 'file:///android_asset/www/css/imgs/icons/toggle.png') 
		{
			setTimeout(function(){							
			document.getElementById(imgId).src= 'file:///android_asset/www/css/imgs/icons/toggle-in.png';
			document.getElementById(imgId).alt="-6";
			},300);			
		}	
		else if(imgIdToToggle== 'file:///android_asset/www/css/imgs/icons/toggle-in.png') 			
		{
			setTimeout(function(){							
			document.getElementById(imgId).src= 'file:///android_asset/www/css/imgs/icons/toggle.png';
			document.getElementById(imgId).alt="6";
			},300);
		}	
	}
	else if(type=='business'){	
		if(imgIdToToggle== 'file:///android_asset/www/css/imgs/icons/business-in.png') 
		{
			setTimeout(function(){							
			document.getElementById(imgId).src= 'file:///android_asset/www/css/imgs/icons/business.png';
			document.getElementById(imgId).alt="4";
			},300);			
		}		
		else if(imgIdToToggle == 'file:///android_asset/www/css/imgs/icons/business.png')
		{
			setTimeout(function(){									
			document.getElementById(imgId).src = 'file:///android_asset/www/css/imgs/icons/business-in.png';
			document.getElementById(imgId).alt="-4";
			},300);			
		}
	}
}
/***********************************************************************************************/
function isNumber(evt) {

	evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
//    	$.msg({ content : 'Only Numbers' });	    
        return false;
    
    }
    return true;
}
/***********************************************************************************************/
function togglePlusInProfile(e)
{
click_time = e['timeStamp'];
if (click_time && (click_time - last_click_time) < 2000)
{
    e.stopImmediatePropagation()
	e.preventDefault()
	  	return false
}
	    else
	    {
	  		last_click_time = click_time;
           	navigator.notification.vibrate(50);
           	$('#p-info').show(); $('#pub-minus').show(); $('#pub-plus').hide();
	       	document.getElementById('pub-plus').style.color = 'black';
  	   	 	document.getElementById('pub-minus').style.color = 'blue';
	    	   	

		}
		return true;
	
}
/***********************************************************************************************/
function toggleMinusInProfile(e)
{
click_time = e['timeStamp'];
if (click_time && (click_time - last_click_time) < 2000)
{
    e.stopImmediatePropagation()
	e.preventDefault()
	  	return false
}
	    else
	    {
	  		last_click_time = click_time;
   
	navigator.notification.vibrate(50);
    $('#p-info').hide(); $('#pub-plus').show(); $('#pub-minus').hide();
    document.getElementById('pub-plus').style.color = 'blue';
    document.getElementById('pub-minus').style.color = 'black';
    
    
		}
		return true;

}
/***********************************************************************************************/
function toggleDivInProfile(e)
{
	click_time = e['timeStamp'];
	if (click_time && (click_time - last_click_time) < 2000)
	{
	    e.stopImmediatePropagation()
		e.preventDefault()
		  	return false
	}
	else
	{
 		last_click_time = click_time;
		navigator.notification.vibrate(50);

    	if(document.getElementById('pub-plus').style.color == 'blue'){
    	   	$('#p-info').show(); $('#pub-minus').show(); $('#pub-plus').hide();
    	   	document.getElementById('pub-plus').style.color = 'black';
    	   	document.getElementById('pub-minus').style.color = 'blue';
    	   	}
    	
    	else{
    	    $('#p-info').hide(); $('#pub-plus').show(); $('#pub-minus').hide();
    	    document.getElementById('pub-minus').style.color = 'black';
    	   	document.getElementById('pub-plus').style.color = 'blue';
    	    
    	}
                
 	}
	return true;

}