function showMyProfile()
{
  	var sqltxt= 'SELECT DISTINCT name FROM COMPANIES WHERE name=(?)';
	db.transaction(function(tx){
		tx.executeSql(sqltxt,[company],companyySuccess,errorCB);
	},  successDB, errorCB);     
}
/******************************************************************************************/
function drawMyProfileImg(img) {
	console.log("img"+img);
	javascript:menu('18');
	var c = document.getElementById("myImageFull");
	//c.width = width;
	//c.height = height-100;
	c.src= img;
}
/******************************************************************************************/
function companyySuccess()
{	
	var callotherdata = "true";
	var div1 = document.createElement("div");
	div1.setAttribute('class', 'left');

	var image1 = document.createElement("img");
	image1.setAttribute('class', 'profile_img');
	image1.setAttribute('id', 'myprofile_img_id');
	image1.setAttribute('src', myProfImage);
	image1.setAttribute("onClick", "drawMyProfileImg(\'"+myProfImage+"'\)");
	//alert(document.getElementById('myprofile_img_id'));
	if(document.getElementById('myprofile_img_id') == null)
		div1.appendChild(image1);
	else	
		callotherdata = "false";
	
	var div2 = document.createElement("div");
	div2.setAttribute('class', 'prof-d');
	
	var span1= document.createElement("span");
	span1.setAttribute('id', 'prof_name_span');
	span1.setAttribute('style', 'font-size: 20px; color:#199ff9; ');
	
	div2.appendChild(span1);
	
	var p1= document.createElement("p");
	p1.setAttribute('id', 'prof_name_p');
	p1.setAttribute('style', 'font-size: 20px;');
	
	
	var em1= document.createElement("em");
	em1.setAttribute('id', "prof_name_em");
	em1.setAttribute('innerHTML',company);
	em1.setAttribute('style', 'font-size: 20px; font-weight: bold;');
	
	div2.appendChild(p1);
	div2.appendChild(em1);
	//alert(company);	
	var div3 = document.createElement("div");
	div3.setAttribute('class', 'clear');
	
	document.getElementById("myOwnProfile").appendChild(div1);
	document.getElementById("myOwnProfile").appendChild(div3);
	document.getElementById("myOwnProfile").appendChild(div2);

	document.getElementById("prof_name_span").innerHTML=name; 
	document.getElementById('prof_name_p').innerHTML=position+" @";
    document.getElementById("prof_name_em").innerHTML=company;

	if(callotherdata == "true"){
	drawProfileByView("myProfile_viewfriend",friendsId);
	drawProfileByView("myProfile_viewbusiness", "4");
	drawProfileByView("myProfile_viewfamily", familyId);
	attachMySocial();
	}
}
/******************************************************************************************/
function attachMySocial()
{
	var socialbody = document.getElementById('myprofile_socialBody');
   	if(myfacebook!=""){
    	var a = document.createElement("a");
    	a.setAttribute('href','#');
    	var i = document.createElement("i");
    	i.setAttribute('class','fa fa-facebook');
    	a.appendChild(i);
    	socialbody.appendChild(a);
    }
    if(mytwitter!=""){
    	var a = document.createElement("a");
    	a.setAttribute('href','#');
    	var i = document.createElement("i");
    	i.setAttribute('class','fa fa-twitter');
    	a.appendChild(i);
    	socialbody.appendChild(a);
    }
    	
    if(mylinkedin!=""){
    	var a = document.createElement("a");
    	a.setAttribute('href','#');
    	var i = document.createElement("i");
    	i.setAttribute('class','fa fa-linkedin');
    	a.appendChild(i);
    	socialbody.appendChild(a);
    }    			
    if(myskype!=""){   	
    	var s = document.createElement("a");
    	s.setAttribute('href','#');
    	var si = document.createElement("i");
    	si.setAttribute('class','fa fa-skype');
    	s.appendChild(si);
    	socialbody.appendChild(s);
    }    			
}
/******************************************************************************************/
function drawProfileByView(view, typeId)
{
	var div4 = document.createElement("div");
	div4.setAttribute('class', "prof-menu");
	
	var span2= document.createElement("span");
	span2.setAttribute('id', view+'prof_about_me');

	var p2= document.createElement("p");
	p2.setAttribute('id', view+'prof_about');

	var span3= document.createElement("span");
	span3.setAttribute('id', view+'interests_me');

	var p3= document.createElement("p");
	p3.setAttribute('id', view+'interests');
	
	var span4= document.createElement("span");
	span4.setAttribute('id', view+'address_me');

	var p4= document.createElement("p");
	p4.setAttribute('id', view+'address');
	
	var div5 = document.createElement("div");
	div5.setAttribute('class', "prof-id");

	var i1, li1;
	var ul= document.createElement("ul");
	console.log("mobilesArrCounter"+mobilesArrCounter);
	if(mobilesArrCounter>=0)
		ul.appendChild(document.createElement('hr'));

	for(var l=0;l<=mobilesArrCounter;l++)
	{
		var res="";
		if(mobilestypeArr[l])
			res = mobilestypeArr[l].split(",");
		console.log("res"+res);
		
		for(var i=0;i<res.length;i++)
		{
			console.log("res[i]"+res[i]);
			if(res[i]==typeId){
				i1= document.createElement("i");
				i1.setAttribute('class', 'fa fa-mobile');
				i1.setAttribute('id', view+'prof_mob'+l);
				i1.setAttribute('style','font-weight: bold;');
				i1.appendChild(document.createTextNode( '\u00A0' ));
		
				li1= document.createElement("li");
				li1.appendChild(i1);
				ul.appendChild(li1);
			}
		}
	}

	if(emailsArrCounter>=0)
		ul.appendChild(document.createElement('hr'));

	for(var l=0;l<=emailsArrCounter;l++)
	{
		var res="";
		if(emailstypeArr[l])
			res = emailstypeArr[l].split(",");
		for(var i=0;i<res.length;i++)
		{
			if(res[i]==typeId){		
				li1= document.createElement("li");

				li1.appendChild(document.createTextNode( '\u00A0' ));

				i1= document.createElement("i");
				i1.setAttribute('class', 'fa fa-envelope');
				i1.setAttribute('id', view+'prof_email'+l);
				i1.setAttribute('style','font-weight: bold;');
				i1.appendChild(document.createTextNode( '\u00A0' ));
		
				li1= document.createElement("li");
				li1.appendChild(i1);
				li1.appendChild(document.createTextNode( '\u00A0' ));
	
				ul.appendChild(li1);
			}
		}
	}	


	if(phonesArrCounter>=0)
		ul.appendChild(document.createElement('hr'));

	for(var l=0;l<=phonesArrCounter;l++)
	{
		var res="";
		if(phonestypeArr[l])
			res = phonestypeArr[l].split(",");
		for(var i=0;i<res.length;i++)
		{
			if(res[i]==typeId){		
				li1= document.createElement("li");

				li1.appendChild(document.createTextNode( '\u00A0' ));

				i1= document.createElement("i");
				i1.setAttribute('class', 'fa fa fa-phone');
				i1.setAttribute('id', view+'prof_phones'+l);
				i1.setAttribute('style','font-weight: bold;');
				i1.appendChild(document.createTextNode( '\u00A0' ));
		
				li1= document.createElement("li");
				li1.appendChild(i1);
				li1.appendChild(document.createTextNode( '\u00A0' ));
	
				ul.appendChild(li1);
			}
		}
	}	


	if(faxesArrCounter>=0)
		ul.appendChild(document.createElement('hr'));

	for(var l=0;l<=faxesArrCounter;l++)
	{
		var res="";
		if(faxestypeArr[l])
			res = faxestypeArr[l].split(",");
		for(var i=0;i<res.length;i++)
		{
			if(res[i]==typeId){		
				li1= document.createElement("li");

				li1.appendChild(document.createTextNode( '\u00A0' ));

				i1= document.createElement("i");
				i1.setAttribute('class', 'fa fa fa-fax');
				i1.setAttribute('id', view+'prof_faxes'+l);
				i1.setAttribute('style','font-weight: bold;');
				i1.appendChild(document.createTextNode( '\u00A0' ));
		
				li1= document.createElement("li");
				li1.appendChild(i1);
				li1.appendChild(document.createTextNode( '\u00A0' ));
	
				ul.appendChild(li1);
			}
		}
	}	

	
	
	div5.appendChild(span2);
	div5.appendChild(p2);
	div5.appendChild(document.createElement('hr'));
	
	div5.appendChild(span3);
	div5.appendChild(p3);
	div5.appendChild(document.createElement('hr'));
	
	div5.appendChild(span4);
	div5.appendChild(p4);
	//div5.appendChild(document.createElement('hr'));

	div4.appendChild(div5);
	div4.appendChild(ul);
	document.getElementById(view).appendChild(div4);
	document.getElementById(view+'prof_about_me').innerHTML="About "+name;
	document.getElementById(view+'prof_about').innerHTML=myAboutme; 
	
	document.getElementById(view+"interests_me").innerHTML="Keywords: "; 
	document.getElementById(view+"interests").innerHTML=mywants; 
	
	document.getElementById(view+"address_me").innerHTML="Personal address: "; 
	if(myaddress== null || myaddress=="null")
		myaddress="";
	if(mycityname== null || mycityname=="null")
		mycityname="";
	if(mycountryname== null || mycountryname=="null")
		mycountryname="";

	document.getElementById(view+"address").innerHTML=myaddress+ " "+mycityname+" "+mycountryname; 
	for(var l=0;l<=mobilesArrCounter;l++)
	{
		var res="";
		if(mobilestypeArr[l])
			res = mobilestypeArr[l].split(",");
	
		for(var i=0;i<res.length;i++)
		{
			if(res[i]==typeId){

					var t1 = document.createTextNode("+"+mobilesArr[l]);
					document.getElementById(view+"prof_mob"+l).appendChild(document.createTextNode( '\u00A0' ));
					document.getElementById(view+"prof_mob"+l).appendChild(t1);
			
			}
		}
	}

	for(var l=0;l<=emailsArrCounter;l++)
	{
		var res="";
		if(emailstypeArr[l])
			res = emailstypeArr[l].split(",");
		for(var i=0;i<res.length;i++)
		{
			if(res[i]==typeId){		
				var t1 = document.createTextNode(emailsArr[l]);
				document.getElementById(view+"prof_email"+l).appendChild(document.createTextNode( '\u00A0' ));
				document.getElementById(view+"prof_email"+l).appendChild(t1);

			}
		}
	}
	
	for(var l=0;l<=phonesArrCounter;l++)
	{
		var res="";
		if(phonestypeArr[l])
			res = phonestypeArr[l].split(",");
	
		for(var i=0;i<res.length;i++)
		{
			if(res[i]==typeId){
				var t1 = document.createTextNode("+"+phonesArr[l]);
				document.getElementById(view+"prof_phones"+l).appendChild(document.createTextNode( '\u00A0' ));
				document.getElementById(view+"prof_phones"+l).appendChild(t1);
			
			}
		}
	}

	
	for(var l=0;l<=faxesArrCounter;l++)
	{
		var res="";
		if(faxestypeArr[l])
			res = faxestypeArr[l].split(",");
	
		for(var i=0;i<res.length;i++)
		{
			if(res[i]==typeId){

					var t1 = document.createTextNode(faxesArr[l]);
					document.getElementById(view+"prof_faxes"+l).appendChild(document.createTextNode( '\u00A0' ));

					document.getElementById(view+"prof_faxes"+l).appendChild(t1);
			
			}
		}
	}

}
