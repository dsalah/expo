function addToFamilyGroup(conId, type, contact_name)
{
	var typeVal;
	if(type=='supplier'){
		if(otherContactFamilyIcon=="true") 
		{
				document.getElementById('family'+conId).src='css/imgs/icons/family-in.png';
				typeVal="false";	
		}
		
		else if(otherContactFamilyIcon=="false") 
		{
			document.getElementById('family'+conId).src='css/imgs/icons/family.png';
			typeVal="true";
		}			
	}
	else if(type=='customer'){
		if(otherContactFriendsIcon=="true") 
		{	
				document.getElementById('friends'+conId).src='css/imgs/icons/friends-in.png';
				typeVal="false";
		}
		
		else if(otherContactFriendsIcon=="false") 
		{
			document.getElementById('friends'+conId).src='css/imgs/icons/friends.png';
			typeVal="true";
		}	
	}

	else if(type=='favorite'){
		if(otherContactFavoriteIcon=="true") 
		{
			document.getElementById('favorite'+conId).src='css/imgs/icons/favorite-in.png';
			typeVal="false";
		}
		
		else if(otherContactFavoriteIcon=="false") 
		
		{
			document.getElementById('favorite'+conId).src='css/imgs/icons/favorite.png';
			typeVal="true";
		}	
	}


	else if(type=='partner'){
		if(otherContactBusinessIcon=="true") 
		{	
				document.getElementById('business'+conId).src='css/imgs/icons/business-in.png';
				typeVal="false";
		}
		
		else if(otherContactBusinessIcon=="false") 
		{
			document.getElementById('business'+conId).src='css/imgs/icons/business.png';
			typeVal="true";
		}	
	}
	var today=new Date();
	var h=today.getHours();
	var m=today.getMinutes();
	var s=today.getSeconds();
	m = checkTime(m);
	s = checkTime(s);	
	
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yyyy = today.getFullYear();

	if(dd<10) {
    	dd='0'+dd;
    }	 

	if(mm<10) {
    	mm='0'+mm;
	} 
	today = dd+'-'+mm+'-'+yyyy;
	var time = h+":"+m+":"+s;
	var fullDate = today+' '+time;
	var mycomment="";
	if(typeVal=="true"){
		
		var	selectSql = 'SELECT * FROM CONTACTSCONNECTION WHERE contactid=(?) AND othercontactid=(?)';
		var	insertSql = 'INSERT INTO CONTACTSCONNECTIONLEVELS (contactconnectionid, typevalue, type, contactid, othercontactid) VALUES (?,?,?,?,?)';
		var	insertConnection = 'INSERT INTO CONTACTSCONNECTION (contactconnectionid,contactid ,othercontactid) VALUES (?,?,?)';
		var	sqltxt = 'UPDATE CONTACTSCONNECTION SET typevalue=(?) WHERE type=(?) AND contactid=(?) AND othercontactid=(?)';
		var insertcomment= 'INSERT INTO CONTACTSCONNECTIONCOMMENT (commentid, contactid,  othercontactid, comment, time) VALUES (?, ?, ?, ?, ?)';	    
		var allComments = 'SELECT max(commentid) AS topId FROM CONTACTSCONNECTIONCOMMENT';

		var commentId="";
    	db.transaction(function(tx){
    		tx.executeSql(allComments,[],function(ax,res){
		    	var totalComments = res.rows.item(0).topId;
		    	commentId = totalComments+1;
		    },errorCB);
		    
            tx.executeSql(selectSql,[id, conId], function(tx, selectResult){
            	console.log("select id "+ conId +" = "+ selectResult.rows.length);
	    		if(selectResult.rows.length>=1)
	    		{
	    			var contactconnectionid = selectResult.rows.item(0).contactconnectionid;	
	    			console.log("contactconnectionid"+contactconnectionid);
		            tx.executeSql(insertSql,[contactconnectionid, typeVal,type, id, conId]);
		        }
				else
				{
					console.log("no connection before");	
		            tx.executeSql(insertConnection,["", id, conId]);
		            tx.executeSql(insertSql,["", typeVal, type, id, conId]);
		            tx.executeSql(insertSql,["", "true", "Business", id, conId]);

				}
				
				mycomment ="You added "+contact_name+" as "+type+" when she/he was "+myCurrentContactPosition+" @ "+myCurrentContactCompany +
				" and you were "+position+" @ "+company;
				
				tx.executeSql(insertcomment,[commentId, id, conId, mycomment, fullDate],errorCB);
				
    networkState = checkConnection();
	if(networkState!="none" && networkState!="unknown"){
			getConnectionId(conId, type, typeVal);
			sendNewCommentToServer(conId, mycomment, contact_name,"");
									
	}
	else
	{
		$.msg({ content : 'Relations updated locally .. Ckeck you internet connection' });			
	    var updates= 'INSERT INTO UPDATESFUNCTIONS(updateid, name, parameters, datetime) VALUES (?,?,?,?)';
	    var sql_max_updated = "SELECT MAX(updateid) as updateid FROM UPDATESFUNCTIONS";	    
		db.transaction(function(tx){
			var updatedId = 0;  
	    	tx.executeSql(sql_max_updated,[],function(tx, results){
	    		var len = results.rows.length;
	    		if(len>0)
		    		updatedId = results.rows.item(0).updateid;
	    		updatedId= updatedId+1;

		var arr1 = conId+","+type+","+typeVal+","+updatedId;
		var arr2 = conId+","+mycomment+","+contact_name+","+(updatedId+1);	
	    		
	    		console.log("updated function id "+updatedId);
	    	tx.executeSql(updates,[updatedId, "getConnectionId", arr1, setLastUpdatesTime()],errorCB);
	    	tx.executeSql(updates,[(updatedId+1), "sendNewCommentToServer", arr2, setLastUpdatesTime()],errorCB);

	    	},errorCB);


	    }, errorCB, successDB);   
	}
			
            }, errorCB);

    		}, errorCB, function(tx,resultSet){
				showContactComments(conId, contact_name);
				console.log(mycomment);
				if(type=='supplier')  {
    				otherContactFamilyIcon=typeVal;
    				}
    			if(type=='customer'){
    				otherContactFriendsIcon=typeVal;
    				}
    			if(type=='favorite'){
    				otherContactFavoriteIcon=typeVal;
    				}
    			if(type=='partner'){
    				otherContactBusinessIcon=typeVal;
    				}
    	}); 	
    }
    else
    {
    	var	selectSql = "SELECT * FROM CONTACTSCONNECTION WHERE contactid=(?) AND othercontactid=(?)";
		var insertcomment= 'INSERT INTO CONTACTSCONNECTIONCOMMENT (commentid, contactid,  othercontactid, comment, time) VALUES (?, ?, ?, ?, ?)';	    
    	var	deleteSql = "DELETE FROM CONTACTSCONNECTIONLEVELS WHERE contactconnectionid=(?) AND contactid=(?) AND othercontactid=(?) AND type=(?)";
		var allComments = 'SELECT max(commentid) AS topId FROM CONTACTSCONNECTIONCOMMENT';
		var commentId = "";
	    	
    	conId=''+conId+'';
    	id=''+id+'';
		db.transaction(function(tx){
		tx.executeSql(allComments,[],function(ax,res){
		  	var totalComments = res.rows.item(0).topId;
		   	commentId = totalComments+1;
		},errorCB);
	    	
        tx.executeSql(selectSql,[id, conId], function(tx, selectResult){
	    	if(selectResult.rows.length>=1)
	    	{
	    		var contactconnectionid = selectResult.rows.item(0).contactconnectionid;
	    		console.log("contactconnectionid to delete: "+contactconnectionid);		        			            
		        tx.executeSql(deleteSql,[contactconnectionid, id, conId ,type]);
		            
		        mycomment ="You delete relation "+type+" when "+contact_name +" was "+myCurrentContactPosition+" @ "+myCurrentContactCompany +
					" and you was "+position+" @ "+company;
				
		        tx.executeSql(insertcomment,[commentId, id, conId, mycomment, fullDate],errorCB);
			    console.log(mycomment);
			    networkState = checkConnection();
				if(networkState!="none" && networkState!="unknown"){
						getConnectionId(conId, type, typeVal, "");
						sendNewCommentToServer(conId, mycomment, contact_name, "");												
				}
				else
				{
					$.msg({ content : 'Relations updated locally .. Ckeck you internet connection' });	
				    var updates= 'INSERT INTO UPDATESFUNCTIONS(updateid, name, parameters, datetime) VALUES (?,?,?,?)';
				    var sql_max_updated = "SELECT MAX(updateid) as updateid FROM UPDATESFUNCTIONS";	    
					db.transaction(function(tx){
						var updatedId = 0;  
				    	tx.executeSql(sql_max_updated,[],function(tx, results){
				    		var len = results.rows.length;
			    		if(len>0)
			    		updatedId = results.rows.item(0).updateid;
			    		updatedId= updatedId+1;
			    		
						var arr1 = conId+","+type+","+typeVal+","+updatedId;
						var arr2 = conId+","+mycomment+","+contact_name+","+(updatedId+1);				

			    		console.log("updated function id "+updatedId);
			    	},errorCB);
				    	tx.executeSql(updates,[updatedId, "getConnectionId", arr1, setLastUpdatesTime()],errorCB);
				    	tx.executeSql(updates,[(updatedId+1), "sendNewCommentToServer", arr2, setLastUpdatesTime()],errorCB);			
				    }, errorCB, successDB);   			
				}		    			    			   		            
		    }
			else
			{
					console.log("no connection before !!!");
					$.msg({ content : 'Please update your app' });							
			}
		}, errorCB);

    }, errorCB, function(tx,resultSet){
				showContactComments(conId, contact_name);
				console.log(mycomment);
				if(type=='supplier')  {
    				otherContactFamilyIcon=typeVal;
    				}
    			if(type=='customer'){
    				otherContactFriendsIcon=typeVal;
    				}
    			if(type=='favorite'){
    				otherContactFavoriteIcon=typeVal;
    				}
    			if(type=='partner'){
    				otherContactBusinessIcon=typeVal;
    				}
    	}); 	 	    		
    }
}
/********************************************************************************/
function getConnectionId(othercontactid, type, typeVal, updatedId)
{
console.log("in get connection");
console.log("othercontactid:"+othercontactid+" type "+type+ "typeVal: "+typeVal);

	    var getconn= 'SELECT * FROM CONTACTSCONNECTION WHERE contactid=(?) AND othercontactid=(?)';
		db.transaction(function(tx){
	    	tx.executeSql(getconn,[id, othercontactid],function(tx,results)
	    	{
	    		console.log("len: "+results.rows.length);
	    		var contactconnectionid="";
	    		
	    		if(results.rows.length>=1){
	    			contactconnectionid = results.rows.item(0).contactconnectionid;
		    		sendNewConnectionToServer(contactconnectionid, type, typeVal, othercontactid, updatedId);
	    			console.log("other id :"+othercontactid + "contactconnection : "+contactconnectionid+ "type "+ type + "value: "+typeVal);

	    		}
	    		
				else{
	    		console.log("other id :"+othercontactid + "contactconnection : "+contactconnectionid+ "type "+ type + "value: "+typeVal);
	    		sendNewConnectionToServer(contactconnectionid, type, typeVal, othercontactid, updatedId);
	    		}
	    	
	    	}
	    	,errorCB);
	        }, errorCB, successDB);   

}
/****************************************************************************************************/
function sendNewConnectionToServer(contactconnectionid, type, typeVal, othercontactid, updatedId)
{
console.log("sendNewConnectionToServer");

var url="";
var levelId="";
if(type=="customer")
	levelId = friendsId;
else if(type=="supplier")
	levelId = familyId;
if(type=="partner")
	levelId = businessId;
if(type=="favorite")
	levelId = favoriteId;

/*if(typeVal=="true")
	url = "http://www.cardsbond.com/BondSys/web/api/v1/events/add_connection_level?bond_contactconnection_id="+contactconnectionid+"&bond_connectiontype_level_id="+levelId+"&id="+id+"&other_id="+othercontactid+"&type_name="+type;
if(typeVal=="false")
	url = "http://www.cardsbond.com/BondSys/web/api/v1/events/remove_connection_level?bond_contactconnection_id="+contactconnectionid+"&bond_connectiontype_level_id="+levelId;
*/
if(typeVal=="true")
	url = "http://www.cardsbond.com/BondSys/web/api/v1/events/add_connection_level?bond_connectiontype_level_id="+levelId+"&id="+id+
	"&other_id="+othercontactid+"&type_name="+type;
if(typeVal=="false")
	url = "http://www.cardsbond.com/BondSys/web/api/v1/events/remove_connection_level?id="+id+
	"&other_id="+othercontactid+"&bond_connectiontype_level_id="+levelId;

console.log(url);

		var ajaxRequest;
		try{
			ajaxRequest = new XMLHttpRequest();
		} catch (e){
			try{
				ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
			} catch (e) {
				try{
					ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
					//alert("Microsoft");
				} catch (e){
					alert("Your browser broke!");
				return false;
				}
			}
		}
		ajaxRequest.onreadystatechange = function(){
			if(ajaxRequest.readyState == 4){
				var response=ajaxRequest.responseText;
					console.log("response:  "+response);
					if(response!="")
					{
						if(response.search("error") == -1)
						{
							var results = JSON.parse(response);
							console.log("Results from send new conn to server : "+results);
							if(results != "0" && results != "success" && typeVal=="true"){
								var	sqltxt1 = 'UPDATE CONTACTSCONNECTION SET contactconnectionid=(?) WHERE contactid=(?) AND othercontactid=(?)';
								var	sqltxt2 = 'UPDATE CONTACTSCONNECTIONLEVELS SET contactconnectionid=(?) WHERE contactid=(?) AND othercontactid=(?)';
	
						    	db.transaction(function(tx){
									tx.executeSql(sqltxt1,[results, id, othercontactid],errorCB);
								    tx.executeSql(sqltxt2,[results, id, othercontactid],errorCB);
								}, errorCB, successDB);
							}
							else if(results == "0" )								
								$.msg({ content : 'Something wrong in the Server' });	
							
						}
						
					}

				}
				}
		ajaxRequest.open("GET", url, true);		
		ajaxRequest.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
		//ajaxRequest.setRequestHeader("Content-length", param.length);
		ajaxRequest.setRequestHeader("Connection", "close");
		ajaxRequest.send();	
}