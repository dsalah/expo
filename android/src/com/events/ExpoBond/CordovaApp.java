/*
       Licensed to the Apache Software Foundation (ASF) under one
       or more contributor license agreements.  See the NOTICE file
       distributed with this work for additional information
       regarding copyright ownership.  The ASF licenses this file
       to you under the Apache License, Version 2.0 (the
       "License"); you may not use this file except in compliance
       with the License.  You may obtain a copy of the License at

         http://www.apache.org/licenses/LICENSE-2.0

       Unless required by applicable law or agreed to in writing,
       software distributed under the License is distributed on an
       "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
       KIND, either express or implied.  See the License for the
       specific language governing permissions and limitations
       under the License.
 */

package com.events.ExpoBond;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.webkit.JavascriptInterface;

import org.apache.cordova.*;

public class CordovaApp extends CordovaActivity
{
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        super.init();
        appView.addJavascriptInterface(this, "MainActivity");
        super.loadUrl(launchUrl);
    }
    
    @JavascriptInterface
    public String get_sim_number()
    {
    	Log.i("sim1 i", "onCreate");
    	TelephonyManager telemamanger = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        String getSimSerialNumber = telemamanger.getSimSerialNumber();
        String getSimNumber = telemamanger.getLine1Number();
        Log.i("getSimNumber",getSimNumber);
        return getSimNumber;
    }
    
    @JavascriptInterface
    public void browseSMS(String number)
    {
    	Log.i("number",number);
    	Uri uri = Uri.parse("smsto:" + number);
	    Intent i = new Intent(Intent.ACTION_SENDTO, uri);
	    startActivity(Intent.createChooser(i, ""));
    }

    @JavascriptInterface
    public void  showDevicePhone(){
    	Intent intent = new Intent(Intent.ACTION_VIEW, ContactsContract.Contacts.CONTENT_URI);
    	startActivity(intent);
    }

    @JavascriptInterface
    public void dialDirectly(String number)
    {
    	Intent intent = new Intent("android.intent.action.CALL");
    	Uri data = Uri.parse("tel:"+ number );    	 
    	intent.setData(data);
    	startActivity(intent);
    }
    

}
